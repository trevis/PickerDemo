﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ACE.DatLoader.FileTypes;
using ACE.Entity.Enum;
using CommandLine;
using UtilityBelt.Navigation.Shared.Lib;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Data;
using UBDataGen.Lib;
using UBDataGen.Lib.Scrapers;
using ACE.Adapter.GDLE.Models;

namespace UBDataGen {
    class Program {
        public static bool Verbose { get; private set; }

        public class Options {
            [Verb("dumpall", true)]
            public class AllDumpOptions {
                [Option('o', "output-dir", Default = @"../../net48/data/", HelpText = "Set output directory")]
                public string OutputDirectory { get; set; }

                [Option('p', "db-port", Default = (uint)3306, HelpText = "Database port for ace world db")]
                public uint ACEDatabasePort { get; set; }

                [Option('h', "db-host", Default = "localhost", HelpText = "Database host for ace world db")]
                public string ACEDatabaseHost { get; set; }

                [Option('u', "db-user", Default = "root", HelpText = "Database user for ace world db")]
                public string ACEDatabaseUser { get; set; }

                [Option('w', "db-pass", Default = "", HelpText = "Database password for ace world db")]
                public string ACEDatabasePassword { get; set; }

                [Option('n', "db-name", Default = "ace_world", HelpText = "Database name for ace world db")]
                public string ACEDatabaseName { get; set; }

                [Option('d', "eor-dat-dir", Default = @"C:\Turbine\Asheron's Call\", HelpText = "Directory containing end of retail dat files")]
                public string EORDatDirectory { get; set; }

                [Option('c', "classic-dat-dir", Default = @"C:\Turbine\Asheron's Call - Classic\", HelpText = "Directory containing classic dat files")]
                public string ClassicDatDirectory { get; set; }

                [Option('g', "gdle-content-dir", Default = @"C:\ac\gdle\", HelpText = "Set gdle content directory")]
                public string GDLEContentDirectory { get; set; }

                [Option('q', "classic-content-dir", Default = @"C:\ac\gdle-classic\", HelpText = "Set gdle classic content directory")]
                public string ClassicContentDirectory { get; set; }
            }

            [Verb("testpath")]
            public class TestPathOptions {
                [Option('o', "output-dir", Default = @"../../net48/data/", HelpText = "Set output directory")]
                public string OutputDirectory { get; set; }
            }
        }

        static void Main(string[] args) {
            var result = Parser.Default.ParseArguments<Options.AllDumpOptions, Options.TestPathOptions>(args)
                .WithParsed(Run);
        }

        private static void Run(object obj) {
            var sw = new Stopwatch();
            sw.Start();

            switch (obj) {
                case Options.TestPathOptions testPathOptions:
                    var file = Path.Combine(testPathOptions.OutputDirectory, "landblockgraph_ace.ubd");
                    var graphStore = new LandblockGraphStore();

                    Program.Log($"CanDeserialize: {graphStore.CanDeserializeFromFile(file)}");

                    if (graphStore.TryDeserialize(file)) {

                        Program.Log($"Loaded graph with {graphStore.Graph.Nodes.Count:N0} nodes and {graphStore.Graph.Nodes.Sum(n => n.Edges.Count):N0} edges");
                    }
                    else {
                        Program.Log($"Failed to load graph: {file}");
                    }

                    var graph = graphStore.Graph;
                    graph.BuildEdgeNodeCaches();

                    TestPath(graph, testPathOptions, 275, new List<string>() { "KingOolutangaEnter" }, new List<string>() {  });
                    break;

                case Options.AllDumpOptions allDumpOptions:
                    if (!Directory.Exists(allDumpOptions.OutputDirectory)) {
                        Directory.CreateDirectory(allDumpOptions.OutputDirectory);
                    }
                    var dumper = new DatDumper(allDumpOptions.EORDatDirectory, allDumpOptions.OutputDirectory, "eor");
                    dumper.Load();

                    var meshGen = new LandblockMeshGen(dumper, allDumpOptions.OutputDirectory, "eor");
                    meshGen.Load();

                    //var asheronDbLandscape = new AsheronDbLandscapeGen(dumper, allDumpOptions.OutputDirectory, "eor");
                    //asheronDbLandscape.Load();

                    var ACEScraper = new ACEScraper(allDumpOptions, dumper.LandblockStore, meshGen.MeshStore);
                    ACEScraper.Load();

                    var graphGen = new LandblockGraphGen(allDumpOptions.OutputDirectory, "ace", dumper.LandblockStore, meshGen.MeshStore, ACEScraper.ContentStore);
                    graphGen.Load();
                    break;
            }

            sw.Stop();
            Program.Log($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms total");
        }

        private static void TestPath(UtilityBelt.Navigation.Shared.Models.LBGraph graph, Options.TestPathOptions testPathOptions, int minLevel, List<string> questFlags, List<string> keys) {
            // just testing pathfinding....

            var start = graph.Nodes.Where(n => (n.Frame.Landblock >> 16) == (0x00070000 >> 16)).FirstOrDefault();
            var goal = graph.Nodes.Where(n => (n.Frame.Landblock >> 16) == (0x5F430000 >> 16)).FirstOrDefault();

            if (start == null) {
                Program.Log($"Start is null");
                return;
            }
            if (goal == null) {
                Program.Log($"Goal is null");
                return;
            }

            var startName = $"Start: {start.FriendlyName}";
            var goalName = $"End: {goal.FriendlyName}";

            if (start.Type == LocationType.Dungeon)
                startName = DungeonsNames.GetName(start.Frame.Landblock);
            if (goal.Type == LocationType.Dungeon)
                goalName = DungeonsNames.GetName(goal.Frame.Landblock);


            //Program.Log($"Generating path from {startName} -> {goalName} (With Quest Flags: {(questFlags.Count > 0 ? string.Join(", ", questFlags) : "none")}, MinLevel: {minLevel})");
            Program.Log($"Generating path from {startName} -> {goalName} (With MinLevel: {minLevel}, Quest Flags: {(questFlags.Count > 0 ? string.Join(", ", questFlags) : "none")}, Keys: {(keys.Count > 0 ? string.Join(", ", keys) : "none")})");
            var sw2 = new Stopwatch();
            sw2.Start();
            var path = LBAStar.FindPath(graph, start, goal, questFlags, minLevel, out float distance, out uint checkedNodeCount);
            sw2.Stop();

            if (path == null) {
                Program.Log($"Path is null! Took {((double)sw2.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms total to check {checkedNodeCount:N0} nodes\n");
                return;
            }

            var pathStrs = new List<string>();
            var prevNode = path.First().Node;
            for (var i = 0; i < path.Count; i++) {
                var node = path[i].Node;
                var reqs = node.Requirements == null ? "" : node.Requirements.ToString();

                if (node.Type == LocationType.Dungeon) {
                    pathStrs.Add($"- Enter Dungeon: {node.FriendlyName} [0x{node.Frame.Landblock >> 16:X4}] {reqs}");
                }
                else if (node.Type == LocationType.Portal) {
                    if (prevNode.Type == LocationType.Dungeon) {
                        pathStrs.Add($"- Run to: {node.Frame.ToLocalString()}");
                        pathStrs.Add($"- Use Portal: {node.FriendlyName} @ {node.Frame.ToLocalString()} {reqs}");
                    }
                    else {
                        pathStrs.Add($"- Use Portal: {node.FriendlyName}[{node.WeenieId}] @ {node.Frame.ToCoordinatesString(3, false)} {reqs}");
                    }
                }
                else if (node.Type == LocationType.Landscape) {
                    while (i < path.Count) {
                        node = path[i].Node;
                        if (i + 1 < path.Count && path[i + 1].Node.Type == LocationType.Landscape) {
                            i++;
                        }
                        else { break; }
                    }
                    var dest = node.Frame.ToCoordinatesString(3, false);
                    if (path.Count > i + 1 && path[i + 1].Node.Type == LocationType.Portal)
                        dest = path[i + 1].Node.Frame.ToCoordinatesString(3, false);
                    pathStrs.Add($"- Run To: {dest} {reqs}");
                }
                prevNode = node;
            }

            Program.Log($"{String.Join("\n", pathStrs)}\n-- Cost: {distance:N2}");
            Program.Log($"Took {((double)sw2.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to find path, with {checkedNodeCount:N0} nodes checked");
        }

        public static void Log(string v, bool newLine=true) {
            if (newLine)
                Console.WriteLine(v);
            else
                Console.Write(v);
        }

        private static void ShowUsage() {
            Console.WriteLine("Processes data from emulator content into a format useable by UtilityBelt");
            Console.WriteLine("");
            Console.WriteLine("Example Usage:");
            Console.WriteLine("# TODO");
        }
    }
}
