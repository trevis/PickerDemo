﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBDataGen.Lib {
    class GeoJSON {
        public string type { get; set; } = "FeatureCollection";
        public List<Feature> features { get; set; } = new List<Feature>();
    }

    public class Feature {
        public string type { get; set; } = "Feature";
        public Dictionary<string, string> properties = new Dictionary<string, string>();
        public Geometry geometry { get; set; } = new Geometry();
    }

    public class Geometry {
        public string type { get; set; } = "Polygon";
        public List<List<List<float>>> coordinates { get; set; } = new List<List<List<float>>>();
    }
}
