﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace UBDataGen.Lib {
    internal static class Helpers {
        public static float QuaternionToHeading(Quaternion q2) {
            var q = new Quaternion(q2.W, q2.X, q2.Y, q2.Z);
            double sinr = 2 * (q.W * q.X + q.Y * q.Z);
            double cosr = 1 - 2 * (q.X * q.X + q.Y * q.Y);
            return (float)((360.0 + 180.0 + (Math.Atan2(sinr, cosr) * 180.0 / Math.PI)) % 360);
        }

        public static Quaternion HeadingToQuaternion(float angle) {
            return ToQuaternion((float)Math.PI * -angle / 180.0f, 0, 0);
        }

        public static double CalculateHeading(Vector3 start, Vector3 target) {
            var deltaY = target.Y - start.Y;
            var deltaX = target.X - start.X;
            return (360 - (Math.Atan2(deltaY, deltaX) * 180 / Math.PI) + 90) % 360;
        }

        // https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        public static Quaternion ToQuaternion(float yaw, float pitch, float roll) { // yaw (Z), pitch (Y), roll (X)
            // Abbreviations for the various angular functions
            float cy = (float)Math.Cos(yaw * 0.5);
            float sy = (float)Math.Sin(yaw * 0.5);
            float cp = (float)Math.Cos(pitch * 0.5);
            float sp = (float)Math.Sin(pitch * 0.5);
            float cr = (float)Math.Cos(roll * 0.5);
            float sr = (float)Math.Sin(roll * 0.5);

            Quaternion q = new Quaternion();

            q.W = cy * cp * cr + sy * sp * sr;
            q.X = cy * cp * sr - sy * sp * cr;
            q.Y = sy * cp * sr + cy * sp * cr;
            q.Z = sy * cp * cr - cy * sp * sr;

            return q;
        }
    }
}
