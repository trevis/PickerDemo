﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Data;

namespace UBDataGen.Lib.Scrapers {
    public abstract class IScraper {

        protected readonly ConcurrentDictionary<uint, Weenie> weenieCache = new ConcurrentDictionary<uint, Weenie>();
        protected readonly ConcurrentDictionary<uint, LandblockInstance> landblockInstanceCache = new ConcurrentDictionary<uint, LandblockInstance>();
        protected LandblockInfoStore LandblockStore;

        public EmuContentStore ContentStore { get; } = new EmuContentStore();
        public LandblockMeshStore MeshStore { get; protected set;  } = new LandblockMeshStore();


        public virtual void Load() {
            Program.Log("");
            FindPortals();
            FindDoorsKeysLevers();
        }

        public void FindDoorsKeysLevers() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("Finding doors/keys/levers... ", false);
            int total = 0;
            int badPos = 0;
            int unlocked = 0;
            int lockpick = 0;
            int lever = 0;
            int keys = 0;
            int unknown = 0;

            foreach (var lbi in landblockInstanceCache.Values) {
                if (!weenieCache.ContainsKey(lbi.WeenieClassId))
                    continue;

                var weenie = weenieCache[lbi.WeenieClassId];

                if (weenie.Type != WeenieType.Door)
                    continue;

                total++;

                var srcX = lbi.Frame.Landblock >> 16 >> 8;
                var srcY = lbi.Frame.Landblock >> 16 & 0xFF;
                var srcLbFlags = LandblockStore.MapData[srcX, srcY];
                var srcIsDungeon = srcLbFlags.HasFlag(LandblockFlag.Dungeon);
                var srcIsLandscape = srcLbFlags.HasFlag(LandblockFlag.Landscape);

                if (!(srcIsDungeon || srcIsLandscape)) {
                    badPos++;
                    continue;
                }

                var foundWayToOpen = false;
                var hasLock = false;
                string keyCode = "";
                int lpResist = 0;
                uint keyWcid = 0;
                string keyName = "";
                List<Lever> levers = new List<Lever>();
                DoorOpenType openType = DoorOpenType.Unknown;

                // locked
                if (weenie.GetProp(PropertyBool.DefaultLocked) || weenie.GetProp(PropertyBool.Locked)) {
                    hasLock = true;
                    // lockpick
                    if (weenie.GetProp(PropertyInt.ResistLockpick) > 0) {
                        lockpick++;
                        lpResist = weenie.GetProp(PropertyInt.ResistLockpick);
                        openType |= DoorOpenType.Lockpick;
                        foundWayToOpen = true;
                    }
                    else {
                        //Program.Log($"NO LP: {lbi.ObjCellId:X8} {weenie.WeenieClassId}:{weenie.ClassName}");
                    }
                }

                // unuseable (lever/door/event or something?)
                if ((Useable)weenie.GetProp(PropertyInt.ItemUseable) == Useable.No) {
                    hasLock = true;
                    if (lbi.Links != null && lbi.Links.Count > 0) {
                        var links = lbi.Links.Where(l => l.ParentGuid == lbi.Guid || l.ChildGuid == lbi.Guid);
                        foreach (var link in links) {
                            var targetId = link.ParentGuid == lbi.Guid ? link.ChildGuid : link.ParentGuid;
                            var targetLbi = landblockInstanceCache[targetId];
                            var targetWeenie = weenieCache.ContainsKey(targetLbi.WeenieClassId) ? weenieCache[targetLbi.WeenieClassId] : null;
                            var targetX = (uint)targetLbi.Frame.Landblock >> 16 >> 8;
                            var targetY = (uint)targetLbi.Frame.Landblock >> 16 & 0xFF;
                            var targetLbFlags = LandblockStore.MapData[targetX, targetY];
                            var targetIsDungeon = targetLbFlags.HasFlag(LandblockFlag.Dungeon);
                            var targetIsLandscape = targetLbFlags.HasFlag(LandblockFlag.Landscape);

                            if (!(targetIsDungeon || targetIsLandscape))
                                continue;

                            // TODO: fixy
                            if (targetWeenie == null)
                                continue;

                            //Program.Log($"Found {weenie.ClassName} (LP:{lpResist}) link to {targetWeenie.ClassName} ({targetWeenie.WeenieType})");
                            levers.Add(new Lever() {
                                Name = weenie.GetProp(PropertyString.Name, "Unknown Lever"),
                                WeenieId = weenie.Id,
                                Frame = targetLbi.Frame,
                                LocationType = targetIsLandscape ? LocationType.Landscape : (targetIsDungeon ? LocationType.Dungeon : LocationType.Unknown),
                            });
                        }

                        if (links.Count() > 0) {
                            foundWayToOpen = true;
                            lever++;
                            openType |= DoorOpenType.LeverOrButton;
                        }
                    }
                }

                // key locked
                if (!string.IsNullOrWhiteSpace(weenie.GetProp(PropertyString.LockCode))) {
                    keyCode = weenie.GetProp(PropertyString.LockCode);

                    if (keyCode != "masterkey") {
                        var weenieKey = weenieCache.Values.Where(w => w.GetProp(PropertyString.KeyCode) == keyCode).FirstOrDefault();

                        if (weenieKey != null) {
                            keys++;
                            keyWcid = weenieKey.Id;
                            keyName = weenieKey.GetProp(PropertyString.Name);
                            foundWayToOpen = true;
                            openType |= DoorOpenType.Key;
                            //Program.Log($"Found {weenie.ClassName} (LP:{lpResist}) needs key ({weenieKey.PropertiesString[PropertyString.Name]} - {weenieKey.WeenieClassId} {weenieKey.ClassName})");
                        }
                    }
                }

                if (!hasLock) {
                    unlocked++;
                    continue;
                }
                else if (!foundWayToOpen) {
                    //Program.Log($"Dont know how to open {weenie.WeenieClassId}({weenie.ClassName}) (LP:{lpResist}, Key:{keyCode}) @ {lbi.ObjCellId:X8}");
                    unknown++;
                }


                var door = new Door() {
                    Name = weenie.GetProp(PropertyString.Name, "Unknown Door"),
                    SetupId = weenie.SetupId,
                    WeenieId = weenie.Id,
                    Frame = lbi.Frame,
                    OpenType = openType,
                    LockpickResist = lpResist,
                    LocationType = srcIsLandscape ? LocationType.Landscape : (srcIsDungeon ? LocationType.Dungeon : LocationType.Unknown),
                    Key = keyWcid == 0 ? new Key() : new Key() {
                        Code = keyCode,
                        Name = keyName,
                        WeenieId = keyWcid
                    },
                    Levers = levers.Count() > 0 ? levers : new List<Lever>()
                };

                ContentStore.Doors.Add(door);

                //Program.Log($"{lbi.Landblock:X4} {srcLbFlags} -> {destination.ObjCellId >> 16:X4} {dstLbFlags}");
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load:");
            Program.Log($"\t Unlocked Doors: \t{unlocked:N0}");
            Program.Log($"\t Locked Doors: \t\t{total - unlocked - (badPos + unknown):N0}");
            Program.Log($"\t\t Can Lockpick: \t\t{lockpick:N0}");
            Program.Log($"\t\t Can Key: \t\t{keys:N0}");
            Program.Log($"\t\t Can Lever: \t\t{lever:N0}");
            Program.Log($"\t Invalid: \t\t{badPos + unknown:N0}");
            Program.Log($"\t\t Bad Pos: \t\t{badPos:N0}");
            Program.Log($"\t\t Unknown: \t\t{unknown:N0}");
            Program.Log($"\t Total Saved: \t\t{ContentStore.Doors.Count:N0} / {total:N0}\n");
        }

        public void FindPortals() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("Finding portals... ", false);
            int landscapeToDungeon = 0;
            int dungeonToLandscape = 0;
            int landscapeToLandscape = 0;
            int dungeonToDungeon = 0;
            int dungeonInternal = 0;
            int noDestination = 0;
            int badSrcOrDstPosition = 0;
            int unknown = 0;
            int total = 0;
            int minLevelRestricted = 0;
            int questRestricted = 0;
            int noCollisions = 0;
            int generator = 0;

            foreach (var lbi in landblockInstanceCache.Values) {
                if (!weenieCache.ContainsKey(lbi.WeenieClassId))
                    continue;

                var weenie = weenieCache[lbi.WeenieClassId];

                if (weenie.Type != WeenieType.Portal)
                    continue;

                total++;

                // not currently include portals tied to generators
                if (lbi.IsLinkChild) {
                    generator++;
                    continue;
                }

                // check that we have a destination set
                if (!weenie.HasPosition(PositionType.Destination)) {
                    //Program.Log($"weenie.PropertiesPosition == null?? {weenie.WeenieClassId} {weenie.ClassName}");
                    noDestination++;
                    continue;
                }

                if (weenie.GetProp(PropertyBool.IgnoreCollisions, false) || !weenie.GetProp(PropertyBool.ReportCollisions, false)) {
                    noCollisions++;
                    continue;
                }

                var destination = weenie.GetPosition(PositionType.Destination);
                var srcX = lbi.Frame.Landblock >> 24;
                var srcY = (lbi.Frame.Landblock >> 16) & 0xFF;
                var srcLbFlags = LandblockStore.MapData[srcX, srcY];
                var dstX = destination.Landblock >> 24;
                var dstY = (destination.Landblock >> 16) & 0xFF;
                var dstLbFlags = LandblockStore.MapData[dstX, dstY];
                var srcIsDungeon = srcLbFlags.HasFlag(LandblockFlag.Dungeon);
                var srcIsLandscape = srcLbFlags.HasFlag(LandblockFlag.Landscape);
                var dstIsDungeon = dstLbFlags.HasFlag(LandblockFlag.Dungeon);
                var dstIsLandscape = dstLbFlags.HasFlag(LandblockFlag.Landscape);

                var terrainHeightDiffAllowance = 8.0f;

                // check portal z against terrain height
                if (srcIsLandscape) {
                    var cellOffsetX = (lbi.Frame.Landblock << 16 >> 24);
                    var cellOffsetY = (lbi.Frame.Landblock << 24 >> 24);
                    var x = (srcX * 8) + cellOffsetX;
                    var y = (srcY * 8) + cellOffsetY;
                    //Program.Log($"src: {x}, {y} // ({srcX} * 8) + {(lbi.Frame.Landblock << 16 >> 24)}, ({srcY} * 8) + {(lbi.Frame.Landblock << 24 >> 24)}");
                    if (lbi.Frame.LocalZ < -5 || lbi.Frame.LocalX < -5) {
                        if (!(cellOffsetX > 8 || cellOffsetY > 8)) {
                            //Program.Log($"src Math.Abs({MeshStore.CellHeights[x, y]} - {lbi.Frame.LocalZ}) = {Math.Abs(MeshStore.CellHeights[x, y] - lbi.Frame.LocalZ) > terrainHeightDiffAllowance}");
                        }
                        srcIsLandscape = false;
                    }
                }

                // check portal dest z against terrain height
                if (dstIsLandscape) {
                    var cellOffsetX = (destination.Landblock << 16 >> 24);
                    var cellOffsetY = (destination.Landblock << 24 >> 24);
                    var x = (dstX * 8) + cellOffsetX;
                    var y = (dstY * 8) + cellOffsetY;
                    //Program.Log($"src: {x}, {y} // ({dstX} * 8) + {(destination.Landblock << 16 >> 24)}, ({dstY} * 8) + {(destination.Landblock << 24 >> 24)}");
                    if (destination.LocalZ < -5 || destination.LocalX < -5) {
                        if (!(cellOffsetX > 8 || cellOffsetY > 8)) {
                            //Program.Log($"dst Math.Abs({MeshStore.CellHeights[x, y]} - {destination.LocalZ}) = {Math.Abs(MeshStore.CellHeights[x, y] - destination.LocalZ) > terrainHeightDiffAllowance}");
                        }
                        srcIsLandscape = false;
                    }
                }

                // check valid src/dst
                if (!(srcIsDungeon || srcIsLandscape) || !(dstIsDungeon || dstIsLandscape)) {
                    badSrcOrDstPosition++;
                    continue;
                }

                PortalTravelType travelType = PortalTravelType.Invalid;
                int minLevel = weenie.GetProp(PropertyInt.MinLevel, 0);
                string questRestriction = weenie.GetProp(PropertyString.QuestRestriction, "");

                if (minLevel > 0)
                    minLevelRestricted++;

                if (!string.IsNullOrWhiteSpace(questRestriction))
                    questRestricted++;

                //if (weenie.HasProp(PropertyString.Quest)) {
                //    Program.Log($"{weenie.GetProp(PropertyString.Name)} has Quest: {weenie.GetProp(PropertyString.Quest)}");
                //}

                // dungeon portal with destination in the same dungeon
                if (srcIsDungeon && dstIsDungeon && (destination.Landblock >> 16) == (lbi.Frame.Landblock >> 16)) {
                    dungeonInternal++;
                    travelType = PortalTravelType.DungeonInternal;
                    //Program.Log($"Found Dungeon->Self: (InstId:{lbi.Guid:X8}//WCID:{weenie.Id}) {UBCommon.Data.DungeonsNames.GetName(lbi.Frame.Landblock)}(@{lbi.Frame.Landblock:X8}) -> self (@{destination.Landblock:X8})");
                }
                // dungeon -> landscape
                else if (srcIsDungeon && dstIsLandscape && !dstIsDungeon) {
                    dungeonToLandscape++;
                    travelType = PortalTravelType.DungeonToLandscape;
                }
                // dungeon -> other dungeon
                else if (srcIsDungeon && dstIsDungeon) {
                    dungeonToDungeon++;
                    travelType = PortalTravelType.DungeonToDungeon;
                    //Program.Log($"Found Dungeon->Dungeon: (InstId:{lbi.Guid:X8}//WCID:{weenie.Id}) {UBCommon.Data.DungeonsNames.GetName(lbi.Frame.Landblock)}({lbi.Frame.Landblock:X8}) -> {UBCommon.Data.DungeonsNames.GetName(destination.Landblock)}({destination.Landblock:X8})");
                }
                // landscape -> landscape
                else if (!srcIsDungeon && srcIsLandscape && dstIsLandscape && !dstIsDungeon) {
                    landscapeToLandscape++;
                    travelType = PortalTravelType.LandscapeToLandscape;
                }
                // landscape -> dungeon
                else if (!srcIsDungeon && srcIsLandscape && dstIsDungeon) {
                    landscapeToDungeon++;
                    travelType = PortalTravelType.LandscapeToDungeon;
                }
                else {
                    unknown++;
                    continue;
                }

                var portal = new Portal() {
                    WeenieId = weenie.Id,
                    Frame = lbi.Frame,
                    Destination = new Frame(destination.Landblock, destination.LocalX, destination.LocalY, destination.LocalZ, destination.Heading),
                    Bitmask = (PortalBitmask)weenie.GetProp(PropertyInt.PortalBitmask),
                    TravelType = travelType,
                    Name = weenie.GetProp(PropertyString.Name),
                    LocationType = (travelType != PortalTravelType.LandscapeToLandscape && travelType != PortalTravelType.LandscapeToDungeon) ? LocationType.Dungeon : LocationType.Landscape,
                    Requirements = new UseRequirements() {
                        MinLevel = minLevel,
                        QuestRestriction = questRestriction
                    }
                };

                ContentStore.Portals.Add(portal);

                //Program.Log($"{lbi.Landblock:X4} {srcLbFlags} -> {destination.ObjCellId >> 16:X4} {dstLbFlags}");
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to find portals:");
            Program.Log($"\t Dungeon: \t\t{dungeonInternal + dungeonToLandscape + dungeonToDungeon:N0}");
            Program.Log($"\t\t To Itself: \t\t{dungeonInternal:N0}");
            Program.Log($"\t\t To Landscape: \t\t{dungeonToLandscape:N0}");
            Program.Log($"\t\t To Dungeon: \t\t{dungeonToDungeon:N0}");
            Program.Log($"\t Landscape: \t\t{landscapeToLandscape + landscapeToDungeon:N0}");
            Program.Log($"\t\t To Landscape: \t\t{landscapeToLandscape:N0}");
            Program.Log($"\t\t To Dungeon: \t\t{landscapeToDungeon:N0}");
            Program.Log($"\t Restricted: \t\t{minLevelRestricted:N0}");
            Program.Log($"\t\t Min Level: \t\t{minLevelRestricted:N0}");
            Program.Log($"\t\t Quest: \t\t{questRestricted:N0}");
            Program.Log($"\t Invalid: \t\t{noDestination + badSrcOrDstPosition + unknown:N0}");
            Program.Log($"\t\t Generated: \t\t{generator:N0}");
            Program.Log($"\t\t No Dest: \t\t{noDestination:N0}");
            Program.Log($"\t\t No Collisions: \t{noCollisions:N0}");
            Program.Log($"\t\t Bad Pos: \t\t{badSrcOrDstPosition:N0}");
            Program.Log($"\t\t Unknown: \t\t{unknown:N0}");
            Program.Log($"\t Total Saved: \t\t{ContentStore.Portals.Count:N0} / {total:N0}\n");
        }
    }
}
