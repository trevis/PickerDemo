﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace UBDataGen.Lib.Scrapers {
    public class GDLEScraper : IScraper {

        private Program.Options.AllDumpOptions Options;

        public GDLEScraper(object opts, DatDumper datDumper) {
            Options = (Program.Options.AllDumpOptions)opts;
        }

        public override void Load() {
            LoadWeenies();
            LoadLandblockInstances();
            base.Load();
        }

        uint guid = 1;
        private void LoadLandblockInstances() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("Loading LandblockInstances from spawnmap jsons... ", false);

            var spawnMapsDir = Path.Combine(Options.GDLEContentDirectory, "spawnmaps");
            var worldSpawnsFile = Path.Combine(Options.GDLEContentDirectory, "worldspawns.json");

            if (!ACE.Adapter.GDLE.GDLELoader.TryLoadWorldSpawns(worldSpawnsFile, out ACE.Adapter.GDLE.Models.WorldSpawns worldSpawns)) {
                Program.Log($"Failed to Load and Convert WorldSpawns");
            }
            else {
                foreach (var lb in worldSpawns.Landblocks) {
                    List<ACE.Database.Models.World.LandblockInstanceLink> links;
                    List<ACE.Database.Models.World.LandblockInstance> instances;
                    if (ACE.Adapter.GDLE.GDLEConverter.TryConvert(lb, out instances, out links)) {
                        foreach (var lbi in instances) {
                            var nid = guid++;
                            landblockInstanceCache[nid] = ACEScraper.ConvertLandblockInstance(lbi);
                            landblockInstanceCache[nid].Guid = nid;
                        }
                    }
                }
            }

            List<ACE.Database.Models.World.LandblockInstanceLink> landblockLinks;
            List<ACE.Database.Models.World.LandblockInstance> landblockInstances;
            if (!ACE.Adapter.GDLE.GDLELoader.TryLoadLandblocksConverted(spawnMapsDir, out landblockInstances, out landblockLinks)) {
                Program.Log($"Failed to Load and Convert LandblockInstances");
            }
            else {
                foreach (var lbi in landblockInstances) {
                    var nid = guid++;
                    landblockInstanceCache[nid] = ACEScraper.ConvertLandblockInstance(lbi);
                    landblockInstanceCache[nid].Guid = nid;
                }
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load {landblockInstanceCache.Count:N0} LandblockInstances");
        }

        private void LoadWeenies() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("Loading Weenies from weenies jsons... ", false);
            var spawnMapsDir = Path.Combine(Options.GDLEContentDirectory, "weenies");
            List<ACE.Database.Models.World.Weenie> weenies;

            if (!ACE.Adapter.Lifestoned.LifestonedLoader.TryLoadWeeniesConvertedInParallel(spawnMapsDir, out weenies)) {
                Program.Log($"Failed to Load and Convert Weenies");
            }
            else {
                foreach (var weenie in weenies) {
                    weenieCache[weenie.ClassId] = ACEScraper.ConvertWeenie(weenie);
                }
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load {weenieCache.Count:N0} Weenies");
        }
    }
}
