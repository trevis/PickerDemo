﻿using ACE.Database;
using ACE.Database.Adapter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UtilityBelt.Navigation.Shared.Data;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Enums;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace UBDataGen.Lib.Scrapers {
    public class ACEScraper : IScraper {
        public ACE.Database.Models.World.WorldDbContext DBContext { get; private set; }

        private Program.Options.AllDumpOptions Options;
        public string Type { get; } = "ace";

        public string OutputFilePath { get => Path.Combine(Options.OutputDirectory, OutputFile); }

        public string OutputFile { get => $"content_{Type}.ubd"; }

        public ACEScraper(object opts, LandblockInfoStore lbStore, LandblockMeshStore meshStore) {
            LandblockStore = lbStore;
            MeshStore = meshStore;
            Options = (Program.Options.AllDumpOptions)opts;

            ACE.Common.ConfigManager.Initialize(new ACE.Common.MasterConfiguration() {
                MySql = new ACE.Common.DatabaseConfiguration() {
                    World = new ACE.Common.MySqlConfiguration() {
                        Database = Options.ACEDatabaseName,
                        Host = Options.ACEDatabaseHost,
                        Port = Options.ACEDatabasePort,
                        Username = Options.ACEDatabaseUser,
                        Password = Options.ACEDatabasePassword
                    }
                },
                Server = new ACE.Common.GameConfiguration(),
                Offline = new ACE.Common.OfflineConfiguration()
            });

            DBContext = new ACE.Database.Models.World.WorldDbContext();
        }

        public override void Load() {
            Program.Log($"Loading EmuContent({Type})... ", false);
            var sw = new Stopwatch();
            sw.Start();

            if (ContentStore.TryDeserialize(OutputFilePath)) {
                sw.Stop();
                Program.Log($"Loaded {ContentStore.Portals.Count:N0} portals, {ContentStore.Doors.Count:N0} doors, from cache {OutputFilePath} in {sw.ElapsedMilliseconds / 1000f:N3}s");
                return;
            }

            Program.Log("");
            LoadWeenies();
            LoadLandblockInstances();
            base.Load();
            
            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load {ContentStore.Portals.Count:N0} portals, {ContentStore.Doors.Count:N0} doors");

            ContentStore.TrySerialize(OutputFilePath);

            FileInfo fi = new FileInfo(OutputFilePath);
            Program.Log($"Saved cached ContentStore to {OutputFilePath} ({fi.Length:N0} bytes)\n");
        }

        private void LoadLandblockInstances() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("\tLoading LandblockInstances from db... ", false);
            DBContext.LandblockInstance.Load();
            DBContext.LandblockInstanceLink.Load();

            foreach (var lbi in DBContext.LandblockInstance) {
                landblockInstanceCache[lbi.Guid] = ConvertLandblockInstance(lbi);
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load {landblockInstanceCache.Count:N0} LandblockInstances");
        }

        private void LoadWeenies() {
            var sw = new Stopwatch();
            sw.Start();

            Program.Log("\tLoading Weenies from db... ", false);
            DBContext.Weenie.Load();
            DBContext.WeeniePropertiesPosition.Load();
            DBContext.WeeniePropertiesBool.Load();
            DBContext.WeeniePropertiesInt.Load();
            DBContext.WeeniePropertiesString.Load();
            DBContext.WeeniePropertiesDID.Load();

            foreach (var weenie in DBContext.Weenie) {
                weenieCache[weenie.ClassId] = ConvertWeenie(weenie);
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load {weenieCache.Count:N0} weenies");
        }

        public static LandblockInstance ConvertLandblockInstance(ACE.Database.Models.World.LandblockInstance lbi) {
            return new LandblockInstance() {
                Guid = lbi.Guid,
                WeenieClassId = lbi.WeenieClassId,
                Frame = new Frame(lbi.ObjCellId, lbi.OriginX, lbi.OriginY, lbi.OriginZ, Helpers.QuaternionToHeading(new Quaternion(lbi.AnglesX, lbi.AnglesY, lbi.AnglesZ, lbi.AnglesW))),
                IsLinkChild = lbi.IsLinkChild,
                Links = lbi.LandblockInstanceLink.Select(s => new LandblockInstanceLink() {
                    ParentGuid = s.ParentGuid,
                    ChildGuid = s.ChildGuid
                }).ToList()
            };
        }

        public static Weenie ConvertWeenie(ACE.Database.Models.World.Weenie weenie) {
            var cweenie = WeenieConverter.ConvertToEntityWeenie(weenie);
            var boolProps = new Dictionary<PropertyBool, bool>();
            var intProps = new Dictionary<PropertyInt, int>();
            var stringProps = new Dictionary<PropertyString, string>();
            var positions = new Dictionary<PositionType, Frame>();
            if (cweenie.PropertiesBool != null) {
                foreach (var prop in cweenie.PropertiesBool) {
                    boolProps.Add((PropertyBool)prop.Key, prop.Value);
                }
            }
            if (cweenie.PropertiesInt != null) {
                foreach (var prop in cweenie.PropertiesInt) {
                    intProps.Add((PropertyInt)prop.Key, prop.Value);
                }
            }
            if (cweenie.PropertiesString != null) {
                foreach (var prop in cweenie.PropertiesString) {
                    stringProps.Add((PropertyString)prop.Key, prop.Value);
                }
            }
            if (cweenie.PropertiesPosition != null) {
                foreach (var prop in cweenie.PropertiesPosition) {
                    positions.Add((PositionType)prop.Key, new Frame(prop.Value.ObjCellId, prop.Value.PositionX, prop.Value.PositionY, prop.Value.PositionZ, Helpers.QuaternionToHeading(new Quaternion(prop.Value.RotationX, prop.Value.RotationY, prop.Value.RotationZ, prop.Value.RotationW))));
                }
            }
            
            return new Weenie() {
                Id = cweenie.WeenieClassId,
                Type = (WeenieType)cweenie.WeenieType,
                SetupId = cweenie.PropertiesDID[ACE.Entity.Enum.Properties.PropertyDataId.Setup],
                BoolProps = boolProps,
                IntProps = intProps,
                StringProps = stringProps,
                Positions = positions
            };
        }
    }
}
