﻿using ACE.DatLoader.FileTypes;
using ClipperLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Navigation.Shared.Data;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Enums;
using System.Numerics;

namespace UBDataGen.Lib {
    public class LandblockMeshGen {
        static internal uint CellSize = 24;
        static internal uint BlockSize = 8 * CellSize;

        public DatDumper DatDumper { get; }
        public string OutputDirectory { get; }
        public string Type { get; }

        protected Random rand;

        public LandblockMeshStore MeshStore { get; } = new LandblockMeshStore();

        public string OutputFilePath { get => Path.Combine(OutputDirectory, OutputFile); }

        public string OutputFile { get => $"landblockmesh_{Type}.ubd"; }

        public LandblockMeshGen(DatDumper datDumper, string outputDirectory, string type) {
            DatDumper = datDumper;
            OutputDirectory = outputDirectory;
            Type = type;
            rand = new Random();
        }

        public virtual void Load() {
            Program.Log($"Loading LandblockMesh... ", false);
            var sw = new Stopwatch();
            sw.Start();

            if (MeshStore.TryDeserialize(OutputFilePath)) {
                sw.Stop();
                Program.Log($"Loaded {MeshStore.Polygons.Count:N0} cached polys from {OutputFilePath} in {sw.ElapsedMilliseconds / 1000f:N3}s");
                return;
            }

            var polys = BuildLandmassMesh();

            //DrawLandmassMesh(polys, "final");

            MeshStore.Polygons.AddRange(polys.Values);
            MeshStore.TrySerialize(OutputFilePath);

            FileInfo fi = new FileInfo(OutputFilePath);
            Program.Log($"Saved cached MeshStore to {OutputFilePath} ({fi.Length:N0} bytes)\n");
        }

        protected ConcurrentDictionary<long, Polygon2d> BuildLandmassMesh() {
            ConcurrentDictionary<long, Polygon2d> polys = BuildLandmassPolys();

            SimplifyLandmassPolys(ref polys);

            return polys;
        }

        protected void SimplifyLandmassPolys(ref ConcurrentDictionary<long, Polygon2d> polys) {
            SimplifyLandmassPolysbyChunk(ref polys, false);
        }

        protected virtual void SimplifyLandmassPolysbyChunk(ref ConcurrentDictionary<long, Polygon2d> polys, bool doSkip = true) {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\tSimplifying {polys.Count:N0} polys... ", false);
            var polysByChunk = new Dictionary<int, ConcurrentDictionary<long, Polygon2d>>();
            foreach (var poly in polys.Values) {
                if (!polysByChunk.ContainsKey(poly.Lbid))
                    polysByChunk.Add(poly.Lbid, new ConcurrentDictionary<long, Polygon2d>());
                polysByChunk[poly.Lbid].TryAdd(poly.Id, poly);
            }

            var i = 0;
            //Parallel.ForEach(polysByChunk, (kv, state) => {
            foreach (var kv in polysByChunk) {
                //var sw1 = new Stopwatch();
                //sw1.Start();
                //var startCount = kv.Value.Count;
                while (SimplifyPolys(kv.Value, doSkip)) ;
                uint _regionId = ((uint)kv.Value.Values.First().Lbid << 16);
                while (FixRegions(kv.Value, ref _regionId));
                //Program.Log($"Fixed... {_regionId:X8}");
                //sw1.Stop();
                //Program.Log($"({i++}/{polysByChunk.Count}) Simplified poly chunk {startCount} polys -> {polysByChunk[kv.Key].Count} polys in {sw1.ElapsedMilliseconds / 1000f:N3}s");
            };

            polys.Clear();
            foreach (var kv in polysByChunk) {
                foreach (var p in kv.Value.Values)
                    polys.TryAdd(p.Id, p);
            }

            //DrawLandmassMesh(polys, $"{chunkSize}");

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to simplify into {polys.Count:N0} nodes with {polys.Values.Select(p => p.RegionId).Distinct().Count()} regions");
        }

        protected bool FixRegions(ConcurrentDictionary<long, Polygon2d> polys, ref uint _regionId) {
            if (polys.Count == 1) {
                polys.Values.First().RegionId = (++_regionId);
                return false;
            }

            foreach (var currentPoly in polys.Values) {
                foreach (var checkPoly in polys.Values.Where(p => p.RegionId != 0)) {
                    if (currentPoly == checkPoly || currentPoly.RegionId == checkPoly.RegionId)
                        continue;

                    if (currentPoly.OverlapsOrTouches(checkPoly) || currentPoly.SharesVerticeWith(checkPoly)) {
                        if (currentPoly.RegionId != 0) {
                            if (checkPoly.RegionId != 0) {
                                foreach (var p in polys.Values.Where(x => x.RegionId == checkPoly.RegionId)) { 
                                    p.RegionId = currentPoly.RegionId;
                                }
                            }
                            checkPoly.RegionId = currentPoly.RegionId;
                            return true;
                        }
                        else if (checkPoly.RegionId != 0) {
                            if (currentPoly.RegionId != 0) {
                                foreach (var p in polys.Values.Where(x => x.RegionId == currentPoly.RegionId)) {
                                    p.RegionId = checkPoly.RegionId;
                                }
                            }
                            currentPoly.RegionId = checkPoly.RegionId;
                            return true;
                        }
                        //Program.Log($"Shared regionId: {currentPoly.RegionId:X8}");
                    }
                }

                if (currentPoly.RegionId == 0) {
                    currentPoly.RegionId = (++_regionId);
                    return true;
                }
            }
            return false;
        }

        protected virtual bool SimplifyPolys(ConcurrentDictionary<long, Polygon2d> polys, bool skipTris = true) {
            var cvals = polys.Values.ToList();

            if (cvals.Count == 1)
                return false;

            var c = new Clipper();
            var solution = new List<List<IntPoint>>();

            c.AddPolygon(cvals.First().Vertices.Select(v => new IntPoint(v.X, v.Y)).ToList(), PolyType.ptSubject);
            foreach (Polygon2d checkPoly in cvals.Skip(1)) {
                c.AddPolygon(checkPoly.Vertices.Select(v => new IntPoint(v.X, v.Y)).ToList(), PolyType.ptClip);
            }
            c.Execute(ClipType.ctUnion, solution);

            if (solution.Count > 0) {
                polys.Clear();
                for (var i = 0; i < solution.Count; i++) {
                    var p = new Polygon2d(solution[i].Select(i => new Vector2i((int)i.X, (int)i.Y)).ToList(), cvals.First().Lbid, 0);
                    polys.TryAdd(p.Id, p);
                }
            }

            return false;
        }

        protected virtual ConcurrentDictionary<long, Polygon2d> BuildLandmassPolys() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\n\tBuilding landmass polys... ", false);
            ConcurrentDictionary<long, Polygon2d> polys = new ConcurrentDictionary<long, Polygon2d>();

            //landblocks
            for (ushort x = 0; x < 255; x++) {
                for (ushort y = 0; y < 255; y++) {
                    var lbid = (ushort)((x << 8) + y);
                    var lbPolys = new List<Polygon2d>();
                    var lbNodes = new List<LBNode>();

                    // magic from ace
                    uint seedA = (uint)(((int)x << 3) * 214614067);
                    uint seedB = (uint)(((int)x << 3) * 1109124029);

                    var landblockData = DatDumper.CellDat.ReadFromDat<CellLandblock>(((uint)lbid << 16) | 0x0000FFFF);

                    if (landblockData.Terrain.All(t => !TerrainTypeIsWalkable(t)))
                        continue;

                    if (landblockData.Height != null && landblockData.Height.Count > 0) {
                        var vertices = ConstructVertices(landblockData);

                        //landcells
                        for (uint tileX = 0; tileX < 8; tileX++) {
                            for (uint tileY = 0; tileY < 8; tileY++) {
                                var lcid = ((uint)lbid << 16) + (tileX << 8) + tileY;
                                var magicB = seedB;
                                var magicA = seedA + 1813693831;

                                if (landblockData.Terrain.All(t => TerrainIsWater(t)))
                                    continue;

                                var splitDir = (uint)(((int)y << 3) + tileY) * magicA - magicB - 1369149221;

                                List<int> tri1_i, tri2_i;

                                int i1 = (int)((tileX * 9) + tileY);
                                int i2 = (int)(((tileX + 1) * 9) + tileY);
                                int i3 = (int)(((tileX) * 9) + (tileY + 1));
                                int i4 = (int)(((tileX + 1) * 9) + tileY + 1);

                                if (splitDir * 2.3283064e-10 < 0.5f) {
                                    tri1_i = new List<int>() { i1, i2, i3 };
                                    tri2_i = new List<int>() { i4, i3, i2 };
                                }
                                else {
                                    tri1_i = new List<int>() { i1, i2, i4 };
                                    tri2_i = new List<int>() { i1, i4, i3 };
                                }

                                var cPolys = DrawCellTris(landblockData, vertices, x, y, (int)tileX, (int)tileY, tri1_i, tri2_i);

                                lbPolys.AddRange(cPolys);
                            }
                            seedA += 214614067;
                            seedB += 1109124029;
                        }

                        //Program.Log($"{count} vs {8 *8 *2}");

                        foreach (var lbPoly in lbPolys)
                            polys.TryAdd(lbPoly.Id, lbPoly);
                    }
                }
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build {polys.Count:N0} polys");

            return polys;
        }

        protected List<Polygon2d> DrawCellTris(CellLandblock landblockData, List<Vector3> vertices, int x, int y, int tileX, int tileY, List<int> tri1_t, List<int> tri2_t) {
            var polys = new List<Polygon2d>();
            var t1 = new List<Vector2i>() {
                                        new Vector2i((int)(x * BlockSize + vertices[tri1_t[0]].X), (int)(y * BlockSize + vertices[tri1_t[0]].Y)),
                                        new Vector2i((int)(x * BlockSize + vertices[tri1_t[1]].X), (int)(y * BlockSize + vertices[tri1_t[1]].Y)),
                                        new Vector2i((int)(x * BlockSize + vertices[tri1_t[2]].X), (int)(y * BlockSize + vertices[tri1_t[2]].Y))
                                    };
            var t2 = new List<Vector2i>() {
                                        new Vector2i((int)(x * BlockSize + vertices[tri2_t[0]].X), (int)(y * BlockSize + vertices[tri2_t[0]].Y)),
                                        new Vector2i((int)(x * BlockSize + vertices[tri2_t[1]].X), (int)(y * BlockSize + vertices[tri2_t[1]].Y)),
                                        new Vector2i((int)(x * BlockSize + vertices[tri2_t[2]].X), (int)(y * BlockSize + vertices[tri2_t[2]].Y))
                                    };
            var tri1 = new List<Vector3>() { vertices[tri1_t[0]], vertices[tri1_t[1]], vertices[tri1_t[2]] };
            var tri2 = new List<Vector3>() { vertices[tri2_t[0]], vertices[tri2_t[1]], vertices[tri2_t[2]] };

            var lbId = (ushort)(((ushort)x << 8) + y);

            MeshStore.CellHeights[(x * 8) + tileX, (y * 8) + tileY] = (ushort)Math.Round((tri1[0].Z + tri1[1].Z + tri1[2].Z + tri2[0].Z + tri2[1].Z + tri2[2].Z) / 6.0);

            AddCellPolys(landblockData, lbId, t1, tri1_t, tri1, ref polys);
            AddCellPolys(landblockData, lbId, t2, tri2_t, tri2, ref polys);
            return polys;
        }

        protected virtual void AddCellPolys(CellLandblock landblockData, ushort lbId, List<Vector2i> t1, List<int> tri1_t, List<Vector3> tri1, ref List<Polygon2d> polys) {
            if (TriIsWalkable(landblockData, tri1_t, tri1)) {
                // walkable water
                if (TerrainIsWater(landblockData.Terrain[tri1_t[0]]) || TerrainIsWater(landblockData.Terrain[tri1_t[1]]) || TerrainIsWater(landblockData.Terrain[tri1_t[2]])) {
                    polys.Add(new Polygon2d(t1, lbId));
                }
                // walkable land
                else {
                    polys.Add(new Polygon2d(t1, lbId));
                }
            }
            // steep slope
            //else if (!TriSlopeIsWalkable(tri1)) {
            //polys.Add(new Polygon2d(t1, "black", lbId));
            //}
        }

        public float GetLandHeight(byte height) {
            return DatDumper.RegionDesc.LandDefs.LandHeightTable[height];
        }

        internal virtual bool TerrainTypeIsWalkable(ushort t) {
            TerrainType tType = (TerrainType)((t >> 2) & 0x3F);
            return tType != TerrainType.WaterDeepSea;
        }

        internal static bool TerrainIsWater(ushort t) {
            TerrainType tType = (TerrainType)((t >> 2) & 0x3F);
            return tType == TerrainType.WaterDeepSea || tType == TerrainType.WaterShallowStillSea || tType == TerrainType.WaterRunning || tType == TerrainType.WaterShallowSea || tType == TerrainType.WaterStandingFresh;
        }

        internal virtual bool TriIsWalkable(CellLandblock landblockData, List<int> tri_t, List<Vector3> tri) {
            // for (var i = 0; i < 3; i++) {
            //     if (!TerrainTypeIsWalkable(landblockData.Terrain[tri_t[i]]))
            //         return false;
            // }
            return TriSlopeIsWalkable(tri);
        }

        internal bool TriSlopeIsWalkable(List<Vector3> tri) {
            var t0 = new Vector3(tri[0].X * (24f / CellSize), tri[0].Y * (24f / CellSize), tri[0].Z);
            var t1 = new Vector3(tri[1].X * (24f / CellSize), tri[1].Y * (24f / CellSize), tri[1].Z);
            var t2 = new Vector3(tri[2].X * (24f / CellSize), tri[2].Y * (24f / CellSize), tri[2].Z);
            return Vector3.Normalize(Vector3.Cross(t0 - t1, t1 - t2)).Z >= 0.66417414618662751f;
        }

        internal List<Vector3> ConstructVertices(CellLandblock landblockData) {
            List<Vector3> vertices = new List<Vector3>();
            for (var x = 0; x < 9; x++) {
                for (var y = 0; y < 9; y++) {
                    var z = GetLandHeight(landblockData.Height[((x * 9) + y)]);
                    vertices.Add(new Vector3());
                    vertices[(x * 9) + y] = new Vector3(x * CellSize, y * CellSize, z);
                }
            }

            return vertices;
        }

        protected virtual void DrawLandmassMesh(ConcurrentDictionary<long, Polygon2d> polys, string filename) {
            var regionColors = new Dictionary<uint, System.Drawing.Color>();
            var scale = 1 / 12f;
            var mapSize = (int)(255 * BlockSize * scale);
            var lbPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(60, System.Drawing.Color.Red)), 1);
            var lcPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(50, System.Drawing.Color.Orange)), 1);
            var lbFont = new System.Drawing.Font("mono", 7, System.Drawing.FontStyle.Bold);
            var lbFontBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(200, System.Drawing.Color.White));
            var backgroundBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, System.Drawing.Color.Black));
            var walkablePen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, System.Drawing.Color.Green)), 1);

            using (var brush = new System.Drawing.SolidBrush(System.Drawing.Color.Red))
            using (var bmp = new System.Drawing.Bitmap(mapSize, mapSize))
            using (var gfx = System.Drawing.Graphics.FromImage(bmp)) {
                gfx.FillRectangle(backgroundBrush, new System.Drawing.Rectangle(0, 0, mapSize, mapSize));

                foreach (var poly in polys.Values) {
                    if (!regionColors.ContainsKey(poly.RegionId)) {
                        var r = rand.Next(90, 255);
                        var g = rand.Next(90, 255);
                        var b = rand.Next(90, 255);
                        regionColors.Add(poly.RegionId, System.Drawing.Color.FromArgb(255, r, g, b));
                    }

                    brush.Color = regionColors[poly.RegionId];

                    try {
                        gfx.FillPolygon(brush, poly.Vertices.Select(p => new System.Drawing.PointF(p.X * scale, mapSize - (p.Y * scale))).ToArray());
                    }
                    catch (Exception ex) {
                        var ps = poly.Vertices.Select(p => $"{p.X * scale}, {mapSize - (p.Y * scale)}");
                        Program.Log($"Tried to draw poly: {String.Join(", ", ps)}");
                    }
                }
                /*
                for (var x = 0; x < 255; x++) {
                    for (var y = 0; y < 255; y++) {
                        var lbid = (ushort)((x << 8) + (254 - y));
                        gfx.DrawRectangle(lbPen, new System.Drawing.Rectangle((int)(x * 192 * scale), (int)(y * 192 * scale), (int)(192 * scale), (int)(192 * scale)));
                        gfx.DrawString($"{lbid:X4}", lbFont, lbFontBrush, (int)(x * 192 * scale) + 2, (int)(y * 192 * scale) + 2);
                    }
                }
                //*/

                gfx.Save();
                bmp.Save(Path.Combine(OutputDirectory, $"landscape_{filename}.png"), System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }
}
