﻿using ACE.DatLoader.FileTypes;
using ClipperLib;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UtilityBelt.Navigation.Shared.Models;
using System.Numerics;

namespace UBDataGen.Lib {
    public class AsheronDbLandscapeGen : LandblockMeshGen {

        public AsheronDbLandscapeGen(DatDumper datDumper, string outputDirectory, string type) : base(datDumper, outputDirectory, type) {

        }

        public override void Load() {
            Program.Log($"Loading AsheronDBPolys... ", false);
            var sw = new Stopwatch();
            sw.Start();

            //if (MeshStore.TryDeserialize(OutputFilePath)) {
            //    sw.Stop();
            //    Program.Log($"Loaded {MeshStore.Polygons.Count:N0} cached polys from {OutputFilePath} in {sw.ElapsedMilliseconds / 1000f:N3}s");
            //    return;
            //}

            ConcurrentDictionary<long, Polygon2d> landPolys = BuildLandmassPolys();

            SimplifyLandmassPolysbyChunkSize(ref landPolys, 1);
            SimplifyLandmassPolysbyChunkSize(ref landPolys, 4);
            SimplifyLandmassPolysbyChunkSize(ref landPolys, 8);

            GenerateGeoJSON(landPolys, "asherondb.slopes");
            DrawLandmassMesh(landPolys, "asherondb.slopes");

            Program.Log($"Simplified slopes to {landPolys.Count:N0} polys and {landPolys.Sum(p => p.Value.Vertices.Count):N0} vertices");

            ConcurrentDictionary<long, Polygon2d> waterPolys = BuildWaterPolys();

            SimplifyLandmassPolysbyChunkSize(ref waterPolys, 8);
            SimplifyLandmassPolysbyChunkSize(ref waterPolys, 32);
            SimplifyLandmassPolysbyChunkSize(ref waterPolys, 256);

            GenerateGeoJSON(waterPolys, "asherondb.water");
            DrawLandmassMesh(waterPolys, "asherondb.water");

            Program.Log($"Simplified water to {waterPolys.Count:N0} polys and {waterPolys.Sum(p => p.Value.Vertices.Count):N0} vertices");


            ConcurrentDictionary<long, Polygon2d> allPolys = new ConcurrentDictionary<long, Polygon2d>();
            foreach (var p in landPolys)
                allPolys.TryAdd(p.Key, p.Value);
            foreach (var p in waterPolys)
                allPolys.TryAdd(p.Key, p.Value);
            DrawUnwalkableOverlay(allPolys, "asherondb.unwalkable");


            //MeshStore.Polygons.AddRange(polys.Values);
            //MeshStore.TrySerialize(OutputFilePath);

            //FileInfo fi = new FileInfo(OutputFilePath);
            //Program.Log($"Saved cached MeshStore to {OutputFilePath} ({fi.Length:N0} bytes)\n");
        }

        private void GenerateGeoJSON(ConcurrentDictionary<long, Polygon2d> polys, string filename) {
            var geo = new GeoJSON();
            var mod = (float)1f / 2f / 8f;
            foreach (var poly in polys) {
                var verts = poly.Value.Vertices.ToList();

                for (var i = 0; i < poly.Value.Vertices.Count; i++) {
                    poly.Value.Vertices[i] = new Vector2i() {
                        X = poly.Value.Vertices[i].X,
                        Y = poly.Value.Vertices[i].Y
                    };
                }

                if (!poly.Value.CCW)
                    verts.Reverse();

                var coords = verts.Select(v => new List<float>() {
                    v.X * mod,
                    v.Y * mod
                }).ToList();

                var first = verts.First();
                coords.Add(new List<float>() {
                    first.X * mod,
                    first.Y * mod
                });
                geo.features.Add(new Feature() {
                    geometry = new Geometry() {
                        coordinates = new List<List<List<float>>>() {
                            coords
                        }
                    }
                });
            }

            string output = JsonConvert.SerializeObject(geo, Formatting.None);
            File.WriteAllText(Path.Combine(OutputDirectory, $"{filename}.json"), output);
        }

        protected ConcurrentDictionary<long, Polygon2d> BuildWaterPolys() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\tBuilding water polys... ", false);
            ConcurrentDictionary<long, Polygon2d> polys = new ConcurrentDictionary<long, Polygon2d>();

            //landblocks
            for (ushort x = 0; x < 255; x++) {
                for (ushort y = 0; y < 255; y++) {
                    var lbid = (ushort)((x << 8) + y);
                    var lbPolys = new List<Polygon2d>();
                    var lbNodes = new List<LBNode>();

                    // magic from ace
                    uint seedA = (uint)(((int)x << 3) * 214614067);
                    uint seedB = (uint)(((int)x << 3) * 1109124029);

                    var landblockData = DatDumper.CellDat.ReadFromDat<CellLandblock>(((uint)lbid << 16) | 0x0000FFFF);

                    if (landblockData.Terrain.All(t => TerrainIsWater(t))) {
                        var p = new Polygon2d(new List<Vector2i>() {
                                            new Vector2i((int)(x * BlockSize), (int)(y * BlockSize)),
                                            new Vector2i((int)(x * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)(y * BlockSize)),
                                        }, lbid);
                        polys.TryAdd(p.Id, p);
                        continue;
                    }

                }
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build {polys.Count:N0} polys");

            return polys;
        }

        protected override ConcurrentDictionary<long, Polygon2d> BuildLandmassPolys() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\n\tBuilding landmass polys... ", false);
            ConcurrentDictionary<long, Polygon2d> polys = new ConcurrentDictionary<long, Polygon2d>();

            //landblocks
            for (ushort x = 0; x < 255; x++) {
                for (ushort y = 0; y < 255; y++) {
                    var lbid = (ushort)((x << 8) + y);
                    var lbPolys = new List<Polygon2d>();
                    var lbNodes = new List<LBNode>();

                    // magic from ace
                    uint seedA = (uint)(((int)x << 3) * 214614067);
                    uint seedB = (uint)(((int)x << 3) * 1109124029);

                    var landblockData = DatDumper.CellDat.ReadFromDat<CellLandblock>(((uint)lbid << 16) | 0x0000FFFF);

                    if (landblockData.Terrain.All(t => TerrainIsWater(t))) {
                        continue;
                    }

                    if (landblockData.Height != null && landblockData.Height.Count > 0) {
                        var vertices = ConstructVertices(landblockData);

                        //landcells
                        for (uint tileX = 0; tileX < 8; tileX++) {
                            for (uint tileY = 0; tileY < 8; tileY++) {
                                var lcid = ((uint)lbid << 16) + (tileX << 8) + tileY;
                                var magicB = seedB;
                                var magicA = seedA + 1813693831;

                                var splitDir = (uint)(((int)y << 3) + tileY) * magicA - magicB - 1369149221;

                                List<int> tri1_i, tri2_i;

                                int i1 = (int)((tileX * 9) + tileY);
                                int i2 = (int)(((tileX + 1) * 9) + tileY);
                                int i3 = (int)(((tileX) * 9) + (tileY + 1));
                                int i4 = (int)(((tileX + 1) * 9) + tileY + 1);

                                if (splitDir * 2.3283064e-10 < 0.5f) {
                                    tri1_i = new List<int>() { i1, i2, i3 };
                                    tri2_i = new List<int>() { i4, i3, i2 };
                                }
                                else {
                                    tri1_i = new List<int>() { i1, i2, i4 };
                                    tri2_i = new List<int>() { i1, i4, i3 };
                                }

                                var cPolys = DrawCellTris(landblockData, vertices, x, y, (int)tileX, (int)tileY, tri1_i, tri2_i);

                                if (cPolys.Count == 2) {
                                    // simplify
                                    lbPolys.Add(new Polygon2d(new List<Vector2i>() {
                                            new Vector2i((int)(x * BlockSize + tileX * CellSize), (int)(y * BlockSize + tileY * CellSize)),
                                            new Vector2i((int)(x * BlockSize + tileX * CellSize), (int)(y * BlockSize + (tileY + 1) * CellSize)),
                                            new Vector2i((int)(x * BlockSize + (tileX + 1) * CellSize), (int)(y * BlockSize + (tileY + 1) * CellSize)),
                                            new Vector2i((int)(x * BlockSize + (tileX + 1) * CellSize), (int)(y * BlockSize + tileY * CellSize)),
                                        }, lbid));
                                }
                                else {
                                    lbPolys.AddRange(cPolys);
                                }
                            }
                            seedA += 214614067;
                            seedB += 1109124029;
                        }

                        //Program.Log($"{count} vs {8 *8 *2}");

                        if (lbPolys.Count == 8 * 8) {
                            // simplified walkable landblock poly
                            var p = new Polygon2d(new List<Vector2i>() {
                                            new Vector2i((int)(x * BlockSize), (int)(y * BlockSize)),
                                            new Vector2i((int)(x * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)(y * BlockSize)),
                                        }, lbid);
                            polys.TryAdd(p.Id, p);
                        }
                        else {
                            foreach (var lbPoly in lbPolys)
                                polys.TryAdd(lbPoly.Id, lbPoly);
                        }
                    }
                    else {
                        var p = new Polygon2d(new List<Vector2i>() {
                                            new Vector2i((int)(x * BlockSize), (int)(y * BlockSize)),
                                            new Vector2i((int)(x * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)((y + 1) * BlockSize)),
                                            new Vector2i((int)((x + 1) * BlockSize), (int)(y * BlockSize)),
                                        }, lbid);
                        polys.TryAdd(p.Id, p);
                    }
                }
            }

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build {polys.Count:N0} polys");

            return polys;
        }

        protected void SimplifyLandmassPolysbyChunkSize(ref ConcurrentDictionary<long, Polygon2d> polys, int chunkSize = 1) {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\tSimplifying {polys.Count:N0} polys by lbChunkSize {chunkSize}... ", false);
            var maxChunks = (255 / chunkSize);
            var polysByChunk = new Dictionary<int, ConcurrentDictionary<long, Polygon2d>>();
            foreach (var poly in polys.Values) {
                var x = poly.Lbid >> 8;
                var y = poly.Lbid & 0xFF;
                x /= chunkSize;
                y /= chunkSize;
                var chunkId = (x * maxChunks) + y;
                if (!polysByChunk.ContainsKey(chunkId))
                    polysByChunk.Add(chunkId, new ConcurrentDictionary<long, Polygon2d>());
                polysByChunk[chunkId].TryAdd(poly.Id, poly);
            }

            var i = 0;
            //Parallel.ForEach(polysByChunk, (kv, state) => {
            foreach (var kv in polysByChunk) {
                //var sw1 = new Stopwatch();
                //sw1.Start();
                //var startCount = kv.Value.Count;
                checkList.Clear();
                while (SimplifyPolys(kv.Value)) {
                    checkList.Clear();
                };
                //uint _regionId = ((uint)kv.Value.Values.First().Lbid << 16);
                //while (FixRegions(kv.Value, ref _regionId)) ;
                //Program.Log($"Fixed... {_regionId:X8}");
                //sw1.Stop();
                //Program.Log($"({i++}/{polysByChunk.Count}) Simplified poly chunk {startCount} polys -> {polysByChunk[kv.Key].Count} polys in {sw1.ElapsedMilliseconds / 1000f:N3}s");
            };

            polys.Clear();
            foreach (var kv in polysByChunk) {
                foreach (var p in kv.Value.Values)
                    polys.TryAdd(p.Id, p);
            }

            //DrawLandmassMesh(polys, $"{chunkSize}");

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to simplify into {polys.Count:N0} polys with {polys.Values.Sum(p => p.Vertices.Count):N0} vertices");
        }

        List<long> checkList = new List<long>();
        protected override bool SimplifyPolys(ConcurrentDictionary<long, Polygon2d> polys, bool skipTris = true) {
            var cvals = polys.Values.ToList();
            if (cvals.Count <= 1)
                return false;

            var solution = new List<List<IntPoint>>();

            foreach (var currentPoly in cvals) {
                if (checkList.Contains(currentPoly.Id))
                    continue;
                foreach (var checkPoly in cvals) {
                    if (checkList.Contains(checkPoly.Id))
                        continue;
                    if (currentPoly == checkPoly)
                        continue;

                    if (currentPoly.OverlapsOrTouches(checkPoly) || checkPoly.OverlapsOrTouches(currentPoly)) {
                        var c = new Clipper();

                        c.AddPolygon(currentPoly.Vertices.Select(v => new IntPoint(v.X, v.Y)).ToList(), PolyType.ptSubject);
                        c.AddPolygon(checkPoly.Vertices.Select(v => new IntPoint(v.X, v.Y)).ToList(), PolyType.ptClip);

                        c.Execute(ClipType.ctUnion, solution);

                        if (solution.Count == 1) {
                            var p = new Polygon2d(solution[0].Select(i => new Vector2i((int)i.X, (int)i.Y)).ToList(), currentPoly.Lbid, 0);
                            polys.TryRemove(currentPoly.Id, out var _x);
                            polys.TryRemove(checkPoly.Id, out var _xx);
                            polys.TryAdd(p.Id, p);
                            return true;
                        }
                    }
                }
                checkList.Add(currentPoly.Id);
            }

            return false;
        }

        protected override void AddCellPolys(CellLandblock landblockData, ushort lbId, List<Vector2i> t1, List<int> tri1_t, List<Vector3> tri1, ref List<Polygon2d> polys) {
            if (!TriIsWalkable(landblockData, tri1_t, tri1)) {
                polys.Add(new Polygon2d(t1, lbId));
            }
        }

        protected virtual void DrawLandmassMesh(ConcurrentDictionary<long, Polygon2d> polys, string filename) {
            var regionColors = new Dictionary<uint, System.Drawing.Color>();
            var scale = 2;
            var mapSize = (int)(255 * BlockSize * scale);
            var lbPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(60, System.Drawing.Color.Red)), 1);
            var lcPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(50, System.Drawing.Color.Orange)), 1);
            var lbFont = new System.Drawing.Font("mono", 7, System.Drawing.FontStyle.Bold);
            var lbFontBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(200, System.Drawing.Color.White));
            var backgroundBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, System.Drawing.Color.Black));
            var walkablePen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, System.Drawing.Color.Green)), 1);


            using (var brush = new System.Drawing.SolidBrush(System.Drawing.Color.Red))
            using (var bmp = new System.Drawing.Bitmap(mapSize, mapSize))
            using (var gfx = System.Drawing.Graphics.FromImage(bmp)) {
                gfx.FillRectangle(backgroundBrush, new System.Drawing.Rectangle(0, 0, mapSize, mapSize));

                foreach (var poly in polys.Values) {
                    var r = rand.Next(90, 255);
                    var g = rand.Next(90, 255);
                    var b = rand.Next(90, 255);
                    brush.Color = System.Drawing.Color.FromArgb(255, r, g, b);

                    try {
                        gfx.FillPolygon(brush, poly.Vertices.Select(p => new System.Drawing.PointF(p.X * scale, mapSize - (p.Y * scale))).ToArray());
                    }
                    catch {
                        var ps = poly.Vertices.Select(p => $"{p.X * scale}, {mapSize - (p.Y * scale)}");
                        Program.Log($"Tried to draw poly: {String.Join(", ", ps)}");
                    }
                }
                /*
                for (var x = 0; x < 255; x++) {
                    for (var y = 0; y < 255; y++) {
                        var lbid = (ushort)((x << 8) + (254 - y));
                        gfx.DrawRectangle(lbPen, new System.Drawing.Rectangle((int)(x * 192 * scale), (int)(y * 192 * scale), (int)(192 * scale), (int)(192 * scale)));
                        gfx.DrawString($"{lbid:X4}", lbFont, lbFontBrush, (int)(x * 192 * scale) + 2, (int)(y * 192 * scale) + 2);
                    }
                }
                //*/

                gfx.Save();
                filename = Path.Combine(@"./out/", $"landscape_{filename}.png");
                if (File.Exists(filename))
                    File.Delete(filename);
                bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        private void DrawUnwalkableOverlay(ConcurrentDictionary<long, Polygon2d> polys, string filename) {
            var regionColors = new Dictionary<uint, System.Drawing.Color>();
            var scale = 5;
            var mapSize = (int)(255 * BlockSize * scale);
            var lbPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(60, System.Drawing.Color.Red)), 1);
            var lcPen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(50, System.Drawing.Color.Orange)), 1);
            var lbFont = new System.Drawing.Font("mono", 7, System.Drawing.FontStyle.Bold);
            var lbFontBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(200, System.Drawing.Color.White));
            var backgroundBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, System.Drawing.Color.White));
            var walkablePen = new System.Drawing.Pen(new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, System.Drawing.Color.Green)), 1);


            using (var brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(75, System.Drawing.Color.Red)))
            using (var bmp = new System.Drawing.Bitmap(mapSize, mapSize))
            using (var gfx = System.Drawing.Graphics.FromImage(bmp)) {
                gfx.FillRectangle(backgroundBrush, new System.Drawing.Rectangle(0, 0, mapSize, mapSize));

                foreach (var poly in polys.Values) {

                    try {
                        gfx.FillPolygon(brush, poly.Vertices.Select(p => new System.Drawing.PointF(p.X * scale, mapSize - (p.Y * scale))).ToArray());
                    }
                    catch {
                        var ps = poly.Vertices.Select(p => $"{p.X * scale}, {mapSize - (p.Y * scale)}");
                        Program.Log($"Tried to draw poly: {String.Join(", ", ps)}");
                    }
                }
                /*
                for (var x = 0; x < 255; x++) {
                    for (var y = 0; y < 255; y++) {
                        var lbid = (ushort)((x << 8) + (254 - y));
                        gfx.DrawRectangle(lbPen, new System.Drawing.Rectangle((int)(x * 192 * scale), (int)(y * 192 * scale), (int)(192 * scale), (int)(192 * scale)));
                        gfx.DrawString($"{lbid:X4}", lbFont, lbFontBrush, (int)(x * 192 * scale) + 2, (int)(y * 192 * scale) + 2);
                    }
                }
                //*/

                gfx.Save();
                filename = Path.Combine(@"./out/", $"landscape_{filename}.png");
                if (File.Exists(filename))
                    File.Delete(filename);
                bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }
}
