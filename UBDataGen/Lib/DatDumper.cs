﻿using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Data;

namespace UBDataGen.Lib {
    public class DatDumper {
        public string DatDirectory { get; }
        public string OutputDirectory { get; }
        public string Type { get; }
        public static CellDatDatabase CellDat { get; private set; }
        public static CellDatDatabase PortalDat { get; private set; }
        public RegionDesc RegionDesc { get; private set; }

        public LandblockInfoStore LandblockStore { get; } = new LandblockInfoStore();

        public string OutputFilePath { get => Path.Combine(OutputDirectory, OutputFile); }

        public string OutputFile { get => $"landblockinfo_{Type}.ubd"; }

        public DatDumper(string datDirectory, string outputDirectory, string type) {
            DatDirectory = datDirectory;
            OutputDirectory = outputDirectory;
            Type = type;
        }

        internal void Load() {
            LoadDatFiles();


            Program.Log($"Loading Landblocks... ", false);
            var sw = new Stopwatch();
            sw.Start();
            if (LandblockStore.TryDeserialize(OutputFilePath)) {
                sw.Stop();
                Program.Log($"Loaded {LandblockStore.MapData.Length:N0} landblocks from {OutputFilePath} in {sw.ElapsedMilliseconds / 1000f:N3}s");
                return;
            }
            sw.Stop();

            GenerateLandblockData();
            LandblockStore.TrySerialize(OutputFilePath);
            FileInfo fi = new FileInfo(OutputFilePath);
            Program.Log($"Saved cached Landblocks to {OutputFilePath} ({fi.Length:N0} bytes)\n");
        }

        private void LoadDatFiles() {
            var cellDatPath = Path.Combine(DatDirectory, "client_cell_1.dat");
            var portalDatPath = Path.Combine(DatDirectory, "client_portal.dat");

            if (!File.Exists(cellDatPath)) {
                Console.WriteLine($"Unable to find cell dat: {cellDatPath}");
                return;
            }

            if (!File.Exists(portalDatPath)) {
                Console.WriteLine($"Unable to find portal dat: {portalDatPath}");
                return;
            }

            var csw = new Stopwatch();
            csw.Start();
            Program.Log($"Loading cell dat... ", false);
            CellDat = new CellDatDatabase(cellDatPath, true);
            csw.Stop();
            Program.Log($"Took {csw.ElapsedMilliseconds / 1000f:N3} seconds");

            var psw = new Stopwatch();
            psw.Start();
            Program.Log($"Loading portal dat... ", false);
            PortalDat = new CellDatDatabase(portalDatPath, true);
            psw.Stop();
            Program.Log($"Took {psw.ElapsedMilliseconds / 1000f:N3} seconds");


            RegionDesc = DatDumper.PortalDat.ReadFromDat<RegionDesc>(0x13000000);
        }

        private void GenerateLandblockData() {
            var sw = new Stopwatch();
            sw.Start();

            int validLandblocks = 0;
            int dungeons = 0;
            int walkableLandblocks = 0;
            int unwalkableLandblocks = 0;
            int total = 0;


            // init mapData
            for (int x = 0; x < 255; x++) {
                var ylist = new List<LandblockFlag>();
                for (int y = 0; y < 255; y++) {
                    uint lbid = (((uint)x << 8) + (uint)y) << 16;
                    LandblockStore.MapData[x, y] = LandblockFlag.Nothing;
                }
            }

            // loop through all landblocks and set appropriate flags
            // 0,0 is bottom left
            for (int x = 0; x < 255; x++) {
                for (int y = 0; y < 255; y++) {
                    uint lbid = (((uint)x << 8) + (uint)y) << 16;
                    var landblockData = CellDat.ReadFromDat<CellLandblock>(lbid | 0x0000FFFF);
                    var landblockInfo = CellDat.ReadFromDat<LandblockInfo>(lbid | 0x0000FFFE);

                    total++;

                    LandblockStore.MapData[x, y] = LandblockFlag.Nothing;

                    if (landblockInfo == null)
                        continue;

                    validLandblocks++;

                    
                    if (IsDungeon(landblockData, landblockInfo)) {
                        LandblockStore.MapData[x, y] |= LandblockFlag.Dungeon;
                        dungeons++;
                    }

                    if (landblockData == null || landblockData.Terrain == null)
                        continue;

                    var allWater = landblockData.Terrain.All(t => LandblockMeshGen.TerrainIsWater(t));
                    if (!allWater) {
                        LandblockStore.MapData[x, y] |= LandblockFlag.Landscape;
                        walkableLandblocks++;
                    }

                }
            }

            //DrawMap(MapData);

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to load:");
            Program.Log($"\t Dungeons: \t\t{dungeons:N0}");
            Program.Log($"\t Walkable Landblocks: \t{walkableLandblocks:N0}");
            //Program.Log($"\t\t Walkable: \t\t{walkableLandblocks:N0}");
            //Program.Log($"\t\t UnWalkable: \t\t{unwalkableLandblocks:N0}");
            Program.Log($"\t Total Saved: \t\t{validLandblocks:N0} / {total:N0}");
        }

        private void DrawASCIIMap(LandblockFlag[,] mapData) {
            // start at the top
            for (var y = 253; y >= 0; y--) {
                string line = "";
                for (var x = 0; x < 254; x++) {
                    var lb = mapData[x, y];
                    switch (lb) {
                        case LandblockFlag.Unwalkable:
                        case LandblockFlag.Nothing:
                            line += " ";
                            break;
                        default:
                            if (lb.HasFlag(LandblockFlag.Landscape)) {
                                if (lb.HasFlag(LandblockFlag.HasDeepSea) && lb.HasFlag(LandblockFlag.HasImpassibleSlopes)) {
                                    line += "#";
                                }
                                else if (lb.HasFlag(LandblockFlag.HasDeepSea)) {
                                    line += "~";
                                }
                                else if (lb.HasFlag(LandblockFlag.HasImpassibleSlopes)) {
                                    line += ",";
                                }
                                else {
                                    line += ".";
                                }
                            }
                            else if (lb.HasFlag(LandblockFlag.Dungeon)) {
                                line += " ";
                            }
                            else {
                                line += "?";
                            }
                            break;
                    }
                }
                Program.Log(line);
            }
        }

        // logic from ACE
        private bool IsDungeon(CellLandblock landblockData, LandblockInfo landblockInfo) {
            // a dungeon landblock is determined by:
            // - all heights being 0
            // - having at least 1 EnvCell (0x100+)
            // - contains no buildings 

            //foreach (var height in landblockData.Height) {
            //    if (height != 0) {
            //        return false;
            //    }
            //}
            return HasDungeon(landblockData, landblockInfo);
        }

        private List<ushort> LandblocksWithDungeonsHack = new List<ushort>() {
            0x200f,
            0x77E7
        };

        private bool HasDungeon(CellLandblock landblockData, LandblockInfo landblockInfo) {
            
            // hack for landblocks that contain dungeons *and* buildings
            if (landblockInfo != null && LandblocksWithDungeonsHack.Contains(((ushort)(landblockInfo.Id >> 16)))) {
                return true;
            }

            return landblockInfo != null && landblockInfo.NumCells > 0  && landblockInfo.Buildings != null && landblockInfo.Buildings.Count ==0;
        }
    }
}
