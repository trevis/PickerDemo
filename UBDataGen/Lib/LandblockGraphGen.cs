﻿using ACE.DatLoader.FileTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Numerics;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Data;

namespace UBDataGen.Lib {
    public class LandblockGraphGen {
        private Dictionary<ushort, List<Portal>> portalCache;
        private Dictionary<ushort, List<Door>> doorCache;
        private Dictionary<ushort, List<Polygon2d>> polygonCache;

        private Dictionary<ushort, LBNode> dungeonNodesCache = new Dictionary<ushort, LBNode>();
        private Dictionary<ushort, List<LBNode>> landscapeNodesCache = new Dictionary<ushort, List<LBNode>>();
        private Dictionary<ushort, List<LBNode>> portalNodesCache = new Dictionary<ushort, List<LBNode>>();

        private Dictionary<uint, Portal> portalFromNodeIdLookup = new Dictionary<uint, Portal>();
        private Dictionary<uint, Polygon2d> polygonFromNodeIdLookup = new Dictionary<uint, Polygon2d>();

        public LBGraph Graph { get => GraphStore.Graph; }
        public LandblockMeshStore MeshStore { get; }
        public string OutputDirectory { get; }
        public string Type { get; }
        public LandblockInfoStore LandblockStore { get; }
        public EmuContentStore ContentStore { get; }

        public LandblockGraphStore GraphStore { get; } = new LandblockGraphStore();

        public string OutputFilePath { get => Path.Combine(OutputDirectory, OutputFile); }

        public string OutputFile { get => $"landblockgraph_{Type}.ubd"; }

        protected Random rand = new Random();

        public LandblockGraphGen(string outputDirectory, string type, LandblockInfoStore lbInfoStore, LandblockMeshStore lbMeshStore, EmuContentStore contentStore) {
            OutputDirectory = outputDirectory;
            Type = type;
            LandblockStore = lbInfoStore;
            MeshStore = lbMeshStore;
            ContentStore = contentStore;
        }

        public void Load() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"Building LandblockGraph... ", false);
            if (GraphStore.TryDeserialize(OutputFilePath)) {
                sw.Stop();
                Program.Log($"Loaded {GraphStore.Graph.Nodes.Count:N0} nodes with {GraphStore.Graph.Nodes.Sum(n => n.Edges.Count):N0} edges from {OutputFilePath} in {sw.ElapsedMilliseconds / 1000f:N3}s");
                return;
            }

            Program.Log("");
            portalCache = BuildPortalCache();
            doorCache = BuildDoorCache();
            polygonCache = BuildPolygonCache();

            BuildNodes();
            BuildEdges();

            //DrawLandblockGraph(Graph, "eor");

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build with {Graph.Nodes.Count:N0} nodes and {Graph.Nodes.Sum(n => n.Edges.Count):N0} edges");

            GraphStore.TrySerialize(OutputFilePath);
            FileInfo fi = new FileInfo(OutputFilePath);
            Program.Log($"Saved cached Landblocks to {OutputFilePath} ({fi.Length:N0} bytes)\n");
        }

        private void BuildNodes() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\tBuilding Nodes... ", false);

            BuildDungeonNodes();
            BuildLandscapeNodes();
            BuildPortalNodes();

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build {Graph.Nodes.Count:N0} nodes: landscape: {landscapeNodesCache.Count:N0}, dungeon: {dungeonNodesCache.Count:N0}, portals: {portalNodesCache.Count:N0}");
        }

        private void BuildDungeonNodes() {
            for (ushort x = 0; x < 255; x++) {
                for (ushort y = 0; y < 255; y++) {
                    var lbid = (ushort)((x << 8) + y);
                    var lbFlag = LandblockStore.MapData[x, y];
                    if (lbFlag.HasFlag(LandblockFlag.Dungeon)) {
                        var requirements = new UseRequirements();
                        var keys = new List<Key>();
                        if (doorCache.ContainsKey(lbid)) {
                            foreach (var door in doorCache[lbid]) {
                                if (door.Key != null && !requirements.Keys.Select(k => k.Code).Contains(door.Key.Code))
                                    requirements.Keys.Add(door.Key);
                                else if (door.LockpickResist > requirements.LockpickResist)
                                    requirements.LockpickResist = door.LockpickResist;
                            }
                        }

                        var node = new LBNode() {
                            Frame = new Frame((uint)lbid << 16, 0, 0, 0, 0),
                            Type = LocationType.Dungeon,
                            FriendlyName = DungeonsNames.GetName((uint)lbid << 16),
                            Requirements = requirements
                        };
                        Graph.AddNode(node);
                        dungeonNodesCache.Add(lbid, node);
                    }
                }
            }
        }

        private void BuildLandscapeNodes() {
            foreach (var kv in polygonCache) {
                var lbid = kv.Key;
                var x = kv.Key >> 8;
                var y = kv.Key & 0xFF;
                var lbFlag = LandblockStore.MapData[x, y];

                foreach (var poly in kv.Value) {
                    // walkable landblock
                    if (lbFlag.HasFlag(LandblockFlag.Landscape)) {
                        var node = new LBNode() {
                            Frame = new Frame((uint)lbid << 16, 0, 0, 0, 0),
                            Type = LocationType.Landscape,
                            FriendlyName = $"LB.{lbid:X4}"
                        };
                        Graph.AddNode(node);

                        if (!landscapeNodesCache.ContainsKey(lbid))
                            landscapeNodesCache.Add(lbid, new List<LBNode>());

                        landscapeNodesCache[lbid].Add(node);
                        poly.LBNode = node;
                        node.RegionId = poly.RegionId;
                        polygonFromNodeIdLookup.Add(node.Id, poly);
                    }
                }
            }
        }

        private void BuildPortalNodes() {
            var flags = new List<string>();
            foreach (var (lbid, portalList) in portalCache) {
                foreach (var portal in portalList) {
                    if (!string.IsNullOrEmpty(portal.Requirements?.QuestRestriction)) {
                        Console.WriteLine($"Portal {portal.Name} {portal.WeenieId:X8} needs quest flag: {portal.Requirements.QuestRestriction}");
                        if (!flags.Contains(portal.Requirements.QuestRestriction)) {
                            flags.Add(portal.Requirements.QuestRestriction);
                        }
                    }
                    var node = new LBNode() {
                        Frame = portal.Frame,
                        Type = LocationType.Portal,
                        FriendlyName = portal.Name,
                        Requirements = portal.Requirements
                    };
                    Graph.AddNode(node);

                    if (!portalNodesCache.ContainsKey(lbid))
                        portalNodesCache.Add(lbid, new List<LBNode>());

                    portalNodesCache[lbid].Add(node);
                    portalFromNodeIdLookup.Add(node.Id, portal);
                }
            }
            flags.Sort();
            Console.WriteLine($"\nQuestFlags: {string.Join(", ", flags)}");
        }

        private void BuildEdges() {
            var sw = new Stopwatch();
            sw.Start();
            Program.Log($"\tBuilding Edges... ", false);

            BuildPortalEdges();
            BuildRegionEdges();

            sw.Stop();
            Program.Log($"Took {sw.ElapsedMilliseconds / 1000f:N3}s to build {Graph.Nodes.Sum(n => n.Edges.Count):N0} edges");
        }

        private void BuildRegionEdges() {
            var allNodes = new List<LBNode>();
            foreach (var (lbId, lbNodes) in landscapeNodesCache) {
                allNodes.AddRange(lbNodes);
            }

            foreach (var currentNode in allNodes) {
                var currentPoly = polygonFromNodeIdLookup[currentNode.Id];
                ushort x = (ushort)((currentNode.Frame.Landblock >> 16) >> 8);
                ushort y = (ushort)((currentNode.Frame.Landblock >> 16) & 0xFF);

                var dirs = new List<List<short>>() {
                        // top
                        new List<short>(){ 0, 1 },
                        // bottom
                        new List<short>(){ 0, -1 },
                        // right
                        new List<short>(){ 1, 0 },
                        // left
                        new List<short>(){ -1, 0 },
                        // top left
                        new List<short>(){ -1, 1 },
                        // top right
                        new List<short>(){ 1, 1 },
                        // bottom left
                        new List<short>(){ -1, -1 },
                        // bottom right
                        new List<short>(){ 1, -1 },

                    };
                foreach (var dir in dirs) {
                    if (x + dir[0] >= 0 && x + dir[0] <= 254 && y + dir[1] >= 0 && y + dir[1] <= 254) {
                        var lc = (ushort)(((x + dir[0]) << 8) + (y + dir[1]));
                        //Program.Log($"{lc:X8}");

                        if (!landscapeNodesCache.ContainsKey(lc))
                            continue;

                        var dirNodes = landscapeNodesCache[lc];

                        foreach (var checkNode in dirNodes) {
                            var checkPoly = polygonFromNodeIdLookup[checkNode.Id];
                            List<List<Vector2>> edgePairs = currentPoly.GetSharedEdgesWith(checkPoly);

                            if (edgePairs.Count > 0 && (dir[0] == 0 || dir[1] == 0)) {
                                foreach (var edgePair in edgePairs) {
                                    currentNode.Edges.Add(new LBEdge() {
                                        OtherNodeId = checkNode.Id,
                                        Type = LBEdge.LBEdgeType.Landscape,
                                        StartFrame = new Frame() {
                                            Landblock = (((uint)Math.Floor(edgePair[0].X / 192f) << 8) + (uint)Math.Floor(edgePair[0].Y / 192f)) << 16,
                                            LocalX = ((int)edgePair[0].X % 192),
                                            LocalY = ((int)edgePair[0].Y % 192),
                                        },
                                        DestinationFrame = new Frame() {
                                            Landblock = (((uint)(Math.Floor(edgePair[1].X / 192f)) << 8) + (uint)Math.Floor(edgePair[1].Y / 192f)) << 16,
                                            LocalX = ((int)edgePair[1].X % 192),
                                            LocalY = ((int)edgePair[1].Y % 192),
                                        },
                                    });
                                    //Program.Log($"{currentNode.Edges.Last().StartFrame}   {currentNode.Edges.Last().DestinationFrame}");
                                }
                            }
                            if (currentPoly.SharesVerticeWith(checkPoly) && dir[0] != 0 && dir[1] != 0) {
                                var shared = currentPoly.GetSharedVertice(checkPoly);
                                currentNode.Edges.Add(new LBEdge() {
                                    OtherNodeId = checkNode.Id,
                                    Type = LBEdge.LBEdgeType.Landscape,
                                    StartFrame = new Frame() {
                                        Landblock = (((uint)Math.Floor(shared.X / 192f) << 8) + (uint)Math.Floor(shared.Y / 192f)) << 16,
                                        LocalX = ((int)shared.X % 192),
                                        LocalY = ((int)shared.Y % 192),
                                    },
                                    DestinationFrame = new Frame() {
                                        Landblock = (((uint)(Math.Floor(shared.X / 192f)) << 8) + (uint)Math.Floor(shared.Y / 192f)) << 16,
                                        LocalX = ((int)shared.X % 192),
                                        LocalY = ((int)shared.Y % 192),
                                    },
                                });
                            }
                        }
                    }
                }
            }
        }

        private void BuildPortalEdges() {
            foreach (var (lbId, portalNodes) in portalNodesCache) {
                var x = lbId >> 8;
                var y = lbId & 0xFF;
                var lbFlag = LandblockStore.MapData[x, y];

                foreach (var portalNode in portalNodes) {
                    var portal = portalFromNodeIdLookup[portalNode.Id];
                    var destLbId = (ushort)(portal.Destination.Landblock >> 16);
                    var destX = destLbId >> 8;
                    var destY = destLbId & 0xFF;
                    var destLbFlag = LandblockStore.MapData[destX, destY];

                    // link landscape/dungeon node to this portal
                    if (lbFlag.HasFlag(LandblockFlag.Dungeon)) {
                        if (!dungeonNodesCache.ContainsKey(lbId)) {
                            Program.Log($"Could not find dungeon node: {lbId:X4}");
                            continue;
                        }
                        var dungeonNode = dungeonNodesCache[lbId];
                        dungeonNode.Edges.Add(new LBEdge() {
                            OtherNodeId = portalNode.Id,
                            Type = LBEdge.LBEdgeType.Portal,
                            StartFrame = dungeonNode.Frame,
                            DestinationFrame = portal.Frame
                        });
                    }
                    else {
                        if (!polygonCache.ContainsKey(lbId)) {
                            Program.Log($"Could not find polygonCache node: {lbId:X4}");
                            continue;
                        }
                        var foundpoint = false;
                        foreach (var p in polygonCache[lbId]) {
                            if (p.ContainsPoint(portal.Frame)) {
                                p.LBNode.Edges.Add(new LBEdge() {
                                    OtherNodeId = portalNode.Id,
                                    Type = LBEdge.LBEdgeType.Portal,
                                    StartFrame = p.LBNode.Frame,
                                    DestinationFrame = portal.Frame
                                });
                                foundpoint = true;
                                continue;
                            }
                        }
                        if (!foundpoint) {
                            //Program.Log($"No point found for: {portal.Name}");
                        }
                    }

                    // link portal to destination
                    if (destLbFlag.HasFlag(LandblockFlag.Dungeon)) {
                        if (!dungeonNodesCache.ContainsKey(destLbId)) {
                            Program.Log($"Could not find destination dungeon node: {destLbId:X4}");
                            continue;
                        }

                        var dungeonNode = dungeonNodesCache[destLbId];
                        portalNode.Edges.Add(new LBEdge() {
                            OtherNodeId = dungeonNode.Id,
                            Type = LBEdge.LBEdgeType.Portal,
                            StartFrame = portal.Frame,
                            DestinationFrame = portal.Destination
                        });
                    }
                    else {
                        if (!polygonCache.ContainsKey(destLbId)) {
                            Program.Log($"Could not find dest polygonCache node: {destLbId:X4}");
                            continue;
                        }
                        var landscapeNode = polygonCache[destLbId].First().LBNode;

                        portalNode.Edges.Add(new LBEdge() {
                            OtherNodeId = landscapeNode.Id,
                            Type = LBEdge.LBEdgeType.Portal,
                            StartFrame = portal.Frame,
                            DestinationFrame = portal.Destination
                        });
                    }
                }
            }
        }

        private Dictionary<ushort, List<Portal>> BuildPortalCache() {
            var portals = new Dictionary<ushort, List<Portal>>();

            foreach (var portal in ContentStore.Portals) {
                var lbid = (ushort)(portal.Frame.Landblock >> 16);
                if (!portals.ContainsKey(lbid))
                    portals.Add(lbid, new List<Portal>());

                portals[lbid].Add(portal);
            }

            return portals;
        }

        private Dictionary<ushort, List<Door>> BuildDoorCache() {
            var doors = new Dictionary<ushort, List<Door>>();

            foreach (var door in ContentStore.Doors) {
                var lbid = (ushort)(door.Frame.Landblock >> 16);
                if (!doors.ContainsKey(lbid))
                    doors.Add(lbid, new List<Door>());

                doors[lbid].Add(door);
            }

            return doors;
        }

        private Dictionary<ushort, List<Polygon2d>> BuildPolygonCache() {
            var polys = new Dictionary<ushort, List<Polygon2d>>();

            foreach (var poly in MeshStore.Polygons) {;
                if (!polys.ContainsKey(poly.Lbid))
                    polys.Add(poly.Lbid, new List<Polygon2d>());

                polys[poly.Lbid].Add(poly);
            }

            return polys;
        }

        private Dictionary<uint, Color> nodeColorCache = new Dictionary<uint, Color>();
        private Color GetNodeColor(LBNode node, int start = 160, int end = 200) {
            if (true || !nodeColorCache.ContainsKey(node.Id)) {
                var r = rand.Next(start, end);
                var g = rand.Next(start, end);
                var b = rand.Next(start, end);
                nodeColorCache.Add(node.Id, Color.FromArgb(255, r, g, b));
            }

            return nodeColorCache[node.Id];
        }

        private void DrawLandblockGraph(LBGraph graph, string filename) {
            var scale = 1 / 8f;
            var mapSize = (int)(255 * LandblockMeshGen.BlockSize * scale);
            var portalPen = new Pen(new SolidBrush(Color.FromArgb(60, Color.Purple)), 1);
            var lcPen = new Pen(new SolidBrush(Color.FromArgb(50, Color.Orange)), 1);
            var dungeonPen = new Pen(new SolidBrush(Color.FromArgb(250, Color.GreenYellow)), 1);
            var lbFont = new System.Drawing.Font("mono", 8, FontStyle.Regular);
            var dungeonFont = new System.Drawing.Font("mono", 8, FontStyle.Regular);
            var lbFontBrush = new SolidBrush(Color.FromArgb(255, Color.Red));
            var backgroundBrush = new SolidBrush(Color.FromArgb(255, Color.Black));
            var walkablePen = new Pen(new SolidBrush(Color.FromArgb(255, Color.Green)), 1);
            var portalTravelPen = new Pen(new SolidBrush(Color.FromArgb(255, Color.Purple)), 1);

            portalTravelPen.DashCap = System.Drawing.Drawing2D.DashCap.Round;
            portalTravelPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            portalTravelPen.DashPattern = new float[] { 6.0F, 2.0F };


            var landscapeTravelPen = new Pen(new SolidBrush(Color.FromArgb(180, Color.Green)), 1);

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            GraphStore.Graph.BuildEdgeNodeCaches();

            using (var brush = new SolidBrush(Color.FromArgb(75, Color.Red)))
            using (var bmp = new Bitmap(mapSize, mapSize))
            using (var gfx = Graphics.FromImage(bmp)) {
                gfx.FillRectangle(backgroundBrush, new Rectangle(0, 0, mapSize, mapSize));

                var portalNodes = new Dictionary<uint, LBNode>();
                foreach (var nodes in portalNodesCache.Values)
                    foreach (var node in nodes)
                        portalNodes.Add(node.Id, node);

                var dungeonNodes = new Dictionary<uint, LBNode>();
                foreach (var node in dungeonNodesCache.Values)
                    dungeonNodes.Add(node.Id, node);

                var landscapeNodes = new Dictionary<uint, LBNode>();
                foreach (var nodes in landscapeNodesCache.Values)
                    foreach (var node in nodes)
                        landscapeNodes.Add(node.Id, node);

                var lbSize = (int)(8 * LandblockMeshGen.CellSize * scale);

                foreach (var node in landscapeNodes.Values) {
                    brush.Color = GetNodeColor(node);
                    var poly = polygonFromNodeIdLookup[node.Id];
                    gfx.FillPolygon(brush, poly.Vertices.Select(p => new PointF(p.X * scale, mapSize - (p.Y * scale))).ToArray());
                    var x = (int)((node.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                    var y = (int)(254 * lbSize) - (int)(((node.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                    var halfX = x + ((int)LandblockMeshGen.BlockSize * scale / 2);
                    //gfx.DrawString($"{(node.Frame.Landblock >> 16):X4}", lbFont, lbFontBrush, new PointF(halfX, y + 8), stringFormat);
                }

                foreach (var node in portalNodes.Values) {
                    brush.Color = Color.Magenta;
                    var x = (int)((node.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                    var y = (int)(254 * lbSize) - (int)(((node.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);

                    x += (int)(node.Frame.LocalX * (lbSize / 192f));
                    y += (int)(192f * scale) - (int)(( node.Frame.LocalY) * (lbSize / 192f));

                    foreach (var edge in node.Edges) {
                        if (edge.Type != LBEdge.LBEdgeType.Portal)
                            continue;

                        var destNode = landscapeNodes.ContainsKey(edge.OtherNodeId) ? landscapeNodes[edge.OtherNodeId] : dungeonNodes[edge.OtherNodeId];
                        var destX = (int)((destNode.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                        var destY = (int)(254 * lbSize) - (int)(((destNode.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                        destX += (int)(edge.DestinationFrame.LocalX * (lbSize / 192f));
                        destY += (int)(192f * scale) - (int)(( edge.DestinationFrame.LocalY) * (lbSize / 192f));

                        portalTravelPen.Color = GetNodeColor(node, 50, 120);
                        gfx.DrawLine(portalTravelPen, new Point(x, y), new Point(destX, destY));
                        //gfx.DrawString($"{(node.WeenieId)}", lbFont, lbFontBrush, new PointF(x, y), stringFormat);
                    }
                }

                foreach (var node in dungeonNodes.Values) {
                    var x = (int)((node.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                    var y = (int)(254 * lbSize) - (int)(((node.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                    var halfX = x + ((int)LandblockMeshGen.BlockSize * scale / 2);
                    gfx.DrawRectangle(dungeonPen, x, y, LandblockMeshGen.BlockSize * scale, LandblockMeshGen.BlockSize * scale);
                    var s = (node.Frame.Landblock >> 16).ToString("X4");
                    gfx.DrawString(s.Insert(2, "\n"), lbFont, lbFontBrush, new PointF(halfX, y + 14), stringFormat);
                    //gfx.DrawString(UBLoader.Data.DungeonsNames.GetName(node.Frame.Landblock), dungeonFont, lbFontBrush, new Rectangle(x, (int)(y) + 8, lbSize, lbSize -8), stringFormat);
                }

                foreach (var node in landscapeNodes.Values) {
                    var i = 0;
                    foreach (var edge in node.Edges) {
                        var other = node.EdgeNodes[i++];

                        var startX = (int)((edge.StartFrame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                        var startY = (int)(254 * lbSize) - (int)(((edge.StartFrame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                        startX += (int)(edge.StartFrame.LocalX * (lbSize / 192f));
                        startY += (int)(192f * scale) - (int)(edge.StartFrame.LocalY * (lbSize / 192f));

                        var destX = (int)((edge.DestinationFrame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                        var destY = (int)(254 * lbSize) - (int)(((edge.DestinationFrame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                        destX += (int)(edge.DestinationFrame.LocalX * (lbSize / 192f));
                        destY += (int)(192f * scale) - (int)(edge.DestinationFrame.LocalY * (lbSize / 192f));

                        var edgemid = new Vector2((startX + destX) / 2f, (startY + destY) / 2f);
                        //Program.Log($"{lbmid.X}, {lbmid.Y}  --  {edgemid.X}, {edgemid.Y}");

                        var v = new Vector2(startX - destX, startY - destY);
                        var g = Vector2.Normalize(v);
                        g = new Vector2(-g.Y, g.X);
                        var d = 4f;
                        var end = new Vector2(edgemid.X + (g.X * d), edgemid.Y + (g.Y * d));


                        if (Math.Sqrt(Math.Pow(Math.Abs(startX - destX), 2) + Math.Pow(Math.Abs(startY - destY), 2)) < 2) {
                            var s = 2;
                            brush.Color = Color.Red;
                            gfx.FillEllipse(brush, new Rectangle((int)startX - s / 2, (int)startY - s / 2, s, s));
                        }
                        else {
                            gfx.DrawLine(landscapeTravelPen, new Point((int)edgemid.X, (int)edgemid.Y), new Point((int)end.X, (int)end.Y));
                            var s = 2;
                            brush.Color = Color.Red;
                            gfx.FillEllipse(brush, new Rectangle((int)end.X - s / 2, (int)end.Y - s / 2, s, s));
                        }

                        //gfx.DrawLine(landscapeTravelPen, new Point(startX, startY), new Point(destX, destY));
                        //var s = 2;
                        //brush.Color = Color.Red;
                        //gfx.FillEllipse(brush, new Rectangle(midX - s / 2, midY - s / 2, s, s));
                        //brush.Color = Color.Red;
                        //gfx.FillEllipse(brush, new Rectangle(startX - s / 2, startY - s / 2, s, s));
                        //brush.Color = Color.Blue;
                        //gfx.FillEllipse(brush, new Rectangle(destX - s / 2, destY - s / 2, s, s));
                    }
                }

                foreach (var node in portalNodes.Values) {
                    brush.Color = Color.Green;
                    var x = (int)((node.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                    var y = (int)(254 * lbSize) - (int)(((node.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);

                    x += (int)(node.Frame.LocalX * (lbSize / 192f));
                    y += (int)(192f * scale) - (int)((node.Frame.LocalY) * (lbSize / 192f));

                    var s = 2;
                    gfx.FillEllipse(brush, new Rectangle(x - s / 2, y - s / 2, s, s));
                    //gfx.DrawString($"{(node.Frame.Landblock >> 16):X4}", lbFont, lbFontBrush, new PointF(x, y), stringFormat);


                    foreach (var edge in node.Edges) {
                        if (edge.Type != LBEdge.LBEdgeType.Portal)
                            continue;

                        var destNode = landscapeNodes.ContainsKey(edge.OtherNodeId) ? landscapeNodes[edge.OtherNodeId] : dungeonNodes[edge.OtherNodeId];
                        var destX = (int)((destNode.Frame.Landblock >> 24) * LandblockMeshGen.BlockSize * scale);
                        var destY = (int)(254 * lbSize) - (int)(((destNode.Frame.Landblock >> 16) & 0xFF) * LandblockMeshGen.BlockSize * scale);
                        destX += (int)(edge.DestinationFrame.LocalX * (lbSize / 192f));
                        destY += (int)(192f * scale) - (int)((edge.DestinationFrame.LocalY) * (lbSize / 192f));

                        brush.Color = Color.Blue;
                        gfx.FillEllipse(brush, new Rectangle(destX - s / 2, destY - s / 2, s, s));
                    }
                }

                gfx.Save();
                filename = Path.Combine(OutputDirectory, $"graph_{filename}.png");
                if (System.IO.File.Exists(filename))
                    System.IO.File.Delete(filename);
                bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }
}
