﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Lib;

namespace UtilityBelt.Navigation.Shared.Models
{
    public class LBGraph {
        public List<LBNode> Nodes { get; set; } = new List<LBNode>();

        [NonSerialized]
        public Dictionary<uint, LBNode> NodeLookup = new Dictionary<uint, LBNode>();
        public void AddNode(LBNode node) {
            Nodes.Add(node);
        }

        public LBNode FindNodeById(uint id) {
            if (NodeLookup.Count != Nodes.Count)
                BuildEdgeNodeCaches();

            if (NodeLookup.TryGetValue(id, out LBNode ret)) {
                return ret;
            }
            return null;
        }

        public void BuildEdgeNodeCaches() {
            foreach (var node in Nodes) {
                NodeLookup.Add(node.Id, node);
            }

            foreach (var node in Nodes) {
                node.EdgeNodes = node.Edges == null ? new List<LBNode>() : node.Edges.Select(e => NodeLookup[e.OtherNodeId]).ToList();
            }
        }

        public void Serialize(BinaryWriter writer) {
            writer.Write((uint)Nodes.Count);
            foreach (var node in Nodes) {
                node.Serialize(writer);
            }
        }

        public void Deserialize(BinaryReader reader) {
            var nodeCount = reader.ReadUInt32();
            for (var i = 0; i < nodeCount; i++) {
                var node = new LBNode();
                node.Deserialize(reader);
                Nodes.Add(node);
            }
        }

        public List<LBAStar.LBTravelledNode> FindPath(LBNode start, LBNode end, List<string> questFlags, int minLevel, out float distance, out uint checkedNodeCount) {
            return LBAStar.FindPath(this, start, end, questFlags, minLevel, out distance, out checkedNodeCount);
        }
    }
}
