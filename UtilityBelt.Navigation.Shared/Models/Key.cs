﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    public class Key : POIBase {
        public string Code { get; set; } = "";

        public Key() { }


        public override bool Serialize(BinaryWriter writer) {
            base.Serialize(writer);
            writer.Write(Code);
            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            base.Deserialize(reader);
            Code = reader.ReadString();
            return true;
        }
    }
}
