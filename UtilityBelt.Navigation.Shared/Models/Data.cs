﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class Data {
        public List<Portal> Portals { get; set; }
        public List<Door> Doors { get; set; }

        public Data() { }
    }
}
