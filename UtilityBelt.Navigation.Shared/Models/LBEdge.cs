﻿using System;
using System.IO;

namespace UtilityBelt.Navigation.Shared.Models {
    public class LBEdge {
        public enum LBEdgeType : byte {
            Portal = 0x1,
            Landscape = 0x2,
            Platform = 0x3
        }

        public uint OtherNodeId { get; set; }
        public LBEdgeType Type { get; set; }

        public Frame StartFrame { get; set; } = new Frame();
        public Frame DestinationFrame { get; set; } = new Frame();

        public void Serialize(BinaryWriter writer) {
            writer.Write(OtherNodeId);
            writer.Write((byte)Type);
            StartFrame.Serialize(writer);
            DestinationFrame.Serialize(writer);
        }

        public void Deserialize(BinaryReader reader) {
            OtherNodeId = reader.ReadUInt32();
            Type = (LBEdgeType)reader.ReadByte();
            StartFrame.Deserialize(reader);
            DestinationFrame.Deserialize(reader);
        }
    }
}