﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public struct Waypoint {
        public Frame Frame;
        public WaypointType WaypointType;
        public uint UseId;
        public string Name;
    }
}
