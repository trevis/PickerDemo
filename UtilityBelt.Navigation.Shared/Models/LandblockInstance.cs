﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class LandblockInstance {
        public uint Guid { get; set; }
        public uint WeenieClassId { get; set; }
        public Frame Frame { get; set; }

        public List<LandblockInstanceLink> Links { get; set; }
        public bool IsLinkChild { get; set; }
    }
}
