﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Lib;

namespace UtilityBelt.Navigation.Shared.Models {
    public class LBNode {

        private static uint _idGen = 1;

        public uint Id { get; set; }
        public Frame Frame { get; set; } = new Frame();
        public LocationType Type { get; set; }
        public string FriendlyName { get; set; } = "";
        public uint WeenieId { get; set; }
        public uint RegionId { get; set; }
        public List<LBEdge> Edges { get; set; } = new List<LBEdge>();

        public UseRequirements Requirements { get; set; } = new UseRequirements();

        [NonSerialized]
        public List<LBNode> EdgeNodes;

        public LBNode() {
            Id = _idGen++;
        }

        public float DistanceTo(LBNode b) {
            if (Type == LocationType.Portal)
                return 0;
            var cellSize = Type == LocationType.Dungeon ? 10.0f : 24.0f;
            var ax = ACGeometry.LandblockToEW(Frame.Landblock >> 16 << 16, Frame.LocalX);
            var bx = ACGeometry.LandblockToEW(b.Frame.Landblock >> 16 << 16, b.Frame.LocalX);
            var ay = ACGeometry.LandblockToNS(Frame.Landblock >> 16 << 16, Frame.LocalY);
            var by = ACGeometry.LandblockToNS(b.Frame.Landblock >> 16 << 16, b.Frame.LocalY);
            var dist = (float)Math.Sqrt(Math.Pow(Math.Abs(ax - bx), 2) + Math.Pow(Math.Abs(ay - by), 2));
            //Program.Log($"Dist: {a.Landcell:X8}({a.Type}) -> {b.Landcell:X8}({b.Type}) is {dist} -- {ax},{ay}  {bx},{by}");

            return dist;
        }

        public void Serialize(BinaryWriter writer) {
            writer.Write(Id);
            Frame.Serialize(writer);
            writer.Write((byte)Type);
            writer.Write(FriendlyName);
            writer.Write(WeenieId);
            writer.Write(RegionId);
            writer.Write((uint)Edges.Count);
            foreach (var edge in Edges) {
                edge.Serialize(writer);
            }
            Requirements.Serialize(writer);
        }

        public void Deserialize(BinaryReader reader) {
            Id = reader.ReadUInt32();
            Frame.Deserialize(reader);
            Type = (LocationType)reader.ReadByte();
            FriendlyName = reader.ReadString();
            WeenieId = reader.ReadUInt32();
            RegionId = reader.ReadUInt32();
            var edgeCount = reader.ReadUInt32();
            for (var i = 0; i < edgeCount; i++) {
                var edge = new LBEdge();
                edge.Deserialize(reader);
                Edges.Add(edge);
            }
            Requirements.Deserialize(reader);
        }
    }
}
