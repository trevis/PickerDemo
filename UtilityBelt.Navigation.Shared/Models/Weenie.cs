﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class Weenie {
        public uint Id { get; set; }
        public uint SetupId { get; set; }
        public WeenieType Type { get; set; }

        public Dictionary<PropertyBool, bool> BoolProps { get; set; } = new Dictionary<PropertyBool, bool>();
        public Dictionary<PropertyInt, int> IntProps { get; set; } = new Dictionary<PropertyInt, int>();
        public Dictionary<PropertyString, string> StringProps { get; set; } = new Dictionary<PropertyString, string>();
        public Dictionary<PositionType, Frame> Positions { get; set; } = new Dictionary<PositionType, Frame>();

        public bool HasProp(PropertyBool prop) {
            return BoolProps.ContainsKey(prop);
        }

        public bool HasProp(PropertyInt prop) {
            return IntProps.ContainsKey(prop);
        }

        public bool HasProp(PropertyString prop) {
            return StringProps.ContainsKey(prop);
        }

        public bool HasPosition(PositionType prop) {
            return Positions.ContainsKey(prop);
        }

        public bool GetProp(PropertyBool prop, bool defaultValue = false) {
            return BoolProps.ContainsKey(prop) ? BoolProps[prop] : defaultValue;
        }

        public int GetProp(PropertyInt prop, int defaultValue = 0) {
            return IntProps.ContainsKey(prop) ? IntProps[prop] : defaultValue;
        }

        public string GetProp(PropertyString prop, string defaultValue = "") {
            return StringProps.ContainsKey(prop) ? StringProps[prop] : defaultValue;
        }

        public Frame GetPosition(PositionType prop) {
            return Positions.ContainsKey(prop) ? Positions[prop] : new Frame();
        }
    }
}
