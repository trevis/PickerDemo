﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UtilityBelt.Navigation.Shared.Data;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Lib;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class Frame {
        public uint Landblock { get; set; }
        public float LocalX { get; set; }
        public float LocalY { get; set; }
        public float LocalZ { get; set; }

        public float Heading { get; set; }

        public float NS { get => ACGeometry.LandblockToNS(Landblock, LocalY); }
        public float EW { get => ACGeometry.LandblockToEW(Landblock, LocalX); }

        [JsonIgnore]
        public float WorldZ { get => LocalZ / 240.0f; }

        [NonSerialized]
        public static Regex WorldCoordinateRegex = new Regex(@"(?<NSval>\d+.?\d*)\s*(?<NSchr>[ns]),?\s*(?<EWval>\d+.?\d*)(?<EWchr>[ew])(,?\s*(?<Zval>\-?\d+.?\d*)z)?", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        [NonSerialized]
        public static Regex LocalCoordinateRegex = new Regex(@"0x(?<landcell>[0-9a-f]{8}) \[(?<xVal>\-?\d+.?\d*), (?<yVal>\-?\d+.?\d*), (?<zVal>\-?\d+.?\d*)\]", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        [JsonIgnore]
        public string DungeonName { get => DungeonsNames.GetName(Landblock); }
        public LocationType LocationType { get => IsDungeon() ? LocationType.Dungeon : LocationType.Landscape; }

        public static Frame FromString(string location) {
            var f = new Frame();
            location = location.ToLower().Trim();
            if (WorldCoordinateRegex.IsMatch(location)) {
                var m = WorldCoordinateRegex.Match(location);
                var ns = double.Parse(m.Groups["NSval"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);
                var ew = double.Parse(m.Groups["EWval"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);

                if (ns >= 103 || ew >= 103)
                    return null;

                ns *= m.Groups["NSchr"].Value.ToLower().Equals("n") ? 1 : -1;
                ew *= m.Groups["EWchr"].Value.ToLower().Equals("e") ? 1 : -1;
                f.Landblock = ACGeometry.GetLandblockFromCoordinates(ew, ns);
                f.LocalX = ACGeometry.EWToLandblock(f.Landblock, (float)ew);
                f.LocalY = ACGeometry.NSToLandblock(f.Landblock, (float)ns);
                if (!string.IsNullOrEmpty(m.Groups["Zval"].Value))
                    f.LocalZ = float.Parse(m.Groups["Zval"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);
            }
            else if (LocalCoordinateRegex.IsMatch(location)) {
                var m = LocalCoordinateRegex.Match(location);
                f.Landblock = uint.Parse(m.Groups["landcell"].Value, System.Globalization.NumberStyles.HexNumber);
                f.LocalX = float.Parse(m.Groups["xVal"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);
                f.LocalY = float.Parse(m.Groups["yVal"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);
                f.LocalZ = float.Parse(m.Groups["zVal"].Value, (IFormatProvider)CultureInfo.InvariantCulture.NumberFormat);
            }
            else {
                f.Landblock = DungeonsNames.DungeonNames.Where(kv => kv.Value.ToLower() == location).FirstOrDefault().Key << 16;
            }

            if (f.Landblock == 0)
                return null;

            return f;
        }

        public Frame() {
        
        }

        public Frame(float ew, float ns) {
            Landblock = ACGeometry.GetLandblockFromCoordinates(ew, ns);
            LocalX = ACGeometry.EWToLandblock(Landblock, (float)ew);
            LocalY = ACGeometry.NSToLandblock(Landblock, (float)ns);
        }

        public Frame(uint landblock, float x, float y, float z, float heading) {
            Landblock = landblock;
            LocalX = x;
            LocalY = y;
            LocalZ = z;
            Heading = heading;
        }

        public bool Serialize(BinaryWriter writer) {
            writer.Write(Landblock);
            writer.Write(LocalX);
            writer.Write(LocalY);
            writer.Write(LocalZ);
            writer.Write(Heading);

            return true;
        }

        public bool Deserialize(BinaryReader reader) {
            Landblock = reader.ReadUInt32();
            LocalX = reader.ReadSingle();
            LocalY = reader.ReadSingle();
            LocalZ = reader.ReadSingle();
            Heading = reader.ReadSingle();

            return true;
        }

        public double HeadingTo(Frame target) {
            var deltaY = target.NS - NS;
            var deltaX = target.EW - EW;
            return (360 - (Math.Atan2(deltaY, deltaX) * 180 / Math.PI) + 90) % 360;
        }

        public double DistanceTo(Frame other) {
            var nsdiff = (((NS * 10) + 1019.5) * 24) - (((other.NS * 10) + 1019.5) * 24);
            var ewdiff = (((EW * 10) + 1019.5) * 24) - (((other.EW * 10) + 1019.5) * 24);
            return Math.Abs(Math.Sqrt(Math.Pow(Math.Abs(nsdiff), 2) + Math.Pow(Math.Abs(ewdiff), 2) + Math.Pow(Math.Abs(WorldZ - other.WorldZ), 2)));
        }

        public double DistanceToFlat(Frame other) {
            var nsdiff = (((NS * 10) + 1019.5) * 24) - (((other.NS * 10) + 1019.5) * 24);
            var ewdiff = (((EW * 10) + 1019.5) * 24) - (((other.EW * 10) + 1019.5) * 24);
            return Math.Abs(Math.Sqrt(Math.Pow(Math.Abs(nsdiff), 2) + Math.Pow(Math.Abs(ewdiff), 2)));
        }

        public bool IsDungeon() {
            // TODO: check actual position for inside dungeon vs landscape?
            return DungeonsNames.DungeonNames.ContainsKey(Landblock >> 16);
        }

        public string ToCoordinatesString(int precision=3, bool showZ = true) {
            var pf = $"F{precision}";
            return $"{Math.Abs(NS).ToString(pf)}{(NS >= 0 ? "N" : "S")}, {Math.Abs(EW).ToString(pf)}{(EW >= 0 ? "E" : "W")}{(showZ ? $", {WorldZ.ToString(pf)}Z" : "")}";
        }

        public string ToLocalString() {
            return $"[0x{Landblock >> 16:X4} {LocalX:F0}, {LocalY:F0}, {LocalZ:F0}]";
        }

        public override string ToString() {
            if (!IsDungeon()) {
                return $"{ToCoordinatesString(3, false)}";
            }
            else {
                return $"{DungeonName} {ToLocalString()}";
            }
        }
    }
}
