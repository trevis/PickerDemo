﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    public interface ISerializable {
        bool Serialize(BinaryWriter writer);
        bool Deserialize(BinaryReader reader);
    }
}
