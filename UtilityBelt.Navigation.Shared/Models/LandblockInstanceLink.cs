﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class LandblockInstanceLink {
        public uint ParentGuid { get; set; }
        public uint ChildGuid { get; set; }
    }
}