﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class WaypointRoute {
        public Frame StartFrame { get; set; } = new Frame();
        public Frame EndFrame { get; set; } = new Frame();
        public List<Waypoint> Waypoints { get; set; } = new List<Waypoint>();
        public WaypointRouteStatus Status { get; set; }

    }
}
