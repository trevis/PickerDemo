﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    public class Lever : POIBase {

        public Lever() { }

        public override bool Serialize(BinaryWriter writer) {
            base.Serialize(writer);
            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            base.Deserialize(reader);
            return true;
        }
    }
}
