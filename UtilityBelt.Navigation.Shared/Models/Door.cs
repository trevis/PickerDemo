﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    public class Door : POIBase {
        public DoorOpenType OpenType { get; set; }
        public int LockpickResist { get; set; }
        public uint SetupId { get; set; }
        public Key Key { get; set; } = new Key();
        public List<Lever> Levers { get; set; } = new List<Lever>();

        public Door() { }

        public Door(Frame frame, DoorOpenType openType, int lpResist, Key key, List<Lever> levers) {
            Frame = frame;
            OpenType = openType;
            LockpickResist = lpResist;
            Key = key;
            Levers = levers;
        }

        public override bool Serialize(BinaryWriter writer) {
            base.Serialize(writer);
            writer.Write((byte)OpenType);
            writer.Write(LockpickResist);
            writer.Write(SetupId);
            Key.Serialize(writer);

            writer.Write((uint)Levers.Count);
            foreach (var lever in Levers) {
                lever.Serialize(writer);
            }

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            base.Deserialize(reader);
            OpenType = (DoorOpenType)reader.ReadByte();
            LockpickResist = reader.ReadInt32();
            SetupId = reader.ReadUInt32();
            Key.Deserialize(reader);

            var leverCount = reader.ReadUInt32();
            for (var i = 0; i < leverCount; i++) {
                var lever = new Lever();
                lever.Deserialize(reader);
                Levers.Add(lever);
            }

            return true;
        }
    }
}
