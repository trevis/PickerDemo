﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Models {
    [Serializable]
    public class UseRequirements {
        public int MinLevel { get; set; }
        public string QuestRestriction { get; set; } = "";
        public List<Key> Keys { get; set; } = new List<Key>();
        public int LockpickResist { get; set; }

        public bool Serialize(BinaryWriter writer) {
            writer.Write(MinLevel);
            writer.Write(QuestRestriction);
            writer.Write(LockpickResist);
            writer.Write((uint)Keys.Count);
            foreach (var key in Keys) {
                key.Serialize(writer);
            }

            return true;
        }

        public bool Deserialize(BinaryReader reader) {
            MinLevel = reader.ReadInt32();
            QuestRestriction = reader.ReadString();
            LockpickResist = reader.ReadInt32();
            var keyCount = reader.ReadUInt32();
            for (var i = 0; i < keyCount; i++) {
                var key = new Key();
                key.Deserialize(reader);
                Keys.Add(key);
            }

            return true;
        }

        public bool CanMeet(int level, List<string> questFlags) {
            if (level < MinLevel)
                return false;
            if (!string.IsNullOrWhiteSpace(QuestRestriction) && questFlags.Count == 0) // !questFlags.Contains(QuestRestriction))
                return false;
            if (LockpickResist > 0 && questFlags.Count == 0)
                return false;
            if (Keys != null && Keys.Count > 0 && questFlags.Count == 0)
                return false;

            return true;
        }

        public override string ToString() {
            List<string> details = new List<string>();
            if (MinLevel > 0)
                details.Add($"MinLevel: {MinLevel}");
            if (!string.IsNullOrEmpty(QuestRestriction))
                details.Add($"QuestFlag: {QuestRestriction}");
            if (Keys != null && Keys.Count > 0)
                details.Add($"Keys: ({string.Join(", ", Keys.Select(k => k.Name).ToArray())})");
            if (LockpickResist > 0)
                details.Add($"LockpickResist: {LockpickResist}");

            return details.Count > 0 ? $"({string.Join(", ", details.ToArray())})" : "";
        }
    }
}
