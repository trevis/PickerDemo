﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    public class Portal : POIBase {
        public Frame Destination { get; set; } = new Frame();
        public PortalBitmask Bitmask { get; set; }
        public PortalTravelType TravelType { get; set; }
        public UseRequirements Requirements { get; set; } = new UseRequirements();

        public Portal() { }

        public override bool Serialize(BinaryWriter writer) {
            base.Serialize(writer);
            writer.Write((ushort)Bitmask);
            writer.Write((byte)TravelType);
            Destination.Serialize(writer);
            Requirements.Serialize(writer);

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            base.Deserialize(reader);
            Bitmask = (PortalBitmask)reader.ReadUInt16();
            TravelType = (PortalTravelType)reader.ReadByte();
            Destination.Deserialize(reader);
            Requirements.Deserialize(reader);

            return true;
        }
    }
}
