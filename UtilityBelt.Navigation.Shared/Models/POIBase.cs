﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Models {
    public class POIBase : ISerializable {
        public LocationType LocationType { get; set; } = LocationType.Unknown;
        public string Name { get; set; } = "";
        //public string Description { get; set; } = "";
        public uint WeenieId { get; set; } = 0;
        public Frame Frame { get; set; } = new Frame();

        public virtual bool Serialize(BinaryWriter writer) {
            writer.Write(WeenieId);
            writer.Write((byte)LocationType);
            writer.Write(Name);
            //writer.Write(Description);
            Frame.Serialize(writer);

            return true;
        }

        public virtual bool Deserialize(BinaryReader reader) {
            WeenieId = reader.ReadUInt32();
            LocationType = (LocationType)reader.ReadByte();
            Name = reader.ReadString();
            //Description = reader.ReadString();
            Frame.Deserialize(reader);

            return true;
        }
    }
}
