﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Numerics;

namespace UtilityBelt.Navigation.Shared.Models {
    public class Polygon2d {
        private static long _id = Int64.MinValue;

        public List<Vector2i> Vertices { get; } = new List<Vector2i>();

        public bool CCW { get => SignedArea() > 0; }
        public ushort Lbid { get; set; }
        public uint RegionId { get; set; }

        public long Id { get; } = _id++;

        public LBNode LBNode { get; set; }

        public Polygon2d() { }

        public Polygon2d(List<Vector2i> vertices, ushort lbid, uint regionId = 0) {
            Vertices.AddRange(vertices);

            RegionId = 0;

            Lbid = lbid;
        }

        internal void Serialize(BinaryWriter writer) {
            writer.Write(Lbid);
            writer.Write(RegionId);
            writer.Write((uint)Vertices.Count);
            foreach (var v in Vertices) {
                writer.Write(v.X);
                writer.Write(v.Y);
            }
        }

        internal void Deserialize(BinaryReader reader) {
            Vertices.Clear();

            Lbid = reader.ReadUInt16();
            RegionId = reader.ReadUInt32();
            var verticeCount = reader.ReadUInt32();
            for (var i = 0; i < verticeCount; i++) {
                var x = reader.ReadInt32();
                var y = reader.ReadInt32();
                Vertices.Add(new Vector2i(x, y));
            }
        }

        // needs fixing...
        public bool SharesEdge(Polygon2d checkPoly) {
            if (CCW)
                Vertices.Reverse();

            if (checkPoly.CCW)
                checkPoly.Vertices.Reverse();

            for (var vi = 0; vi < Vertices.Count; vi++) {
                var curVert = Vertices[vi];
                for (var ci = checkPoly.Vertices.Count - 1; ci >= 0; ci--) {
                    var checkVert = checkPoly.Vertices[ci];
                    if (checkVert.X == curVert.X && checkVert.Y == curVert.Y) {
                        var nextCheckVert = ci < checkPoly.Vertices.Count - 2 ? checkPoly.Vertices[ci + 1] : checkPoly.Vertices[0];
                        var nextCurVert = vi > 0 ? Vertices[vi - 1] : Vertices.Last();
                        if (nextCurVert.X == nextCheckVert.X && nextCurVert.Y == nextCheckVert.Y) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        // needs fixing...
        public void Union(Polygon2d checkPoly) {
            var currentVertices = Vertices.ToList();
            var checkVertices = checkPoly.Vertices.ToList();
            var sharedVertices = new List<Vector2i>();

            // find shared edges
            for (var vi = 0; vi < Vertices.Count; vi++) {
                var curVert = Vertices[vi];
                for (var ci = checkPoly.Vertices.Count - 1; ci >= 0; ci--) {
                    var checkVert = checkPoly.Vertices[ci];
                    if (curVert.Equals(checkVert)) {
                        var nextCheckVert = ci < checkPoly.Vertices.Count - 2 ? checkPoly.Vertices[ci + 1] : checkPoly.Vertices[0];
                        var nextCurVert = vi > 0 ? Vertices[vi - 1] : Vertices.Last();
                        if (nextCurVert.X == nextCheckVert.X && nextCurVert.Y == nextCheckVert.Y) {
                            if (!sharedVertices.Contains(nextCurVert))
                                sharedVertices.Add(nextCurVert);
                            if (!sharedVertices.Contains(nextCheckVert))
                                sharedVertices.Add(nextCheckVert);
                            if (!sharedVertices.Contains(curVert))
                                sharedVertices.Add(curVert);
                            if (!sharedVertices.Contains(checkVert))
                                sharedVertices.Add(checkVert);
                        }
                    }
                }
            }

            // remove sharedVertices and keep track of removedIndex
            var insertIndex = -1;
            for (var i = 0; i < Vertices.Count; i++) {
                if (sharedVertices.Contains(Vertices[i])) {
                    currentVertices.Remove(Vertices[i]);
                    if (insertIndex < 0)
                        insertIndex = i;
                }
            }

            var cinsertIndex = -1;
            for (var i = 0; i < checkPoly.Vertices.Count; i++) {
                if (sharedVertices.Contains(checkPoly.Vertices[i])) {
                    checkVertices.Remove(checkPoly.Vertices[i]);
                    if (cinsertIndex < 0)
                        cinsertIndex = i;
                }
            }

            // re-order checkPoly so we can insert its cleaned vertices into currentPoly
            if (cinsertIndex != 0 && cinsertIndex != checkVertices.Count - 1 && cinsertIndex != -1) {
                var newCheckVertices = new List<Vector2i>();
                newCheckVertices.AddRange(checkVertices.GetRange(cinsertIndex, checkVertices.Count - cinsertIndex));
                newCheckVertices.AddRange(checkVertices.GetRange(0, cinsertIndex));
                checkVertices = newCheckVertices;
            }

            currentVertices.InsertRange(insertIndex, checkVertices);
            Vertices.Clear();
            Vertices.AddRange(currentVertices);
        }

        public bool SharesVerticeWith(Polygon2d checkPoly) {
            for (var vi = 0; vi < Vertices.Count; vi++) {
                var curVert = Vertices[vi];
                for (var ci = 0; ci < checkPoly.Vertices.Count; ci++) {
                    var checkVert = checkPoly.Vertices[ci];
                    if (checkVert.X == curVert.X && checkVert.Y == curVert.Y) {
                        return true;
                    }
                }
            }
            return false;
        }

        public Vector2i GetSharedVertice(Polygon2d checkPoly) {
            for (var vi = 0; vi < Vertices.Count; vi++) {
                var curVert = Vertices[vi];
                for (var ci = 0; ci < checkPoly.Vertices.Count; ci++) {
                    var checkVert = checkPoly.Vertices[ci];
                    if (checkVert.X == curVert.X && checkVert.Y == curVert.Y) {
                        return checkVert;
                    }
                }
            }
            return new Vector2i();
        }

        public bool OverlapsOrTouches(Polygon2d checkPoly) {
            foreach (var v in checkPoly.Vertices) {
                if (PointIsInPoly(v, Vertices))
                    return true;
            }
            
            foreach (var v in Vertices) {
                if (PointIsInPoly(v, checkPoly.Vertices))
                    return true;
            }

            return false;
        }

        static bool PointIsInPoly(Vector2i pnt, List<Vector2i> vertices) {
            int i, j;
            int nvert = vertices.Count;
            bool c = false;
            for (i = 0, j = nvert - 1; i < nvert; j = i++) {
                if (((vertices[i].Y > pnt.Y) != (vertices[j].Y > pnt.Y)) &&
                 (pnt.X <= (vertices[j].X - vertices[i].X) * (pnt.Y - vertices[i].Y) / (vertices[j].Y - vertices[i].Y) + vertices[i].X))
                    c = !c;
            }
            return c;
        }

        public List<List<Vector2>> GetSharedEdgesWith(Polygon2d checkPoly) {
            var sharedEdges = new List<List<Vector2>>();

            for (var vi = 0; vi < Vertices.Count; vi++) {
                var curVert = Vertices[vi];
                var nextCurVert = vi > 0 ? Vertices[vi - 1] : Vertices.Last();
                for (var ci = 0; ci < checkPoly.Vertices.Count; ci++) {
                    var checkVert = checkPoly.Vertices[ci];
                    var nextCheckVert = ci > 0 ? checkPoly.Vertices[ci - 1] : checkPoly.Vertices.Last();

                    if ((checkVert.Equals(nextCurVert) && nextCheckVert.Equals(curVert)) || (checkVert.Equals(curVert) && nextCheckVert.Equals(nextCurVert))) {
                        sharedEdges.Add(new List<Vector2>() {
                            new Vector2(checkVert.X, checkVert.Y),
                            new Vector2(nextCheckVert.X, nextCheckVert.Y),
                        });
                        // Program.Log($"Found shared verts edge");
                        continue;
                    }
                    
                    var intersectionResults = Intersector.Intersection(
                        new System.Drawing.PointF(curVert.X, curVert.Y),
                        new System.Drawing.PointF(nextCurVert.X, nextCurVert.Y),
                        new System.Drawing.PointF(checkVert.X, checkVert.Y),
                        new System.Drawing.PointF(nextCheckVert.X, nextCheckVert.Y)
                        );
                    //Program.Log($"{intersectionResults.Length}");
                    if (intersectionResults.Length >= 2) {
                        sharedEdges.Add(new List<Vector2>() {
                            new Vector2(intersectionResults[0].X, intersectionResults[0].Y),
                            new Vector2(intersectionResults[1].X, intersectionResults[1].Y),
                        });
                    }
                }
            }

            // todo, no shared edges so return shared vertices;

            return sharedEdges;
        }

        private float SignedArea() {
            float signedArea = 0;
            for (var i = 0; i < Vertices.Count; i++) {
                Vector2i v1 = Vertices[i];
                Vector2i v2;
                if (i == Vertices.Count - 1) {
                    v2 = Vertices[0];
                }
                else {
                    v2 = Vertices[i + 1];
                }

                signedArea += (v1.X * v2.Y - v2.X * v1.Y);
            }

            return signedArea / 2f;
        }

        public Polygon2d Clone() {
            return new Polygon2d(Vertices.Select(v => new Vector2i(v.X, v.Y)).ToList(), Lbid, RegionId);
        }

        public bool ContainsPoint(Frame frame) {
            var x = (frame.Landblock >> 24) * 192f;
            var y = (((frame.Landblock >> 16) & 0xFF) * 192f);
            x += frame.LocalX;
            y += frame.LocalY;
            bool result = false;
            int j = Vertices.Count() - 1;
            for (int i = 0; i < Vertices.Count(); i++) {
                if ((float)Vertices[i].Y < y && (float)Vertices[j].Y >= y || (float)Vertices[j].Y < y && (float)Vertices[i].Y >= y) {
                    if ((float)Vertices[i].X + (x - (float)Vertices[i].Y) / ((float)Vertices[j].Y - (float)Vertices[i].Y) * ((float)Vertices[j].X - (float)Vertices[i].X) < x) {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
    }
}
