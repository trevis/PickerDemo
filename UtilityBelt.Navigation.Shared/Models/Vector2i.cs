﻿using System;

namespace UtilityBelt.Navigation.Shared.Models {
    public struct Vector2i {
        public int X;
        public int Y;
        public Vector2i(int x, int y) {
            X = x;
            Y = y;
        }

        public float DistanceTo(Vector2i other) {
            return (float)Math.Sqrt(Math.Pow(Math.Abs(X - other.X), 2) + Math.Pow(Math.Abs(Y - other.Y), 2));
        }

        public override bool Equals(object obj) {
            if (obj is Vector2i v2)
                return X == v2.X && Y == v2.Y;
            return false;
        }
    }
}