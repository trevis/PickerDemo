﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Data {
    public class LandblockInfoStore : IDataStore {
        // bump this number and it will invalidate all old store files that arent this version,
        // causing them to be regenerated
        public const uint VERSION = 2;

        private const int MAP_SIZE = 255;

        public LandblockFlag[,] MapData { get; } = new LandblockFlag[MAP_SIZE, MAP_SIZE];

        public override StoreType GetStoreType() {
            return StoreType.LandblockInfo;
        }

        public override uint GetStoreVersion() {
            return VERSION;
        }

        public override bool Serialize(BinaryWriter writer) {
            if (!base.Serialize(writer))
                return false;

            for (var x = 0; x < MAP_SIZE; x++) {
                for (var y = 0; y < MAP_SIZE; y++) {
                    writer.Write((ushort)MapData[x, y]);
                }
            }

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            if (!base.Deserialize(reader))
                return false;

            for (var x = 0; x < MAP_SIZE; x++) {
                for (var y = 0; y < MAP_SIZE; y++) {
                    MapData[x, y] = (LandblockFlag)reader.ReadUInt16();
                }
            }

            return true;
        }
    }
}
