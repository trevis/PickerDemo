﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using StoreVersion = System.UInt32;

namespace UtilityBelt.Navigation.Shared.Data {
    public abstract class IDataStore {
        public const StoreVersion DATA_VERSION = 1;

        public enum StoreType : ushort {
            LandblockInfo = 0x0001,
            LandblockMesh = 0x0002,
            LandblockGraph = 0x0003,
            PointsOfInterestDB = 0x0004,
            EmuContentStore = 0x0005,
            LandblockGraphStore = 0x0006
        }

        public abstract StoreVersion GetStoreVersion();
        public abstract StoreType GetStoreType();

        public virtual bool Serialize(BinaryWriter writer) {
            writer.Write(DATA_VERSION);
            writer.Write((ushort)GetStoreType());
            writer.Write(GetStoreVersion());

            return true;
        }

        private bool HasValidHeader(BinaryReader reader) {
            var dataVersion = (StoreVersion)reader.ReadUInt32();
            var storeType = (StoreType)reader.ReadUInt16();
            var storeVersion = (StoreVersion)reader.ReadUInt32();

            if (dataVersion != DATA_VERSION || storeType != GetStoreType() || storeVersion != GetStoreVersion())
                return false;

            return true;
        }
        
        public virtual bool Deserialize(BinaryReader reader) {
            return HasValidHeader(reader);
        }

        public virtual bool CanDeserializeFromFile(string filename) {
            if (!System.IO.File.Exists(filename))
                return false;

            using (var stream = System.IO.File.OpenRead(filename))
            using (var compressedStream = new GZipStream(stream, CompressionMode.Decompress))
            using (var reader = new BinaryReader(compressedStream)) {
                //if (!compressedStream.CanRead || compressedStream.Length < sizeof(StoreType) + sizeof(StoreVersion) + sizeof(StoreVersion))
                //    return false;
                return HasValidHeader(reader);
            }
        }

        public bool TryDeserialize(string outputFile) {
            //if (CanDeserializeFromFile(outputFile)) {
            try {
                using (var stream = System.IO.File.OpenRead(outputFile))
                using (var compressedStream = new GZipStream(stream, CompressionMode.Decompress))
                using (var reader = new BinaryReader(compressedStream)) {
                    if (Deserialize(reader)) {
                        return true;
                    }
                }
            }
            catch { }
            //}

            return false;
        }

        public bool TrySerialize(string outputFile) {
            if (System.IO.File.Exists(outputFile))
                System.IO.File.Delete(outputFile);

            using (var stream = System.IO.File.OpenWrite(outputFile))
            using (var compressedStream = new GZipStream(stream, CompressionMode.Compress))
            using (var writer = new BinaryWriter(compressedStream)) {
                if (Serialize(writer)) {
                    return true;
                }
            }

            return false;
        }
    }
}
