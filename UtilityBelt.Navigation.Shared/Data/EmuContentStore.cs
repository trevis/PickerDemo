﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Models;

namespace UtilityBelt.Navigation.Shared.Data {
    public class EmuContentStore : IDataStore {
        // bump this number and it will invalidate all old store files that arent this version,
        // causing them to be regenerated
        public const uint VERSION = 1;

        public List<Portal> Portals { get; } = new List<Portal>();

        public List<Door> Doors { get; } = new List<Door>();

        public override StoreType GetStoreType() {
            return StoreType.EmuContentStore;
        }

        public override uint GetStoreVersion() {
            return VERSION;
        }

        public override bool Serialize(BinaryWriter writer) {
            if (!base.Serialize(writer))
                return false;

            writer.Write((uint)Portals.Count);
            foreach (var portal in Portals) {
                portal.Serialize(writer);
            }

            writer.Write((uint)Doors.Count);
            foreach (var door in Doors) {
                door.Serialize(writer);
            }

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            if (!base.Deserialize(reader))
                return false;

            var portalCount = reader.ReadUInt32();
            for (var i = 0; i < portalCount; i++) {
                var portal = new Portal();
                portal.Deserialize(reader);
                Portals.Add(portal);
            }

            var doorCount = reader.ReadUInt32();
            for (var i = 0; i < doorCount; i++) {
                var door = new Door();
                door.Deserialize(reader);
                Doors.Add(door);
            }

            return true;
        }
    }
}
