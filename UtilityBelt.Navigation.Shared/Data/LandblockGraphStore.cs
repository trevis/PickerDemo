﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Models;

namespace UtilityBelt.Navigation.Shared.Data {
    public class LandblockGraphStore : IDataStore {
        // bump this number and it will invalidate all old store files that arent this version,
        // causing them to be regenerated

        protected static Random rand = new Random();
        public uint VERSION = 1;// (uint)rand.Next(int.MinValue, int.MaxValue);

        public LBGraph Graph = new LBGraph();

        public override StoreType GetStoreType() {
            return StoreType.LandblockGraphStore;
        }

        public override uint GetStoreVersion() {
            return VERSION;
        }

        public override bool Serialize(BinaryWriter writer) {
            if (!base.Serialize(writer))
                return false;

            Graph.Serialize(writer);

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            if (!base.Deserialize(reader))
                return false;

            Graph.Deserialize(reader);

            return true;
        }
    }
}
