﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Models;

namespace UtilityBelt.Navigation.Shared.Data {
    public class LandblockMeshStore : IDataStore {
        // bump this number and it will invalidate all old store files that arent this version,
        // causing them to be regenerated
        public const uint VERSION = 1;

        public List<Polygon2d> Polygons { get; } = new List<Polygon2d>();
        public ushort[,] CellHeights = new ushort[255 * 8, 255 * 8];

        public LandblockMeshStore() {
            for (var x = 0; x < 255 * 8; x++) {
                for (var y = 0; y < 255 * 8; y++) {
                    CellHeights[x, y] = 0;
                }
            }
        }

        public override StoreType GetStoreType() {
            return StoreType.LandblockMesh;
        }

        public override uint GetStoreVersion() {
            return VERSION;
        }

        public override bool Serialize(BinaryWriter writer) {
            if (!base.Serialize(writer))
                return false;

            writer.Write((uint)Polygons.Count);
            foreach (var poly in Polygons)
                poly.Serialize(writer);

            for (var x = 0; x < 255 * 8; x++) {
                for (var y = 0; y < 255 * 8; y++) {
                    writer.Write(CellHeights[x, y]);
                }
            }

            return true;
        }

        public override bool Deserialize(BinaryReader reader) {
            if (!base.Deserialize(reader))
                return false;

            var polyCount = reader.ReadUInt32();
            for (var i = 0; i < polyCount; i++) {
                var poly = new Polygon2d();
                poly.Deserialize(reader);
                Polygons.Add(poly);
            }

            for (var x = 0; x < 255 * 8; x++) {
                for (var y = 0; y < 255 * 8; y++) {
                    CellHeights[x, y] = reader.ReadUInt16();
                }
            }

            return true;
        }
    }
}
