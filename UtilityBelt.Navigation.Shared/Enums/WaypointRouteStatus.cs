﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    public enum WaypointRouteStatus : byte {
        Impossible = 0x00,
        NoStartFound = 0x01,
        NoEndFound = 0x02,

        Success = 0xFF
    }
}
