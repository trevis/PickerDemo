﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    [Flags]
    public enum Useable : int {
        Undef = 0x00,
        No = 0x01,
        Self = 0x02,
        Wielded = 0x04,
        Contained = 0x08,
        Viewed = 0x10,
        Remote = 0x20,
        NeverWalk = 0x40,
        ObjSelf = 0x80,
    }
}
