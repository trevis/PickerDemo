﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    [Serializable]
    public enum PortalTravelType : byte {
        Invalid = 0x00,
        LandscapeToDungeon = 0x01,
        LandscapeToLandscape = 0x02,
        DungeonToLandscape = 0x03,
        DungeonToDungeon = 0x04,
        DungeonInternal = 0x05
    }
}
