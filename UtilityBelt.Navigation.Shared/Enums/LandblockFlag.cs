﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    [Serializable]
    public enum LandblockFlag : ushort {
        Nothing = 0x0000,
        Dungeon = 0x0001,
        Landscape = 0x0002,
        Unwalkable = 0x0004,
        HasDeepSea = 0x0008,
        HasImpassibleSlopes = 0x0010,
        AllImpassibleSlopes = 0x0020,
    }
}
