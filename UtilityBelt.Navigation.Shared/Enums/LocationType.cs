﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    public enum LocationType : byte {
        Unknown = 0x00,
        Landscape = 0x01,
        Dungeon = 0x02,
        Platform = 0x03,
        LandscapeBuilding = 0x04,
        Portal = 0x05,
        PortalExit = 0x6
    }
}
