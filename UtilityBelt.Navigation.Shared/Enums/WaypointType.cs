﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    public enum WaypointType : byte {
        Unknown = 0x00,
        Run = 0x01,
        Jump = 0x02,
        UseObject = 0x03,
        UsePortal = 0x04,
        UseNPC = 0x05,
        UseVendor = 0x06,

        Start = 0xFE,
        End = 0xFF
    }
}
