﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Enums {
    [Flags]
    [Serializable]
    public enum DoorOpenType : byte {
        Unlocked = 0x00,
        Lockpick = 0x01,
        LeverOrButton = 0x02,
        Key = 0x04,

        Unknown = 0x80
    }
}
