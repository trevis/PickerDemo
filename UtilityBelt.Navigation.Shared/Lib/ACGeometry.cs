﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Navigation.Shared.Lib {
    public static class ACGeometry {
        public static double MaxCoord = 102.15f;
        public static double C = (255.0 * 192.0) / (MaxCoord * 2.0);

        public static uint GetLandblockFromCoordinates(float EW, float NS) {
            return GetLandblockFromCoordinates((double)EW, (double)NS);
        }

        public static uint GetLandblockFromCoordinates(double EW, double NS) {
            uint x = (uint)Math.Floor((EW + MaxCoord) * C / 192f);
            uint y = (uint)Math.Floor((NS + MaxCoord) * C / 192f);
            return ((x << 8) + y) << 16;
        }

        public static int LandblockXDifference(uint originLandblock, uint landblock) {
            var olbx = originLandblock >> 24;
            var lbx = landblock >> 24;

            return (int)(lbx - olbx) * 192;
        }

        public static int LandblockYDifference(uint originLandblock, uint landblock) {
            var olby = originLandblock << 8 >> 24;
            var lby = landblock << 8 >> 24;

            return (int)(lby - olby) * 192;
        }

        public static float NSToLandblock(uint landcell, float ns) {
            return (float)(((ns + MaxCoord) * C) % 192f);
        }

        public static float EWToLandblock(uint landcell, float ew) {
            return (float)(((ew + MaxCoord) * C) % 192f);
        }

        internal static double RelativeAngleOffset(double originAngle, double offsetAngle) {
            var diff = (originAngle - offsetAngle) - 180.0;
            diff /= 360.0;
            return ((diff - Math.Floor(diff)) * 360.0) - 180.0;
        }

        public static float LandblockToNS(uint landcell, float yOffset) {
            var y = ((landcell >> 16) & 0xFF) * 192f + yOffset;
            return (float)((y / C) - MaxCoord);
        }

        public static float LandblockToEW(uint landcell, float xOffset) {
            var x = (landcell >> 24) * 192f + xOffset;
            return (float)((x / C) - MaxCoord);
        }

        public static float Distance2d(float x1, float y1, float x2, float y2) {
            return (float)Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }

}
