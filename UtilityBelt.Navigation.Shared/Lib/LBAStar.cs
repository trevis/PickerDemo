﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Navigation.Shared.Models;
using UtilityBelt.Navigation.Shared.Enums;

namespace UtilityBelt.Navigation.Shared.Lib
{
    public class LBAStar
    {
        public struct LBTravelledNode
        {
            public LBNode Node;
            public LBEdge Edge;
        }

        static public double Heuristic(LBNode a, LBNode b, LBEdge edge = null)
        {
            double mod = 1;
            // dungeons cost more to navigate
            if (b.Type == LocationType.Dungeon || a.Type == LocationType.Dungeon)
                mod = 3;
            // portals are fixed cost
            // TODO: how far can you run in the time it takes to enter/exit portalspace?
            if (a.Type == LocationType.Portal)
                return 0;

            //if (edge != null)
            //    return a.Frame.DistanceTo(edge.StartFrame) * mod;
            //else
                return a.DistanceTo(b) * mod;
        }

        public static List<LBTravelledNode> FindPath(LBGraph graph, LBNode start, LBNode goal, List<string> questFlags, int minLevel, out float cost, out uint checkedNodeCount)
        {
            cost = 0;
            checkedNodeCount = 0;
            bool hasPath = false;
            LBNode current = null;
            Dictionary<LBNode, LBNode> cameFrom = new Dictionary<LBNode, LBNode>();
            Dictionary<LBNode, double> costSoFar = new Dictionary<LBNode, double>();
            var frontier = new PriorityQueue<LBNode>();
            frontier.Enqueue(start, 0);

            cameFrom[start] = start;
            costSoFar[start] = 0;

            while (frontier.Count > 0)
            {
                current = frontier.Dequeue();
                checkedNodeCount++;

                if (current.Equals(goal))
                {
                    cost = (float)costSoFar[current];
                    hasPath = true;
                    break;
                }

                for (var i = 0; i < current.Edges.Count; i++)
                {
                    var next = current.EdgeNodes[i];
                    if (next.Requirements != null && !next.Requirements.CanMeet(minLevel, questFlags))
                        continue;

                    double newCost = costSoFar[current] + Heuristic(current, next, current.Edges[i]);
                    if (!costSoFar.ContainsKey(next) || newCost < costSoFar[next])
                    {
                        costSoFar[next] = newCost;
                        double priority = newCost;// + Heuristic(next, goal);
                        frontier.Enqueue(next, priority);
                        cameFrom[next] = current;
                    }
                }
            }

            if (!hasPath)
            {
                return null;
            }

            var currentNode = goal;
            var path = new List<LBNode>();

            while (currentNode != start)
            {
                path.Add(currentNode);
                currentNode = cameFrom[currentNode];
            }
            path.Add(start);
            path.Reverse();

            // clean path
            var cleanPath = new List<LBTravelledNode>();
            currentNode = path.First();
            path.RemoveAt(0);
            while (path.Count > 0)
            {
                var nextNodeId = path.Count > 2 ? path.Skip(2).First().Id : goal.Id;
                cleanPath.Add(new LBTravelledNode()
                {
                    Node = currentNode,
                    Edge = currentNode.Edges.Find(e => e.OtherNodeId == nextNodeId)
                });
                currentNode = path.First();
                path.RemoveAt(0);
            }
            cleanPath.Add(new LBTravelledNode()
            {
                Node = goal
            });

            return cleanPath;
        }
    }
}
