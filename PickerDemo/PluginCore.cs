﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using PickerDemo.Lib;
using PickerDemo.Lib.Autonav;
using PickerDemo.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service.Views;

namespace PickerDemo {
    /// <summary>
    /// This is the main plugin class. When your plugin is loaded, Startup() is called, and when it's unloaded Shutdown() is called.
    /// </summary>
    [FriendlyName("PickerDemo")]
    public class PluginCore : PluginBase {
        private static string _assemblyDirectory = null;
        private ExampleUI ui;

        internal static Guid IID_IDirect3DDevice9 = new Guid("{D0223B96-BF7A-43fd-92BD-A43B0D82B9EB}");
        internal IntPtr unmanagedD3dPtr;

        internal static Device D3Ddevice { get; private set; }
        internal Picker Picker { get; private set; }
        internal CameraH CameraH { get; private set; }
        internal AutoNav Nav { get; private set; }


        internal List<ITool> Tools = new List<ITool>();

        internal UtilityBelt.Scripting.Interop.Game Game = new UtilityBelt.Scripting.Interop.Game();

        /// <summary>
        /// Assembly directory containing the plugin dll
        /// </summary>
        public static string AssemblyDirectory {
            get {
                if (_assemblyDirectory == null) {
                    try {
                        _assemblyDirectory = System.IO.Path.GetDirectoryName(typeof(PluginCore).Assembly.Location);
                    }
                    catch {
                        _assemblyDirectory = Environment.CurrentDirectory;
                    }
                }
                return _assemblyDirectory;
            }
            set {
                _assemblyDirectory = value;
            }
        }

        public static PluginCore Instance { get; private set; }

        /// <summary>
        /// Called when your plugin is first loaded.
        /// </summary>
        protected unsafe override void Startup() {
            if (CoreManager.Current.CharacterFilter.LoginStatus == 3) {
                Init();
            }
            else {
                // subscribe to CharacterFilter_LoginComplete event, make sure to unscribe later.
                // note: if the plugin was reloaded while ingame, this event will never trigger on the newly reloaded instance.
                CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            }
        }

        private unsafe void Init() {
            try {
                Instance = this;
                object a = CoreManager.Current.Decal.Underlying.GetD3DDevice(ref IID_IDirect3DDevice9);
                Marshal.QueryInterface(Marshal.GetIUnknownForObject(a), ref IID_IDirect3DDevice9, out unmanagedD3dPtr);
                D3Ddevice = new Device(unmanagedD3dPtr);

                ui = new ExampleUI(this);
                Picker = new Picker();
                CameraH = new CameraH();
                //Nav = new AutoNav();

                Tools.Add(new RenderTool());
                Tools.Add(new VisibleCellsTool());
                Tools.Add(new PickerTool());
                Tools.Add(new DragDropTool());
                Tools.Add(new ClientUITool());
                Tools.Add(new TransformTool());
                Tools.Add(new DatTool());
                Tools.Add(new PropTool());
                //Tools.Add(new NavTool());

                CoreManager.Current.RenderFrame += Current_RenderFrame;
                CoreManager.Current.ChatBoxMessage += Current_ChatBoxMessage;

            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e)
        {
            //CoreManager.Current.Actions.InvokeChatParser($"/say t: {e.Text}");
            Log(e.Text);
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try
            {
                CameraH?.Update();
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        internal static void ResetDirectXState() {
            D3Ddevice.Transform.World = Matrix.Identity;

            // thanks paradox for proper dx state resetting
            D3Ddevice.RenderState.Lighting = false;
            D3Ddevice.Lights[0].Enabled = true;
            D3Ddevice.Lights[0].Type = LightType.Directional;
            D3Ddevice.Lights[0].Attenuation0 = 5f;
            D3Ddevice.Lights[0].XPosition = 80;
            D3Ddevice.Lights[0].YPosition = 100;
            D3Ddevice.Lights[0].ZPosition = 19;
            D3Ddevice.Lights[0].XDirection = 1;
            D3Ddevice.Lights[0].YDirection = 1;
            D3Ddevice.Lights[0].ZDirection = -1;
            D3Ddevice.RenderState.FogEnable = false; 
            /*
            _plugin.D3Ddevice.RenderState.FogStart = 0;
            _plugin.D3Ddevice.RenderState.FogEnd = 120;
            _plugin.D3Ddevice.RenderState.FogColor = Color.FromArgb(255, 0, 20, 20);  
            _plugin.D3Ddevice.RenderState.FogDensity = 0.1f;
            */

            D3Ddevice.RenderState.ZBufferEnable = true;
            D3Ddevice.RenderState.ZBufferWriteEnable = true;
            D3Ddevice.RenderState.ZBufferFunction = Compare.LessEqual;

            D3Ddevice.RenderState.MultiSampleAntiAlias = true;
            D3Ddevice.RenderState.AntiAliasedLineEnable = true;

            D3Ddevice.RenderState.CullMode = Cull.None;

            D3Ddevice.RenderState.DiffuseMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.AmbientMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.SpecularMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.EmissiveMaterialSource = ColorSource.Material;

            D3Ddevice.RenderState.AlphaBlendEnable = true;
            D3Ddevice.RenderState.SourceBlend = Blend.SourceAlpha;
            D3Ddevice.RenderState.DestinationBlend = Blend.InvSourceAlpha;

            //we use the alpha test to exclude any alpha pixels
            //from drawing (or updating the zbuffer). This forces transparency to be
            //binary rather than allowing actual blending.

            D3Ddevice.RenderState.ReferenceAlpha = 1;
            D3Ddevice.RenderState.AlphaTestEnable = true;
            D3Ddevice.RenderState.AlphaFunction = Compare.GreaterEqual;

            D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressU, (int)TextureAddress.Wrap);
            D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressV, (int)TextureAddress.Wrap);
            D3Ddevice.SetSamplerState(0, SamplerStageStates.BorderColor, 0x00000000);
            D3Ddevice.SetTextureStageState(0, TextureStageStates.ColorOperation, 4);
        }

        /// <summary>
        /// CharacterFilter_LoginComplete event handler.
        /// </summary>
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try
            {
                Init();
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        /// <summary>
        /// Called when your plugin is unloaded. Either when logging out, closing the client, or hot reloading.
        /// </summary>
        protected override void Shutdown() {
            try {
                foreach (var tool in Tools) {
                    tool.Dispose();
                }
                // make sure to unsubscribe from any events we were subscribed to. Not doing so
                // can cause the old plugin to stay loaded between hot reloads.
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                CoreManager.Current.RenderFrame -= Current_RenderFrame;
                CoreManager.Current.ChatBoxMessage -= Current_ChatBoxMessage;

                // clean up our ui view 
                ui.Dispose();
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        #region logging
        /// <summary>
        /// Log an exception to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="ex"></param>
        internal static void Log(Exception ex) {
            Log(ex.ToString());
        }

        /// <summary>
        /// Log a string to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message) {
            try {
                File.AppendAllText(System.IO.Path.Combine(AssemblyDirectory, "log.txt"), $"{message}\n");

                CoreManager.Current.Actions.AddChatText(message, 1);
            }
            catch { }
        }
        #endregion // logging
    }
}
