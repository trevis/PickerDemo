﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using PickerDemo.Lib;
using System;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service;
using Vector3 = System.Numerics.Vector3;
using PickerDemo.Lib.Extensions;
using ACClientLib.DatReaderWriter;
using ACClientLib.DatReaderWriter.Options;
using ACClientLib.DatReaderWriter.DBObjs;
using System.Collections.Generic;
using System.Linq;
using Microsoft.DirectX.Direct3D;
using System.Globalization;
using ACClientLib.DatReaderWriter.IO;

namespace PickerDemo.Tools
{
    public unsafe class TransformTool : ITool
    {
        public enum SelectionMode
        {
            StaticObject,
            SpawnStaticObject,
            WorldObject,
            Terrain
        };

        public string Name => "Transform";
        public bool IsActive { get; set; }
        public SelectionMode CurrentSelectionMode { get; set; }
        public PhysicsObjTransformer ObjTransformer { get; }

        private CPhysicsObj* _selectedPhysicsObject = null;
        private bool _isSelectingPhysicsObject = false;
        private int _selectedObjIndex;
        private uint _selectedCellId;
        private bool _selectedIsOutside;
        private readonly string[] _availableSelectionModes;
        private readonly DatDatabaseReader cellDat;
        private readonly List<int> _extraObjects = new();

        private struct StabInfo
        {
            public Coordinates Coords;
            public uint SetupId;
            public StabInfo(Coordinates coords, uint setupId)
            {
                Coords = coords;
                SetupId = setupId;
            }
        }

        private readonly Dictionary<uint, List<StabInfo>> _datStabs = new();

        public TransformTool()
        {
            ObjTransformer = new PhysicsObjTransformer();

            _availableSelectionModes = System.Enum.GetNames(typeof(SelectionMode));
            cellDat = new DatDatabaseReader((opts) =>
            {
                opts.FilePath = @"C:\Turbine\Asheron's Call\client_cell_1.dat";
                opts.AccessType = DatAccessType.ReadWrite;
            });
        }

        public void Render()
        {
            try
            {
                var selectionMode = (int)CurrentSelectionMode;
                if (ImGui.Combo("Selection Mode", ref selectionMode, _availableSelectionModes, _availableSelectionModes.Length))
                {
                    CurrentSelectionMode = (SelectionMode)selectionMode;
                    _isSelectingPhysicsObject = false;
                }

                switch (CurrentSelectionMode)
                {
                    case SelectionMode.WorldObject:
                        RenderWorldObjectSelection();
                        break;
                    case SelectionMode.StaticObject:
                        RenderStaticObjectSelection();
                        break;
                    case SelectionMode.Terrain:
                        RenderTerrainSelection();
                        break;
                    case SelectionMode.SpawnStaticObject:
                        RenderStaticObjectSpawner();
                        break;
                }
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        private string _spawnSetupId = "0x020009A1";
        private uint _nextId;
        private Position _position;



        public static uint GetLandblockFromCoordinates(float EW, float NS)
        {
            return GetLandblockFromCoordinates((double)EW, (double)NS);
        }

        public static uint GetLandblockFromCoordinates(double EW, double NS)
        {
            NS -= 0.5f;
            EW -= 0.5f;
            NS *= 10.0f;
            EW *= 10.0f;

            uint basex = (uint)(EW + 0x400);
            uint basey = (uint)(NS + 0x400);

            if ((int)(basex) < 0 || (int)(basey) < 0 || basex >= 0x7F8 || basey >= 0x7F8)
            {
                Console.WriteLine("Out of Bounds");
            }
            byte blockx = (byte)(basex >> 3);
            byte blocky = (byte)(basey >> 3);
            byte cellx = (byte)(basex & 7);
            byte celly = (byte)(basey & 7);

            int block = (blockx << 8) | (blocky);
            int cell = (cellx << 3) | (celly);

            int dwCell = (block << 16) | (cell + 1);

            return (uint)dwCell;
        }

        private void RenderStaticObjectSpawner()
        {
            ImGui.InputText("Setup Id", ref _spawnSetupId, 100);

            uint id = 0;
            if (_spawnSetupId.StartsWith("0x"))
            {
                if (!uint.TryParse(_spawnSetupId.Replace("0x", ""), NumberStyles.HexNumber, null, out id))
                {
                    ImGui.Text("Invalid setup or gfxobj id. could not parse hex uint");
                    return;
                }
            }
            else if (!uint.TryParse(_spawnSetupId, out id))
            {
                ImGui.Text("Invalid setup or gfxobj id. could not parse uint");
                return;
            }

            if (ImGui.GetIO().WantCaptureMouse) return;
            var pickedCoords = PluginCore.Instance.Picker.PickTerrain();
            if (pickedCoords is not null)
            {
                // 0x02001B78
                if (ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left] && ImGui.GetIO().KeyCtrl)
                {
                    var newObj = CPhysicsObj.makeObject(id, ++_nextId, 1);
                    _position = new AcClient.Position()
                    {
                        frame = new AcClient.Frame()
                        {
                            m_fOrigin = new AcClient.Vector3()
                            {
                                x = pickedCoords.LocalX,
                                y = pickedCoords.LocalY,
                                z = pickedCoords.LocalZ
                            },
                            qw = newObj->m_position.frame.qw,
                            qx = newObj->m_position.frame.qx,
                            qy = newObj->m_position.frame.qy,
                            qz = newObj->m_position.frame.qz
                        },
                        objcell_id = GetLandblockFromCoordinates(pickedCoords.EW, pickedCoords.NS),

                    };
                    CObjectMaint.s_pcInstance[0]->AddObject(newObj);
                    newObj->m_position = _position;
                    newObj->enter_world(&newObj->m_position);
                    CoreManager.Current.Actions.AddChatText($"Spawning {(int)newObj:X8} {id:X8} @ {pickedCoords}", 1);
                    
                    _datStabs.Clear();
                    _extraObjects.Add((int)newObj);

                    var cellId = Coordinates.Me.LandCell;
                    if (cellDat.TryReadFile<LandBlockInfo>((cellId & 0xFFFF0000) + 0xFFFE, out var lb))
                    {
                        lb.Objects.Add(new ACClientLib.DatReaderWriter.Types.Stab()
                        {
                            Id = id,
                            Frame = new ACClientLib.DatReaderWriter.Types.Frame()
                            {
                                Orientation = new System.Numerics.Quaternion(0, 0, 0, 1),
                                Origin = new System.Numerics.Vector3(pickedCoords.LocalX, pickedCoords.LocalY, pickedCoords.LocalZ),
                            }
                        });
                        if (cellDat.TryWriteFile(lb))
                        {
                            CoreManager.Current.Actions.AddChatText($"Saved {(cellId & 0xFFFF0000) + 0xFFFE}!", 1);
                        }
                        else
                        {
                            CoreManager.Current.Actions.AddChatText($"Failed to save {(cellId & 0xFFFF0000) + 0xFFFE}!", 1);
                        }
                    }
                    else
                    {
                        ImGui.Text($"Unable to read LandblockInfo: 0x{(cellId & 0xFFFF0000) + 0xFFFE:X8}");
                    }
                }
            }
        }

        private void RenderStaticObjectSelection()
        {
            if (ImGui.Button(_isSelectingPhysicsObject ? "Cancel Selection" : "Select Static Object"))
            {
                _isSelectingPhysicsObject = !_isSelectingPhysicsObject;
                _selectedCellId = 0;
                _selectedObjIndex = 0;
                _selectedPhysicsObject = null;
                ObjTransformer.ClearTarget();
            }

            if (_isSelectingPhysicsObject)
            {
                _selectedPhysicsObject = null;
                ObjTransformer.ClearTarget();
                var isOutside = PluginCore.Instance.CameraH.Coordinates.IsOutside();
                if (isOutside)
                {
                    RenderStaticLScapeObjectSelector();
                }
                else
                {
                    RenderStaticCellObjectSelector();
                }
            }

            if (_selectedPhysicsObject is not null)
            {
                ImGui.Text($"Selected physics obj: {(int)_selectedPhysicsObject:X8}");
                if (ImGui.Button("Paste"))
                {
                    var newObj = CPhysicsObj.makeObject(_selectedPhysicsObject);
                    CObjectMaint.s_pcInstance[0]->AddObject(newObj);
                    newObj->enter_world(&_selectedPhysicsObject->m_position);
                    _selectedPhysicsObject = newObj;
                }
                if (ImGui.Button("Save"))
                {
                    CoreManager.Current.Actions.AddChatText($"Saving...", 1);
                    if (_selectedIsOutside)
                    {
                        if (cellDat.TryReadFile<LandBlockInfo>((_selectedCellId & 0xFFFF0000) + 0xFFFE, out var lb))
                        {
                            lb.Objects[_selectedObjIndex].Frame.Orientation = new System.Numerics.Quaternion(
                                _selectedPhysicsObject->m_position.frame.qx,
                                _selectedPhysicsObject->m_position.frame.qy,
                                _selectedPhysicsObject->m_position.frame.qz,
                                _selectedPhysicsObject->m_position.frame.qw
                            );
                            lb.Objects[_selectedObjIndex].Frame.Origin = new System.Numerics.Vector3(
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.x,
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.y,
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.z
                            );
                            if (cellDat.TryWriteFile(lb))
                            {
                                CoreManager.Current.Actions.AddChatText($"Saved {(_selectedCellId & 0xFFFF0000) + 0xFFFE}!", 1);
                            }
                            else
                            {
                                CoreManager.Current.Actions.AddChatText($"Failed to save {(_selectedCellId & 0xFFFF0000) + 0xFFFE}!", 1);
                            }
                        }
                        else
                        {
                            ImGui.Text($"Unable to read LandblockInfo: 0x{(_selectedCellId & 0xFFFF0000) + 0xFFFE:X8}");
                        }
                    }
                    else
                    {
                        if (cellDat.TryReadFile<EnvCell>(_selectedCellId, out var envCell))
                        {
                            envCell.StaticObjects[_selectedObjIndex].Frame.Orientation = new System.Numerics.Quaternion(
                                _selectedPhysicsObject->m_position.frame.qx,
                                _selectedPhysicsObject->m_position.frame.qy,
                                _selectedPhysicsObject->m_position.frame.qz,
                                _selectedPhysicsObject->m_position.frame.qw
                            );
                            envCell.StaticObjects[_selectedObjIndex].Frame.Origin = new System.Numerics.Vector3(
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.x,
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.y,
                                _selectedPhysicsObject->m_position.frame.m_fOrigin.z
                            );
                            if (cellDat.TryWriteFile(envCell))
                            {
                                CoreManager.Current.Actions.AddChatText($"Saved!", 1);
                            }
                            else
                            {
                                CoreManager.Current.Actions.AddChatText($"Failed to save!", 1);
                            }
                        }
                        else
                        {
                            ImGui.Text($"Unable to read EnvCell: 0x{_selectedCellId:X8}");
                        }
                    }
                }
                ObjTransformer.SetTarget(_selectedPhysicsObject);
                ObjTransformer.Render(true);
                RenderPhysicsObject(_selectedPhysicsObject, 0xFF00FFFF);

                _selectedPhysicsObject->update_position();
            }
        }

        private void RenderStaticLScapeObjectSelector()
        {
            var lscape = Client.m_instance[0]->smartbox_->lscape;
            if (lscape is null) return;
            var pickRay = PluginCore.Instance.Picker.GetPickRay();
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;

            Coordinates? bestHit = null;
            CPhysicsObj* bestObj = null;
            int newIndex = -1;
            uint lbid = 0;
            for (var x = 0; x < lscape->mid_width; ++x)
            {
                for (var y = 0; y < lscape->mid_width; ++y)
                {
                    var lb = lscape->land_blocks[x + y * lscape->mid_width];
                    var lbX = (lb->block_coord.x / 8);
                    var lbY = (lb->block_coord.y / 8);
                    var id = (uint)((lbX << 24) + (lbY << 16));
                    EnsureDatStabs(id);

                    if (GetStaticObjectHit(lb, pickRay, out var newBestHit, out var newBestObj, out var newIndex1))
                    {
                        if (bestHit == null || newBestHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                        {
                            bestHit = newBestHit;
                            bestObj = newBestObj;
                            newIndex = newIndex1;
                            lbid = id;
                        }
                    }
                }
            }
            foreach (var x in _extraObjects)
            {
                var obj = (CPhysicsObj*)x;
                if (obj is null) continue;
                if (GetStaticObjectHit(obj, pickRay, out var newBestHit, out var newBestObj, out var newIndex1))
                {
                    if (bestHit == null || newBestHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                    {
                        bestHit = newBestHit;
                        bestObj = newBestObj;
                        newIndex = newIndex1;
                        lbid = obj->m_position.objcell_id;
                    }
                }
            }

            if (bestObj is not null)
            {
                RenderPhysicsObject(bestObj);
                if (ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left])
                {
                    _selectedPhysicsObject = bestObj;
                    _isSelectingPhysicsObject = false;
                    _selectedObjIndex = newIndex;
                    _selectedCellId = lbid;
                    _selectedIsOutside = true;
                    CoreManager.Current.Actions.AddChatText($"Selected {_selectedCellId:X8} @ {_selectedObjIndex}", 1);
                }
            }
        }

        private void EnsureDatStabs(uint id)
        {
            if (_datStabs.ContainsKey(id)) return;
            if (cellDat.TryReadFile<LandBlockInfo>((id & 0xFFFF0000) + 0xFFFE, out var lbInfo))
            {
                _datStabs.Add(id, lbInfo.Objects.Select(o => new StabInfo(
                    new Coordinates(id, o.Frame.Origin.X, o.Frame.Origin.Y, o.Frame.Origin.Z),
                    o.Id
                )).ToList());
            }
        }

        private void RenderStaticCellObjectSelector()
        {
            var pickRay = PluginCore.Instance.Picker.GetPickRay();
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            var cEnvCell = VisibleCellsTool.CEnvCell_GetVisible(SmartBox.smartbox[0]->viewer_cell->pos.objcell_id);

            Coordinates? bestHit = null;
            CPhysicsObj* bestObj = null;
            int newIndex = -1;
            uint cellId = 0;

            if (GetStaticObjectHit(cEnvCell, pickRay, out var newBestHit, out var newBestObj, out var newIndex1))
            {
                if (bestHit == null || newBestHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                {
                    bestHit = newBestHit;
                    bestObj = newBestObj;
                    newIndex = newIndex1;
                    cellId = cEnvCell->cObjCell.pos.objcell_id;
                    _selectedIsOutside = false;
                }
            }
            for (var i = 0; i < cEnvCell->cObjCell.num_stabs; i++)
            {
                var cell = VisibleCellsTool.CEnvCell_GetVisible(cEnvCell->cObjCell.stab_list[i]);
                if (GetStaticObjectHit(cell, pickRay, out var newBestHit2, out var newBestObj2, out var newIndex2))
                {
                    if (bestHit == null || newBestHit2.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                    {
                        bestHit = newBestHit2;
                        bestObj = newBestObj2;
                        newIndex = newIndex2;
                        cellId = cell->cObjCell.pos.objcell_id;
                        _selectedIsOutside = false;
                    }
                }
            }

            if (bestObj is not null)
            {
                RenderPhysicsObject(bestObj);
                if (ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left])
                {
                    _selectedPhysicsObject = bestObj;
                    _isSelectingPhysicsObject = false;
                    _selectedObjIndex = newIndex;
                    _selectedCellId = cellId;
                    CoreManager.Current.Actions.AddChatText($"Selected {cellId:X8} @ {_selectedObjIndex}", 1);
                }
            }
        }

        private bool GetStaticObjectHit(CPhysicsObj* sObj, Ray pickRay, out Coordinates bestHit, out CPhysicsObj* bestObj, out int index)
        {
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            bestHit = null;
            bestObj = null;
            index = -1;
            var objCoords = sObj->m_position.ToCoords();

            CSphere sphere = new CSphere();
            var ret = sObj->GetSelectionSphere(&sphere);
            sphere.center.x = sObj->m_position.frame.m_fOrigin.x;
            sphere.center.y = sObj->m_position.frame.m_fOrigin.y;
            sphere.center.z = sObj->m_position.frame.m_fOrigin.z;
            sphere.radius *= sObj->m_scale;
            if (sphere.radius <= 0.1f) {
                sphere.radius = 2f;
            }

            if (ret > 0 && pickRay.IntersectsSphere(sphere))
            {
                if (bestHit is null || objCoords.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                {
                    bestHit = objCoords;
                    bestObj = sObj;
                }
            }

            return bestHit is not null;
        }

        private bool GetStaticObjectHit(CLandBlock* lb, Ray pickRay, out Coordinates bestHit, out CPhysicsObj* bestObj, out int index)
        {
            bestHit = null;
            bestObj = null;
            index = -1;
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            var lbX = (lb->block_coord.x / 8);
            var lbY = (lb->block_coord.y / 8);

            if (!_datStabs.TryGetValue(cameraCoords.LandCell & 0xFFFF0000, out var objs)) return false;

            for (var i = 0; i < lb->num_static_objects; i++)
            {
                var sObj = lb->static_objects.data[i];
                var objCoords = sObj->m_position.ToCoords();

                var lindex = objs.FindIndex(o =>
                {
                    return o.Coords.DistanceTo(objCoords) < 0.01f;
                });

                if (lindex < 0) continue;

                var xDiff = (lbX - ((objCoords.LandCell >> 24) & 0xFF));
                var yDiff = (lbY - ((objCoords.LandCell >> 16) & 0xFF));

                CSphere sphere = new CSphere();
                var ret = sObj->GetSelectionSphere(&sphere);
                sphere.center.x = sObj->m_position.frame.m_fOrigin.x + (xDiff * -192f);
                sphere.center.y = sObj->m_position.frame.m_fOrigin.y + (yDiff * -192f);
                sphere.center.z = sObj->m_position.frame.m_fOrigin.z;
                sphere.radius *= sObj->m_scale;

                if (ret > 0 && pickRay.IntersectsSphere(sphere))
                {
                    if (bestHit is null || objCoords.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                    {
                        bestHit = objCoords;
                        bestObj = sObj;
                        index = lindex;
                    }
                }
            }

            return bestHit is not null;
        }

        private bool GetStaticObjectHit(CEnvCell* cEnvCell, Ray pickRay, out Coordinates bestHit, out CPhysicsObj* bestObj, out int index)
        {
            bestHit = null;
            bestObj = null;
            index = -1;
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            for (var i = 0; i < cEnvCell->num_static_objects; i++)
            {
                var sObj = cEnvCell->static_objects[i];

                CSphere sphere = new CSphere();
                var ret = sObj->GetSelectionSphere(&sphere);
                sphere.center.x = sObj->m_position.frame.m_fOrigin.x;
                sphere.center.y = sObj->m_position.frame.m_fOrigin.y;
                sphere.center.z = sObj->m_position.frame.m_fOrigin.z;
                sphere.radius *= sObj->m_scale;

                if (ret > 0 && pickRay.IntersectsSphere(sphere))
                {
                    var newHit = sObj->m_position.ToCoords();
                    if (bestHit is null || newHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords))
                    {
                        bestHit = newHit;
                        bestObj = sObj;
                        index = i;
                    }
                }
            }

            return bestHit is not null;
        }

        private void RenderPhysicsObject(CPhysicsObj* obj, uint color = 0xFF00FF00)
        {
            var dl = ImGui.GetBackgroundDrawList();
            for (var j = 0; j < obj->part_array->num_parts; j++)
            {
                var part = obj->part_array->parts[j];
                var partMat = part->pos.ToMatrix();
                for (var k = 0; k < part->gfxobj[0]->num_polygons; k++)
                {
                    var poly = part->gfxobj[0]->polygons[k];
                    for (var q = 0; q < poly.num_pts; q++)
                    {
                        var a = new Vector3(
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].x,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].y,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].z);
                        var idxb = q == 0 ? poly.num_pts - 1 : q - 1;
                        var b = new Vector3(
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].x,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].y,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].z);

                        dl.AddLine(Vector3.Transform(a * obj->m_scale, partMat), Vector3.Transform(b * obj->m_scale, partMat), color, 1f);
                    }
                }
            }
        }

        private void RenderTerrainSelection()
        {
            
        }

        private void RenderWorldObjectSelection()
        {
            var selectedWorldObject = PluginCore.Instance.Game.World.Selected;
            var obj = selectedWorldObject is null ? null : CPhysicsObj.GetObjectA(selectedWorldObject.Id);

            if (selectedWorldObject != null)
            {
                ImGui.Text($"Selected: {(selectedWorldObject is null ? "none" : selectedWorldObject)}");
                ObjTransformer.SetTarget(obj);
                ObjTransformer.Render(true);
            }
            else
            {
                ObjTransformer.ClearTarget();
                return;
            }
        }
        public void Dispose()
        {
            cellDat.Dispose();
        }
    }
}
