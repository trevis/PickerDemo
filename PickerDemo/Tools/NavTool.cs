﻿using AcClient;
using Decal.Adapter;
using DotRecast.Core.Numerics;
using ImGuiNET;
using PickerDemo.Lib.Autonav;
using PickerDemo.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotRecast.Detour;
using DotRecast.Recast.Toolset.Tools;
using UtilityBelt.Service.Lib.ACClientModule;
using System.Diagnostics;
using UtilityBelt.Service.Views;
using UtilityBelt.Service;
using PickerDemo.Lib;

namespace PickerDemo.Tools {
    public class NavTool : ITool {
        private List<RcVec3f> _path;
        private bool _doRenderMesh = true;
        private LandscapeMap _landscapeMap;
        private NavMeshRenderer _navMeshRenderer;
        private AutoNav Nav => PluginCore.Instance.Nav;

        public string Name => "Nav";
        public bool IsActive { get; set; }

        public uint Landblock { get; private set; }

        public NavTool() {
            PluginCore.Instance.Game.OnRender3D += Game_OnRender3D;
            _landscapeMap = new LandscapeMap();
            _navMeshRenderer = new NavMeshRenderer();
        }

        private void Game_OnRender3D(object sender, EventArgs e) {
            var landblock = (uint)CoreManager.Current.Actions.Landcell & 0xFFFF0000;
            if (landblock != Landblock) {
                Landblock = landblock;
                //UpdateLandblock(landblock);
            }
        }

        public unsafe void Render() {
            var dl = ImGui.GetBackgroundDrawList();
            //if (ImGui.Button("Reload")) {
            //    UpdateLandblock(SmartBox.smartbox[0]->viewer.objcell_id & 0xFFFF0000);
            //}

            //ImGui.Checkbox("Render Mesh", ref _doRenderMesh);

            if (_doRenderMesh && Nav?.OutsideMesh is not null) {
                _navMeshRenderer.Render(Nav.OutsideMesh);
            }

            _landscapeMap?.Render();

            //if (Nav.OutsideMesh is not null && ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left]) {
            //    FindPickedPath();
            //}

            if (_path is not null) {
                var cameraPos = PluginCore.Instance.CameraH.Coordinates;
                var offset = new System.Numerics.Vector3(cameraPos.LBX() * -192f, cameraPos.LBY() * -192f, 0);

                for (var i = 1; i < _path.Count; i++) {
                    //ImGui.Text($"{new System.Numerics.Vector3(_path[i - 1].X, _path[i - 1].Z, _path[i - 1].Y) + offset} // {_path[i - 1]}");
                    dl.AddLine(
                        new System.Numerics.Vector3(_path[i - 1].X, _path[i - 1].Z, _path[i - 1].Y) + offset,
                        new System.Numerics.Vector3(_path[i].X, _path[i].Z, _path[i].Y) + offset,
                        0xFF00FF00, 3f);
                }
            }
        }

        private void UpdateLandblock(uint landblock) {
            if (landblock == 0) {
                return;
            }
            CoreManager.Current.Actions.AddChatText($"Starting update...", 1);

            Task.Run(async () => {
                try {
                    var sw = new System.Diagnostics.Stopwatch();
                    sw.Start();

                    await Nav.FindLocalPath(Coordinates.Me, Coordinates.Me);

                    sw.Stop();
                    CoreManager.Current.Actions.AddChatText($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to generate navmesh for {Landblock:X8}", 1);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            });
        }

        private void FindPickedPath() {
            var picked = PluginCore.Instance.Picker.PickTerrain();
            _path = null;
            if (picked is not null) {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                CoreManager.Current.Actions.AddChatText($"Clicked at {picked}", 1);

                List<RcVec3f> pts = GetStitchedPath(Coordinates.Me, picked);
                if (pts.Count > 0) {
                    _path = pts;
                    sw.Stop();
                    CoreManager.Current.Actions.AddChatText($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to generate path with {pts.Count} points", 1);
                }
            }
        }

        private List<RcVec3f> GetStitchedPath(Coordinates start, Coordinates end, int tries = 10) {
            var rc = new RcTestNavMeshTool();

            var halfExtents = new RcVec3f(1.2f, 1.2f, 1f);

            var query = new DtNavMeshQuery(Nav.OutsideMesh);
            unchecked {
                var m_filter = new DtQueryDefaultFilter();

                var startStatus = query.FindNearestPoly(start.ToNav(), halfExtents, m_filter, out long startRef, out var startPt, out bool isStartOverPoly);
                var endStatus = query.FindNearestPoly(end.ToNav(), halfExtents, m_filter, out long endRef, out var endPt, out bool isEndOverPoly);

                var polys = new List<long>();
                var pts = new List<RcVec3f>();

                var res = rc.FindFollowPath(Nav.OutsideMesh, query, startRef, endRef, startPt, endPt, m_filter, false, ref polys, ref pts);

                if (pts != null && pts.Count > 0) {
                    var last = pts.Last();
                    var fudge = 3f;
                    var lx = last.X % 192f;
                    var ly = last.Z % 192f;
                    CoreManager.Current.Actions.AddChatText($"Dist: {RcVec3f.Distance(last, end.ToNav())} {lx} {ly}", 2);
                    if (RcVec3f.Distance(last, end.ToNav()) > fudge && tries > 0) {
                        var o = new RcVec3f();

                        if (lx <= ly && lx % 192f < fudge) {
                            o.X -= fudge;
                            CoreManager.Current.Actions.AddChatText($"fudge -X", 2);
                        }
                        else if (lx >= ly && 192f - (lx % 192f) < fudge) {
                            o.X += fudge;
                            CoreManager.Current.Actions.AddChatText($"fudge +X", 2);
                        }
                        else if (ly < lx && ly % 192f < fudge) {
                            o.Y -= fudge;
                            CoreManager.Current.Actions.AddChatText($"fudge -Y", 2);
                        }
                        else if (ly > lx && 192f - (ly % 192f) < fudge) {
                            o.Y += fudge;
                            CoreManager.Current.Actions.AddChatText($"fudge +Y", 2);
                        }
                        var c = pts.Last().ToCoords();
                        c.LocalX += o.X;
                        c.LocalY += o.Y;

                        var nextPath = GetStitchedPath(c, end, tries - 1);
                        //if (nextPath.Count == 0) break;
                        pts.AddRange(nextPath);
                    }
                }

                return pts ?? new List<RcVec3f>();
            }
        }

        public void Dispose() {
            PluginCore.Instance.Game.OnRender3D -= Game_OnRender3D;

            _landscapeMap?.Dispose();
            _navMeshRenderer?.Dispose();
        }
    }
}
