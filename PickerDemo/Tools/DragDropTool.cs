﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using System;
using System.Runtime.InteropServices;

namespace PickerDemo.Tools {
    public unsafe class DragDropTool : ITool {
        [Flags]
        public enum DropItemFlags : uint {
            DROPITEM_FLAGS_NONE = 0u,
            DROPITEM_IS_CONTAINER = 1u,
            DROPITEM_IS_VENDOR = 2u,
            DROPITEM_IS_SHORTCUT = 4u,
            DROPITEM_IS_SALVAGE = 8u,
            DROPITEM_IS_ALIAS = 14u
        }


        public string Name => "DragDrop";
        public bool IsActive { get; set; }
        private string _droppedString = "none";

        #region hooks
        public static unsafe char StartDragandDrop(ref UIElementManager This, ref UIElement _elem, int i_iClickX, int i_iClickY) {
            return ((delegate* unmanaged[Thiscall]<ref UIElementManager, ref UIElement, int, int, char>)4579616)(ref This, ref _elem, i_iClickX, i_iClickY);
        }

        //.text:0045E120 ; char __thiscall UIElementManager::StartDragandDrop(UIElementManager *this, struct UIElement *, int, int)
        private static MultiHook Hook_UIElementManager_StartDragandDrop = new AcClient.MultiHook(0x0045E120, 0x004E4027);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal unsafe delegate char UIElementManager_StartDragandDrop_Def(ref UIElementManager This, ref UIElement _elem, int i_iClickX, int i_iClickY);
        private static unsafe char StartDragandDrop_Handler(ref UIElementManager This, ref UIElement _elem, int i_iClickX, int i_iClickY) {
            return StartDragandDrop(ref This, ref _elem, i_iClickX, i_iClickY);
        }
        #endregion hooks

        public DragDropTool() {
            /*
            if (!Hook_UIElementManager_StartDragandDrop.Setup(new UIElementManager_StartDragandDrop_Def(StartDragandDrop_Handler))) {
                UBService.WriteLog($"DragDrop > HOOK > UIElementManager_StartDragandDrop_Def install falure", LogLevel.Error);
                return;
            }
            */
        }

        bool _hasDrop = false;
        string _lastDragged = "none";
        private bool _isDragging;
        private uint _lastDragItemId;
        private uint _lastDragSpellId;
        private AcClient.DropItemFlags _lastDragFlags;
        private UIElement* _m_dragIcon;

        public unsafe IntPtr CreateHollowElement(IntPtr This, IntPtr i_pParent)
        {
            return ((delegate* unmanaged[Thiscall]<IntPtr, IntPtr, IntPtr>)4575680)(This, i_pParent);
        }

        public unsafe byte StartDragandDrop(IntPtr This, IntPtr _elem, int i_iClickX, int i_iClickY)
        {
            return ((delegate* unmanaged[Thiscall]<IntPtr, IntPtr, int, int, byte>)4579616)(This, _elem, i_iClickX, i_iClickY);
        }

        public void Render() {
            ImGui.Text($"AC Drag:");

            var ui = UIElementManager.s_pInstance;

            ImGui.Text($"Dragged: {ui->m_dragX}, {ui->m_dragY}");
            ImGui.Text($"Dragged: {(int)ui->m_dragElement:X8}");
            ImGui.Text($"Dragged: {ui->m_bDragStarted}");

            var dragging = false;

            ImGui.Button($"DRAG ME ({_lastDragged})");
            if (!string.IsNullOrEmpty(_lastDragged) && ImGui.IsMouseDragging(ImGuiMouseButton.Left) && ImGui.BeginDragDropSource(ImGuiDragDropFlags.SourceExtern))
            {
                int _ptr = 0;
                ImGui.SetDragDropPayload("ACDRAGDROP", (IntPtr)(&_ptr), sizeof(int), ImGuiCond.Once);
                ImGui.Button($"DRAG ME {_lastDragged}");
                ImGui.EndDragDropSource();

                dragging = true;

                if (!_isDragging) {
                    _isDragging = true;
                    CoreManager.Current.Actions.AddChatText($"Dragging {_lastDragged}", 1);


                    _m_dragIcon = (UIElement*)CreateHollowElement((IntPtr)UIElementManager.s_pInstance, (IntPtr)UIElementManager.s_pInstance->m_pRootElement);
                    _m_dragIcon->SetDragable(1);
                    _m_dragIcon->SetVisible(1);
                    StartDragandDrop((IntPtr)ui, (IntPtr)_m_dragIcon, 0, 0);
                    CM_Item.SendNotice_BeginDrag(_lastDragItemId, _lastDragSpellId, (byte)_lastDragFlags);
                }
            }
             
            var dragEl = UIElementManager.s_pInstance->m_dragElement;
            if (dragEl is not null)
            {
                _lastDragged = GetDragElDisplayString(dragEl);
                uint itemId = 0;
                uint spellId = 0;
                AcClient.DropItemFlags flags;

                UIElement_ItemList.InqDropIconInfo(dragEl, &itemId, &spellId, &flags);
                _lastDragItemId = itemId;
                _lastDragSpellId = spellId;
                _lastDragFlags = flags;
            }

            if (!dragging && _isDragging)
            {
                CoreManager.Current.Actions.AddChatText($"ACDRAGDROP 222s", 1);
                _isDragging = false;
            }

            var __ds = _droppedString;
            ImGui.InputText("Drag Here", ref __ds, 1000, ImGuiInputTextFlags.ReadOnly);
            if (ImGui.BeginDragDropTarget()) {
                try {
                    var payload = ImGui.AcceptDragDropPayload("ACDRAGDROP");
                    if (payload.Data != IntPtr.Zero) {
                        _isDragging = false;
                        CoreManager.Current.Actions.AddChatText($"ACDRAGDROP 111", 1);
                        _droppedString = _lastDragged;
                    }
                }
                catch(Exception ex) {
                    CoreManager.Current.Actions.AddChatText($"ACDRAGDROP ERROR: {ex}", 15);
                }
                ImGui.EndDragDropTarget();
            }
        }

        private static string GetDragElDisplayString(UIElement* el) {
            if (el is null) {
                return "none";
            }

            uint itemId = 0;
            uint spellId = 0;
            AcClient.DropItemFlags flags;

            UIElement_ItemList.InqDropIconInfo(el, &itemId, &spellId, &flags);
            if (itemId != 0) {
                if (PluginCore.Instance.Game.World.TryGet(itemId, out var wo)) {
                    return $"Item: {wo.Name} {flags}";
                }
                else {
                    return $"Item: 0x{itemId:X8}";
                }
            }
            else {
                if (PluginCore.Instance.Game.Character.SpellBook.TryGet(spellId, out var spell)) {
                    return $"Spell: {spell.Name} {flags}";
                }
                else {
                    return $"Spell: 0x{spellId:X8}";
                }
            }
        }

        private static string GetDragElDisplayString(ref UIElement el) {
            fixed (UIElement* elptr = &el) {
                return GetDragElDisplayString(elptr);
            }
        }

        public void Dispose() {
            Hook_UIElementManager_StartDragandDrop.Remove();
        }
    }
}
