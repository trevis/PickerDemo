﻿using AcClient;
using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using ImGuiNET;
using PickerDemo.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.Settings;

namespace PickerDemo.Tools
{
    public unsafe class ClientUITool : ITool
    {
        private bool wireframe;
        private bool fogDisabled;
        private Dictionary<uint, StringTable>  _strings = new Dictionary<uint, StringTable>();

        public unsafe static delegate* unmanaged[Thiscall]<PStringBase<byte>*, void> __Ctor__ = (delegate* unmanaged[Thiscall]<PStringBase<byte>*, void>)0x00401340;
        private int __i;
        private readonly EnumMapper _elementLookup;

        public string Name => "Client UI";
        public bool IsActive { get; set; }

        public ClientUITool()
        {
            _elementLookup = UBService.PortalDat.ReadFromDat<EnumMapper>(0x2200001B);

        }

        public unsafe byte InqPropertyGroupNameStringStatic(uint _group_name_enum, PStringBase<byte>* _name_str)
        {
            return ((delegate* unmanaged[Cdecl]<uint, PStringBase<byte>*, byte>)0x00426910)(_group_name_enum, _name_str);
        }

        public unsafe byte InqPropertyPropertyNameStringStatic(uint _group_name_enum, PStringBase<byte>* _name_str)
        {
            return ((delegate* unmanaged[Cdecl]<uint, PStringBase<byte>*, byte>)0x004268D0)(_group_name_enum, _name_str);
        }
        //.text:00426910 ; char __cdecl MasterProperty::InqPropertyGroupNameStringStatic(int, int *)
        //char __cdecl MasterProperty::InqPropertyGroupNameStringStatic(unsigned int _group_name_enum, PStringBase<char> *_name_str)

        public int GetFirstChildElement(ref UIElement This) => ((delegate* unmanaged[Thiscall]<ref UIElement, int>)0x00464110)(ref This);

        public int GetNextChildElement(ref UIElement This, int i_pPrevChild)
        {
            return ((delegate* unmanaged[Thiscall]<ref UIElement, int, int>)4605072)(ref This, i_pPrevChild);
        }

        public byte MasterProperty__InqPropertyNameStringStatic(uint id, PStringBase<byte>* name) =>
            ((delegate* unmanaged[Cdecl]<uint, PStringBase<byte>*, byte>)0x004268D0)(id, name);

        internal static void Call_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, string message) => ((def_AC1Legacy__PStringBase_char__PStringBase_char)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00401340, typeof(def_AC1Legacy__PStringBase_char__PStringBase_char)))(s_NullBuffer, message);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, [MarshalAs(UnmanagedType.LPStr)] string message);

        internal static PStringBase<byte>* PStringBase_char_new(string contents)
        {
            void* s_NullBuffer = (void*)null;
            Call_AC1Legacy__PStringBase_char__PStringBase_char((int)&s_NullBuffer, contents);
            return (PStringBase<byte>*)s_NullBuffer;
        }
        public PStringBase<byte>* BaseProperty_GetGroupName(BaseProperty* This)
        {
            return ((delegate* unmanaged[Thiscall]<BaseProperty*, PStringBase<byte>*>)0x00429A10)(This);
        }
        public PStringBase<byte>* BaseProperty_GetPropertyName(BaseProperty* This)
        {
            return ((delegate* unmanaged[Thiscall]<BaseProperty*, PStringBase<byte>*>)0x00429A00)(This);
        }
        public byte BaseProperty_InqPropertyName(BaseProperty* This, PStringBase<byte>* name)
        {
            return ((delegate* unmanaged[Thiscall]<BaseProperty*, PStringBase<byte>*, byte>)0x00429A20)(This, name);
        }
        public byte BaseProperty_InqEnum(BaseProperty* This, int* val)
        {
            return ((delegate* unmanaged[Thiscall]<BaseProperty*, int*, byte>)0x00429940)(This, val);
        }
        public byte BasePropertyDesc_InqEnum(BasePropertyDesc* This, int* val, PStringBase<byte>* name)
        {
            return ((delegate* unmanaged[Thiscall]<BasePropertyDesc*, int*, PStringBase<byte>*, byte>)0x0042A500)(This, val, name);
        }

        public byte EnumPropertyValue_GetValueAsString(EnumPropertyValue* This, BasePropertyDesc* desc, PStringBase<byte>* name, byte format)
        {
            return ((delegate* unmanaged[Thiscall]<EnumPropertyValue*, BasePropertyDesc*, PStringBase<byte>*, byte, byte>)0x0042B860)(This, desc, name, format);
        }

        public byte BoolPropertyValue_InqBool(BoolPropertyValue* This, byte* val)
        {
            return ((delegate* unmanaged[Thiscall]<BoolPropertyValue*, byte*, byte>)0x00423AF0)(This, val);
        }

        public byte LongIntegerPropertyValue_GetValueAsString(BasePropertyDesc* This, PStringBase<byte>* val, byte flag)
        {
            return ((delegate* unmanaged[Thiscall]<BasePropertyDesc*, PStringBase<byte>*, byte, byte>)0x004279C0)(This, val, flag);
        }

        public int MasterDBMap_DivineType(uint id)
        {
            return ((delegate* unmanaged[Cdecl]<uint, int>)0x0041C280)(id);
        }

        public string GetElementType(uint type)
        {
            if (_elementLookup.IdToStringMap.TryGetValue(type, out var name))
            {
                return name;
            }
            return $"Unknown0x{type:X8}";
        }



        public enum UIElement_SmartBoxWrapper_SearchReason : int
        {
            sr_None = 0x0,
            sr_MouseOver = 0x1,
            sr_Select = 0x2,
            sr_Examine = 0x3,
            sr_Use = 0x4,
            sr_Drop = 0x5,
            sr_Drag = 0x6,
            sr_TargetedUse = 0x7,
        };

        public unsafe struct UIElement_Field
        {
            // Struct:
            public UIElement a0;
            sbyte m_rolloverStateChange;
            uint m_oldState;
        }

        public unsafe struct UIElement_SmartBoxWrapper
        {
            public gmNoticeHandler b;
            public UIElement_Field a;
            public uint m_cFlipCount;
            public double m_timeNextFlip;
            public uint m_iidUnderMouse;
            public uint m_iidSelectedObject;
            public UIElement_SmartBoxWrapper_SearchReason m_SearchReason;
            public bool m_fMouseMovementActive;
            public bool m_fMouseMovementInProgress;
            public SECTION_3D m_CurrentSection;
            public uint m_dropItemID;
            public uint m_targetMode;
            public UIElement* m_dragIcon;
        }

        struct VividTargetIndicator {
            public gmNoticeHandler gmNoticeHandler;
            public QualityChangeHandler qualityChangeHandler;
            public uint m_idSelectedTarget;
            public RGBAColor m_clrSelectedObjectColor;
            public uint m_vtiCurrent;
            public RGBAColor m_clrOnScreen;
            public RGBAColor m_clrOffScreen;
            public SmartArray<PTR<RenderSurface>> m_rgSourceImages;
            public UIElement* m_pOffScreen;
            public UIElement* m_pOnScreen;
            public SmartArray<PTR<UIElement>> m_rgOnScreenCorners;
            public bool m_bDisplayOn;
            public bool m_bEnabled;
    };

    struct gmSmartBoxUI {
            public UIElement_SmartBoxWrapper* m_pSmartBoxWrapper;
            public SmartBox* m_pSmartBox;
            public CPhysicsObj* teleportObj;
            public double gameVDist;
            public TeleportAnimState teleportAnimState;
            public double teleportRotationStartTime;
            public double teleportRotationDuration;
            public double teleportRotationStartAngle;
            public double teleportRotationEndAngle;
            public double teleportTransitionStartTime;
            public double teleportRotationCurAngle;
            public float teleportCurVDist;
            public UIElement_Text* m_pFPSDisplay;
            public UIElement_Viewport* m_pPortalSpace;
            public VividTargetIndicator m_vti;
            public uint m_eWindowID;
            public UIElement* m_pTopBorder;
            public UIElement* m_pLeftBorder;
            public UIElement* m_pBottomBorder;
            public UIElement* m_pRightBorder;
            public UIElement* m_pTopLeftCorner;
            public UIElement* m_pTopRightCorner;
            public UIElement* m_pBottomLeftCorner;
            public UIElement* m_pBottomRightCorner;
    };

    public void Render()
        {
            var ui = UIElementManager.s_pInstance;

            __i = 0;
            if (ui->m_pRootElement is not null)
            {
                var root = *ui->m_pRootElement;

                var first = *(UIElement*)GetFirstChildElement(ref root);
                var smart = (UIElement*)GetFirstChildElement(ref first);
                var w = ((UIElement_SmartBoxWrapper*)smart);
                ImGui.Text($"Smartbox: {w->m_iidUnderMouse:X8} {w->m_iidSelectedObject:X8}");
                if (ImGui.TreeNodeEx($"Root ({(UIElementType)root.m_desc.m_type})"))
                {
                    WriteElements(root, 6);
                    ImGui.TreePop();
                }
            }
        }

        private void WriteElements(UIElement root, int maxDepth, int depth = 1)
        {

            var first = (UIElement*)GetFirstChildElement(ref root);
            while (first != null)
            {
                DrawElement(first, maxDepth, depth);
                first = (UIElement*)GetNextChildElement(ref root, (int)first);
            }
        }

        private void DrawElement(UIElement* first, int maxDepth, int depth = 1)
        {
            var flags = ImGuiTreeNodeFlags.None | ImGuiTreeNodeFlags.OpenOnArrow | ImGuiTreeNodeFlags.OpenOnDoubleClick | ImGuiTreeNodeFlags.AllowOverlap;

            Box2D position = new();
            int zlevel = 0;
            first->GetCurrentPosition(&position, &zlevel);
            if (ImGui.TreeNodeEx($"{GetElementType(first->m_desc.m_elementID)}(0x{first->m_desc.m_type:X8}) ({(UIElementType)first->m_desc.m_type})###UIElement{(int)first}"))
            {
                if (ImGui.TreeNodeEx($"ToggleVisibile###Toggle_{(int)first}_{__i++}", ImGuiTreeNodeFlags.Leaf))
                {
                    if (ImGui.IsItemClicked())
                    {
                        first->SetVisible((byte)(first->IsVisible() != 0 ? 0 : 1));
                    }
                    ImGui.TreePop();
                }
                if (ImGui.TreeNodeEx($"Position: {position} Z: {zlevel}", ImGuiTreeNodeFlags.Leaf))
                {
                    System.Numerics.Vector2 size = new(position.m_x1 - position.m_x0, position.m_y1 - position.m_y0);
                    if (ImGui.InputFloat2("Size##Size", ref size))
                    {
                        first->ResizeTo((int)size.X, (int)size.Y);
                    }
                    System.Numerics.Vector2 pos = new(position.m_x0, position.m_y0);
                    if (ImGui.InputFloat2("Pos##Pos", ref pos))
                    {
                        first->MoveTo((int)pos.X, (int)pos.Y);
                    }
                    ImGui.TreePop();
                }

                var t = first->m_desc.stateDesc.m_properties.m_hashProperties.hashTable.m_Int32rusiveTable;
                var x = t.m_numElements;
                if (x > 0 && ImGui.TreeNodeEx($"Properties##Prop{__i++}", flags))
                {
                    for (var i = 0; i < t.m_numBuckets; i++)
                    {
                        var bucket = t.m_buckets[i];
                        if (bucket is not null)
                        {
                            RenderBaseProperty(first, bucket->m_data);
                        }
                    }

                    ImGui.TreePop();
                }

                var el = *first;
                var hasChildren = (UIElement*)GetFirstChildElement(ref el) is not null;
                if (hasChildren && ImGui.TreeNodeEx($"Children##Children{__i++}", flags))
                {
                    try
                    {
                        WriteElements(el, maxDepth, depth + 1);
                    }
                    catch { }
                    ImGui.TreePop();
                }
                ImGui.TreePop();
            }
        }

        private void RenderBaseProperty(UIElement* element, BaseProperty m_data)
        {
            string disp = null;
            try
            {
                var type = (BasePropertyType)m_data.m_pcPropertyDesc->m_propertyType;
                var nameStr = (PStringBase<byte>*)8487748;
                BaseProperty_InqPropertyName(&(m_data), nameStr);
                var name = nameStr->ToString();

                switch ((BasePropertyType)m_data.m_pcPropertyDesc->m_propertyType)
                {
                    case BasePropertyType.DataFile:
                        var datFileId = (((DataFilePropertyValue*)m_data.m_pcPropertyValue)->m_value);
                        var datFileType = (DatFileType)MasterDBMap_DivineType(datFileId);
                        disp = $"{name} ({type}) = 0x{datFileId:X8} ({datFileType})";
                        break;
                    case BasePropertyType.Integer:
                        disp = $"{name} ({type}) = {(((IntegerPropertyValue*)m_data.m_pcPropertyValue)->m_value)}";
                        int newIntVal = (((IntegerPropertyValue*)m_data.m_pcPropertyValue)->m_value);
                        if (ImGui.InputInt(name, ref newIntVal, 1, 1)) {
                            (((IntegerPropertyValue*)m_data.m_pcPropertyValue)->m_value) = newIntVal;
                        }
                        return;
                    case BasePropertyType.StringInfo:
                        var stringTableId = ((StringInfoPropertyValue*)m_data.m_pcPropertyValue)->m_value.m_tableID;
                        var stringId = ((StringInfoPropertyValue*)m_data.m_pcPropertyValue)->m_value.m_stringID;

                        if (!_strings.TryGetValue(stringTableId, out var table)) {
                            table = UBService.PortalDat.ReadFromDat<StringTable>(stringTableId);
                            _strings.Add(stringTableId, table);
                        }
                        var strTableVal = table.StringTableData.FirstOrDefault(x => x.Id == stringId)?.Strings.FirstOrDefault() ?? $"Unknown 0x{stringTableId:X8} 0x{stringId:X8}";

                        disp = $"{name} ({type}) = {strTableVal}";
                        break;
                    case BasePropertyType.LongInteger:
                        disp = $"{name} ({type}) = {(((LongIntegerPropertyValue*)m_data.m_pcPropertyValue)->m_value)}";
                        break;
                    case BasePropertyType.Bool:
                        disp = $"{name} ({type}) = {(((BoolPropertyValue*)m_data.m_pcPropertyValue)->m_value ? "True" : "False")}";

                        if (ImGui.TreeNodeEx($"{disp}##Property{__i++}", ImGuiTreeNodeFlags.Leaf))
                        {
                            if (ImGui.IsItemClicked())
                            {
                                element->SetAttribute_Bool(m_data.m_pcPropertyDesc->m_propertyName, (byte)(((BoolPropertyValue*)m_data.m_pcPropertyValue)->m_value ? 0 : 1));
                                ((BoolPropertyValue*)m_data.m_pcPropertyValue)->m_value = ((BoolPropertyValue*)m_data.m_pcPropertyValue)->m_value ? false : true;
                                CoreManager.Current.Actions.AddChatText($"Clicked: {disp}", 1);
                                
                            }
                            ImGui.TreePop();
                        }
                        return;
                    case BasePropertyType.Enum:
                        EnumPropertyValue_GetValueAsString((EnumPropertyValue*)m_data.m_pcPropertyValue, m_data.m_pcPropertyDesc, nameStr, 0);
                        disp = $"{name} ({type}) = {nameStr->ToString()}";
                        break;
                    case BasePropertyType.Array:
                        var arrayProp = (ArrayPropertyValue*)m_data.m_pcPropertyValue;
                        if (arrayProp->m_value.m_num == 0)
                        {
                            disp = $"{name} ({type}) = []";
                        }
                        else
                        {
                            /*
                            if (ImGui.TreeNodeEx($"ArrayProp:"))
                            {
                                for (var j = 0; j < arrayProp->m_value.m_num; j++)
                                {
                                    var baseProp = arrayProp->m_value.m_data[j];
                                    RenderBaseProperty(baseProp);
                                }
                                ImGui.TreePop();
                            }
                            */
                        }
                        return;
                    default:
                        disp = $"{name} ({type}) = ??";
                        break;
                }
            }
            catch (Exception ex) { disp = $"{m_data.m_pcPropertyDesc->m_propertyType}: {ex}"; }

            if (ImGui.TreeNodeEx($"{disp}##Property{__i++}", ImGuiTreeNodeFlags.Leaf))
            {
                ImGui.TreePop();
            }
        }

        public void Dispose()
        {

        }
    }
}
