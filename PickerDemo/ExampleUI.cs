﻿using AcClient;
using ACE.DatLoader.FileTypes;
using ACE.Entity;
using Decal.Adapter;
using Decal.Interop.D3DService;
using ImGuiNET;
using JeremyAnsel.Media.WavefrontObj;
using Microsoft.DirectX.Direct3D;
using PickerDemo.Lib;
using PickerDemo.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service;
using UtilityBelt.Service.Views;

namespace PickerDemo {
    internal unsafe class ExampleUI : IDisposable {
        private readonly PluginCore _plugin;
        private bool _didNavResize = false;

        /// <summary>
        /// The UBService Hud
        /// </summary>
        private readonly Hud hud;

        public ExampleUI(PluginCore plugin) {
            _plugin = plugin;
            // Create a new UBService Hud
            hud = UBService.Huds.CreateHud("PickerDemo");

            // set to show our icon in the UBService HudBar
            hud.ShowInBar = true;

            // subscribe to the hud render event so we can draw some controls
            hud.OnRender += Hud_OnRender;
            hud.OnPreRender += Hud_OnPreRender;

            hud.Visible = true;
            hud.WindowSettings |= ImGuiWindowFlags.AlwaysAutoResize;
        }

        private void Hud_OnPreRender(object sender, EventArgs e) {
            if (_plugin.Tools.FirstOrDefault(t => t.Name == "Nav")?.IsActive == true) {
                if (!_didNavResize) {
                    _didNavResize = true;
                    ImGui.SetNextWindowSize(new Vector2(500, 500), ImGuiCond.Always);
                    hud.WindowSettings = ImGuiWindowFlags.None;
                }
            }
            else {
                _didNavResize = false;
                hud.WindowSettings |= ImGuiWindowFlags.AlwaysAutoResize;
            }
        }

        public unsafe DBOCache* GetDBOCache(uint dbType) => ((delegate* unmanaged[Cdecl]<uint, DBOCache*>)0x004146B0)(dbType);
        
        /// <summary>
        /// Called every time the ui is redrawing.
        /// </summary>
        private unsafe void Hud_OnRender(object sender, EventArgs e) {
            try {
                if (ImGui.Button("Reload Map"))
                {
                    try
                    {
                        var human = DBCache.GetDBOCache(0x1)->ReloadObject(0x020010ACu);
                        CoreManager.Current.Actions.AddChatText($"Human: {(int)human:X8}", 1);
                        var olthoi = DBCache.GetDBOCache(0x1)->GetIfUsing(0x13000000u);
                        if (olthoi is not null)
                        {
                            CoreManager.Current.Actions.AddChatText($"Reloading olthoi?", 1);
                            olthoi->ReloadFromDisk();
                        }
                        CoreManager.Current.Actions.AddChatText($"Reloaded?", 1);
                    }
                    catch (Exception ex) {
                        CoreManager.Current.Actions.AddChatText(ex.Message, 1);
                    }
                }
                
                if (ImGui.BeginTabBar("tools")) {
                    foreach (var tool in _plugin.Tools) {
                        if (ImGui.BeginTabItem(tool.Name)) {
                            try {
                                tool.IsActive = true;
                                tool.Render();
                                if (tool is VisibleCellsTool) {
                                    var picker = _plugin.Tools.FirstOrDefault(t => t is PickerTool);
                                    if (picker is not null) {
                                        picker.IsActive = true;
                                        picker.Render();
                                    }
                                }
                            }
                            catch (Exception ex) {
                                PluginCore.Log(ex);
                            }
                            ImGui.EndTabItem();
                        }
                        else {
                            tool.IsActive = false;
                        }
                    }
                }
                ImGui.EndTabBar();
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void Dispose() {
            hud.Dispose();
        }
    }
}