﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;

namespace PickerDemo.Lib.Extensions {
    public static class NavFrameExtensions {
        public static Coordinates ToCoords(this UtilityBelt.Navigation.Shared.Models.Frame frame) {
            return new Coordinates(frame.Landblock, frame.LocalX, frame.LocalY, frame.LocalZ);
        }
    }
}
