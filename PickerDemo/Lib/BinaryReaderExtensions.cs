﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PickerDemo.Lib
{
    static class UnpackableExtensions
    {
        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray(this List<int> value, BinaryReader reader)
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var item = reader.ReadInt32();
                value.Add(item);
            }
        }

        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray(this List<uint> value, BinaryReader reader)
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var item = reader.ReadUInt32();
                value.Add(item);
            }
        }

        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray<T>(this List<T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var item = new T();
                item.Unpack(reader);
                value.Add(item);
            }
        }


        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray<T>(this Dictionary<ushort, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt16();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray<T>(this Dictionary<int, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A SmartArray uses a Compressed UInt32 for the length.
        /// </summary>
        public static void UnpackSmartArray<T>(this Dictionary<uint, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadCompressedUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }


        /// <summary>
        /// A PackedHashTable uses a UInt16 for length, and a UInt16 for bucket size.
        /// We don't need to worry about the bucket size with C#.
        /// </summary>
        public static void UnpackPackedHashTable(this Dictionary<uint, uint> value, BinaryReader reader)
        {
            var totalObjects = reader.ReadUInt16();
            /*var bucketSize = */
            reader.ReadUInt16();

            for (int i = 0; i < totalObjects; i++)
                value.Add(reader.ReadUInt32(), reader.ReadUInt32());
        }

        /// <summary>
        /// A PackedHashTable uses a UInt16 for length, and a UInt16 for bucket size.
        /// We don't need to worry about the bucket size with C#.
        /// </summary>
        public static void UnpackPackedHashTable<T>(this Dictionary<uint, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadUInt16();
            /*var bucketSize = */
            reader.ReadUInt16();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A PackedHashTable uses a UInt16 for length, and a UInt16 for bucket size.
        /// We don't need to worry about the bucket size with C#.
        /// </summary>
        public static void UnpackPackedHashTable<T>(this SortedDictionary<uint, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadUInt16();
            /*var bucketSize = */
            reader.ReadUInt16();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A list that uses a Int32 for the length.
        /// </summary>
        public static void Unpack(this List<uint> value, BinaryReader reader)
        {
            var totalObjects = reader.ReadInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var item = reader.ReadUInt32();
                value.Add(item);
            }
        }

        /// <summary>
        /// A list that uses a UInt32 for the length.
        /// </summary>
        public static void Unpack<T>(this List<T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var item = new T();
                item.Unpack(reader);
                value.Add(item);
            }
        }

        public static void Unpack<T>(this List<T> value, BinaryReader reader, uint fixedQuantity) where T : IUnpackable, new()
        {
            for (int i = 0; i < fixedQuantity; i++)
            {
                var item = new T();
                item.Unpack(reader);
                value.Add(item);
            }
        }


        public static void Unpack<T>(this Dictionary<ushort, T> value, BinaryReader reader, uint fixedQuantity) where T : IUnpackable, new()
        {
            for (int i = 0; i < fixedQuantity; i++)
            {
                var key = reader.ReadUInt16();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A Dictionary that uses a Int32 for the length.
        /// </summary>
        public static void Unpack<T>(this Dictionary<int, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A Dictionary that uses a Int32 for the length.
        /// </summary>
        public static void Unpack<T>(this Dictionary<uint, T> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        public static void Unpack<T>(this Dictionary<uint, T> value, BinaryReader reader, uint fixedQuantity) where T : IUnpackable, new()
        {
            for (int i = 0; i < fixedQuantity; i++)
            {
                var key = reader.ReadUInt32();

                var item = new T();
                item.Unpack(reader);
                value.Add(key, item);
            }
        }

        /// <summary>
        /// A Dictionary that uses a Int32 for the length.
        /// </summary>
        public static void Unpack<T>(this Dictionary<uint, Dictionary<uint, T>> value, BinaryReader reader) where T : IUnpackable, new()
        {
            var totalObjects = reader.ReadUInt32();

            for (int i = 0; i < totalObjects; i++)
            {
                var key = reader.ReadUInt32();

                var values = new Dictionary<uint, T>();
                values.Unpack(reader);

                value.Add(key, values);
            }
        }
    }

    static class BinaryReaderExtensions
    {
        /// <summary>
        /// Aligns the stream to the next DWORD boundary.
        /// </summary>
        public static void AlignBoundary(this BinaryReader reader)
        {
            // Aligns the DatReader to the next DWORD boundary.
            long alignDelta = reader.BaseStream.Position % 4;

            if (alignDelta != 0)
                reader.BaseStream.Position += (int)(4 - alignDelta);
        }


        /// <summary>
        /// A Compressed UInt32 can be 1, 2, or 4 bytes.<para />
        /// If the first MSB (0x80) is 0, it is one byte.<para />
        /// If the first MSB (0x80) is set and the second MSB (0x40) is 0, it's 2 bytes.<para />
        /// If both (0x80) and (0x40) are set, it's 4 bytes.
        /// </summary>
        public static uint ReadCompressedUInt32(this BinaryReader reader)
        {
            var b0 = reader.ReadByte();
            if ((b0 & 0x80) == 0)
                return b0;

            var b1 = reader.ReadByte();
            if ((b0 & 0x40) == 0)
                return (uint)(((b0 & 0x7F) << 8) | b1);

            var s = reader.ReadUInt16();
            return (uint)(((((b0 & 0x3F) << 8) | b1) << 16) | s);
        }

        /// <summary>
        /// First reads a UInt16. If the MSB is set, it will be masked with 0x3FFF, shifted left 2 bytes, and then OR'd with the next UInt16. The sum is then added to knownType.
        /// </summary>
        public static uint ReadAsDataIDOfKnownType(this BinaryReader reader, uint knownType)
        {
            var value = reader.ReadUInt16();

            if ((value & 0x8000) != 0)
            {
                var lower = reader.ReadUInt16();
                var higher = (value & 0x3FFF) << 16;

                return (uint)(knownType + (higher | lower));
            }

            return (knownType + value);
        }

        /// <summary>
        /// Returns a string as defined by the first sizeOfLength-byte's length
        /// </summary>
        public static string ReadPString(this BinaryReader reader, uint sizeOfLength = 2)
        {
            int stringlength;
            switch (sizeOfLength)
            {
                case 1:
                    stringlength = reader.ReadByte();
                    break;
                case 2:
                default:
                    stringlength = reader.ReadUInt16();
                    break;
            }

            byte[] thestring = reader.ReadBytes(stringlength);

            return System.Text.Encoding.Default.GetString(thestring);
        }

        /// <summary>
        /// Returns a string as defined by the first byte's length and removes the obfuscation (upper/lower nibbles swapped)
        /// </summary>
        public static string ReadObfuscatedString(this BinaryReader reader)
        {
            int stringlength = reader.ReadUInt16();

            byte[] thestring = reader.ReadBytes(stringlength);

            for (var i = 0; i < stringlength; i++)
                // flip the bytes in the string to undo the obfuscation: i.e. 0xAB => 0xBA
                thestring[i] = (byte)((thestring[i] >> 4) | (thestring[i] << 4));

            return System.Text.Encoding.GetEncoding(1252).GetString(thestring);
        }

        public static string ReadUnicodeString(this BinaryReader reader)
        {
            uint stringLength = reader.ReadCompressedUInt32();
            string thestring = "";
            for (int i = 0; i < stringLength; i++)
            {
                ushort myChar = reader.ReadUInt16();
                thestring += Convert.ToChar(myChar);
            }
            return thestring;
        }

        public static string Reverse(this string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        /// <summary>
        /// Returns a Vector3 object read out as 3 floats, x y z
        /// </summary>
        public static Vector3 ReadVector3(this BinaryReader reader)
        {
            var x = reader.ReadSingle();
            var y = reader.ReadSingle();
            var z = reader.ReadSingle();

            return new Vector3(x, y, z);
        }
    }
}
