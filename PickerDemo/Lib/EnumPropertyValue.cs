﻿using AcClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcClient
{
    public struct EnumPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public uint m_value;
    }
    public struct BoolPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public bool m_value;
    }
    public struct LongIntegerPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public long m_value;
    }
    public struct IntegerPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public int m_value;
    }
    public struct DataFilePropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public uint m_value;
    }
    public struct ArrayPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public SmartArray<BaseProperty> m_value;
    }
    public struct StringInfoPropertyValue
    {
        public BasePropertyValue basePropertyValue;
        public StringInfo m_value;
    }
}
