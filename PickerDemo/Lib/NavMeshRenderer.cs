﻿using DotRecast.Detour;
using ImGuiNET;
using PickerDemo.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickerDemo.Lib {
    public class NavMeshRenderer : IDisposable {
        internal void Render(DtNavMesh mesh) {
            var dl = ImGui.GetBackgroundDrawList();

            if (mesh is not null) {
                int vertexOffset = 1;
                for (int i = 0; i < mesh.GetTileCount(); i++) {
                    DtMeshTile tile = mesh.GetTile(i);
                    if (tile != null) {
                        for (int p = 0; p < tile.data.header.polyCount; p++) {
                            DtPoly poly = tile.data.polys[p];
                            if (poly.GetPolyType() == DtPolyTypes.DT_POLYTYPE_OFFMESH_CONNECTION) {
                                continue;
                            }
                            DrawPoly(dl, tile, p, 0xFFFF00FF);
                        }

                        vertexOffset += tile.data.header.vertCount;
                    }
                }
            }
        }

        private void DrawPoly(ImDrawListPtr dl, DtMeshTile tile, int index, uint col) {
            DtPoly p = tile.data.polys[index];
            var vertices = new List<System.Numerics.Vector3>();

            if (tile.data.detailMeshes != null) {
                ref DtPolyDetail pd = ref tile.data.detailMeshes[index];
                for (int j = 0; j < pd.triCount; ++j) {
                    int t = (pd.triBase + j) * 4;
                    for (int k = 0; k < 3; ++k) {
                        int v = tile.data.detailTris[t + k];
                        if (v < p.vertCount) {
                            vertices.Add(new System.Numerics.Vector3(tile.data.verts[p.verts[v] * 3], tile.data.verts[p.verts[v] * 3 + 2],
                                tile.data.verts[p.verts[v] * 3 + 1]));
                        }
                        else {
                            vertices.Add(new System.Numerics.Vector3(tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3],
                                tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3 + 2],
                                tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3 + 1]));
                        }
                    }
                }
            }
            else {
                for (int j = 1; j < p.vertCount - 1; ++j) {
                    vertices.Add(new System.Numerics.Vector3(tile.data.verts[p.verts[0] * 3], tile.data.verts[p.verts[0] * 3 + 2],
                        tile.data.verts[p.verts[0] * 3 + 1]));
                    for (int k = 0; k < 2; ++k) {
                        vertices.Add(new System.Numerics.Vector3(tile.data.verts[p.verts[j + k] * 3], tile.data.verts[p.verts[j + k] * 3 + 2],
                            tile.data.verts[p.verts[j + k] * 3 + 1]));
                    }
                }
            }

            var cameraPos = PluginCore.Instance.CameraH.Coordinates;
            var offset = new System.Numerics.Vector3(cameraPos.LBX() * -192f, cameraPos.LBY() * -192f, 0);

            for (var i = 0; i < vertices.Count; i += 3) {
                var a = vertices[i] + offset;
                var b = vertices[i + 1] + offset;
                var c = vertices[i + 2] + offset;

                dl.AddLine(a, b, col);
                dl.AddLine(b, c, col);
                dl.AddLine(c, a, col);
            }
        }

        public void Dispose() {
            
        }
    }
}
