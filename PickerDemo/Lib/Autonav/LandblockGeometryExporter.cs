﻿using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using PickerDemo.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.ACClientModule;
using Environment = ACE.DatLoader.FileTypes.Environment;

namespace UBNetServer.Lib {
    class BoundingBox {
        private bool hasData = false;

        public float MinX { get; set; }
        public float MaxX { get; set; }
        public float MinY { get; set; }
        public float MaxY { get; set; }
        public float MinZ { get; set; }
        public float MaxZ { get; set; }

        public float Width { get => (MaxX - MinX); }
        public float Depth { get => (MaxY - MinY); }
        public float Height { get => (MaxZ - MinZ); }

        public bool ContainsPoint2D(Vector3 loc) {
            return loc.X >= MinX && loc.X <= MaxX && loc.Y >= MinY && loc.Y <= MaxY;
        }

        public bool IntersectsCircle(Vector3 loc, float radius) {
            // dumb
            return loc.X + radius >= MinX && loc.X - radius <= MaxX && loc.Y + radius >= MinY && loc.Y - radius <= MaxY;
        }

        internal void Add(Vector3 pv) {
            if (!hasData || pv.X < MinX)
                MinX = pv.X;
            if (!hasData || pv.Y < MinY)
                MinY = pv.Y;
            if (!hasData || pv.Z < MinZ)
                MinZ = pv.Z;

            if (!hasData || pv.X > MaxX)
                MaxX = pv.X;
            if (!hasData || pv.Y > MaxY)
                MaxY = pv.Y;
            if (!hasData || pv.Z > MaxZ)
                MaxZ = pv.Z;

            hasData = true;
        }

        public override string ToString() {
            return $"MinX:{MinX}, MinY:{MinY}, MinZ:{MinZ}, MaxX:{MaxX}, MaxY:{MaxY}, MaxZ:{MaxZ}, Width:{Width}, Height:{Height}";
        }
    }

    class LandblockGeometryExporter {
        public List<Vector3> Vertices { get; } = new List<Vector3>();
        public List<List<int>> Polygons { get; } = new List<List<int>>();

        private Coordinates coords;
        private uint landblock;
        private uint landcell;
        private List<byte> Height { get; } = new List<byte>();
        private List<ushort> Terrain { get; } = new List<ushort>();

        private List<BoundingBox> BuildingBoundingBoxes = new List<BoundingBox>();

        public LandblockGeometryExporter(uint landcell) {
            this.coords = new Coordinates(landcell, 0, 0, 0);
            this.landblock = (landcell >> 16 << 16);
            this.landcell = landcell;
        }

        public void LoadLandblockInfo() {
            try {
                var frames = new List<Frame>();
                var landblockData = UBService.CellDat.ReadFromDat<CellLandblock>(landblock | 0x0000FFFF);
                var landblockInfo = UBService.CellDat.ReadFromDat<LandblockInfo>(landblock | 0x0000FFFE);

                var hasTerrain = (landcell & 0xFFFF) < 0x100;

                if (landblockData.Height != null && landblockData.Height.Count > 0) {
                    if (landblockData.Height.Where(h => h != 0).Any() == true) {
                        hasTerrain = true;
                    }
                    else {
                        hasTerrain = false;
                    }
                }
                else {
                    hasTerrain = false;
                }

                if (!hasTerrain) {
                    return;
                    for (var i = 0; i < landblockInfo.NumCells; i++) {
                        EnvCell cell = UBService.CellDat.ReadFromDat<EnvCell>((uint)((landblockInfo.Id >> 16 << 16) + 0x00000100 + i));
                        var tempFrames = frames.ToList();
                        tempFrames.Add(cell.Position);

                        LoadEnvironment(cell.EnvironmentId, tempFrames, cell.CellStructure);

                        if (cell.StaticObjects != null && cell.StaticObjects.Count > 0) {
                            foreach (var staticObject in cell.StaticObjects) {
                                var sFrames = frames.ToList();
                                sFrames.Add(staticObject.Frame);
                                BoundingBox bb = new BoundingBox();
                                LoadSetupOrGfxObj(staticObject.Id, sFrames, ref bb);
                            }
                        }
                    }
                    return;
                }

                if (landblockInfo.Objects != null && landblockInfo.Objects.Count > 0) {
                    for (var i = 0; i < landblockInfo.Objects.Count; i++) {
                        var obj = landblockInfo.Objects[i];
                        var sFrames = frames.ToList();
                        sFrames.Add(obj.Frame);
                        BoundingBox bb = new BoundingBox();
                        LoadSetupOrGfxObj(obj.Id, sFrames, ref bb);
                    }
                }

                if (landblockInfo.Buildings != null && landblockInfo.Buildings.Count > 0) {
                    for (var i = 0; i < landblockInfo.Buildings.Count; i++) {
                        var building = landblockInfo.Buildings[i];
                        var sFrames = frames.ToList();
                        sFrames.Add(building.Frame);
                        BoundingBox bb = new BoundingBox();
                        LoadSetupOrGfxObj(building.ModelId, sFrames, ref bb);
                        BuildingBoundingBoxes.Add(bb);
                    }
                }

                AddTerrain(landblockData, frames);
                if (landblockData.Height != null && landblockData.Height.Count > 0) {
                    for (int i = 0; i < 81; i++) {
                        Height.Add(landblockData.Height[i]);
                        Terrain.Add(landblockData.Terrain[i]);
                    }
                }
                
                /*
                var lbX = coords.LBX();
                var lbY = coords.LBY();
                for (var x = (int)lbX - 1; x <= lbX + 1; x++) {
                    if (x < 0 || x > 0xFF) continue;
                    for (var y = (int)lbY - 1; y <= lbY + 1; y++) {
                        if (y < 0 || y > 0xFF) continue;
                        if (x == lbX && y == lbY) continue;

                        var sideLandblock = UBService.CellDat.ReadFromDat<CellLandblock>((((uint)x << 24) + ((uint)y << 16)) | 0x0000FFFF);

                        AddTerrain(sideLandblock, new List<Frame>() {
                            new Frame(new UtilityBelt.Common.Messages.Types.Vector3((x - lbX) * 192f, (y - lbY) * 192f, 0), UtilityBelt.Common.Messages.Types.Quaternion.Identity)
                        });
                    }
                }
                */

                var RegionDesc = UBService.PortalDat.ReadFromDat<RegionDesc>(0x13000000);

                var blockX = (landblock >> 24) * 8;
                var blockY = (landblock >> 16 & 0xFF) * 8;
                for (uint i = 0; i < landblockData.Terrain.Count; i++) {
                    var terrain = landblockData.Terrain[(int)i];

                    var terrainType = terrain >> 2 & 0x1F;      // TerrainTypes table size = 32 (grass, desert, volcano, etc.)
                    var sceneType = terrain >> 11;              // SceneTypes table size = 89 total, 32 which can be indexed for each terrain type

                    var sceneInfo = (int)RegionDesc.TerrainInfo.TerrainTypes[terrainType].SceneTypes[sceneType];
                    var scenes = RegionDesc.SceneInfo.SceneTypes[sceneInfo].Scenes;
                    if (scenes.Count == 0) continue;

                    var cellX = i / 9;
                    var cellY = i % 9;

                    var globalCellX = (uint)(cellX + blockX);
                    var globalCellY = (uint)(cellY + blockY);

                    var cellMat = globalCellY * (712977289 * globalCellX + 1813693831) - 1109124029 * globalCellX + 2139937281;
                    var offset = cellMat * 2.3283064e-10;
                    var scene_idx = (int)(scenes.Count * offset);
                    if (scene_idx >= scenes.Count) scene_idx = 0;

                    var sceneId = scenes[scene_idx];

                    var scene = UBService.PortalDat.ReadFromDat<Scene>(sceneId);

                    var cellXMat = -1109124029 * globalCellX;
                    var cellYMat = 1813693831 * globalCellY;
                    cellMat = 1360117743 * globalCellX * globalCellY + 1888038839;

                    for (uint j = 0; j < scene.Objects.Count; j++) {
                        var obj = scene.Objects[(int)j];
                        var noise = (uint)(cellXMat + cellYMat - cellMat * (23399 + j)) * 2.3283064e-10;
                        if (obj.ObjId != 0 && obj.WeenieObj == 0) {
                            var sFrames = frames.ToList();
                            var position = Displace(obj, globalCellX, globalCellY, j);

                            var lx = cellX * 24 + position.X;
                            var ly = cellY * 24 + position.Y;

                            if (lx < 0 || ly < 0 || lx > 192 || ly > 192)
                                continue;

                            var loc = new UtilityBelt.Common.Messages.Types.Vector3(lx, ly, GetZ(lx, ly));
                            if (OnRoad(loc.ToNumerics()))
                                continue;

                            bool overlapsBuilding = false;
                            foreach (var bbb in BuildingBoundingBoxes) {
                                // todo: unhardcode this radius
                                if (bbb.IntersectsCircle(loc.ToNumerics(), 2.0f)) {
                                    overlapsBuilding = true;
                                    break;
                                }
                            }

                            if (overlapsBuilding) {
                                //Program.Log($"Found building overlap {loc} vs {BuildingBoundingBoxes[0]}");
                                continue;
                            }

                            var frame = new Frame(loc, obj.BaseLoc.Orientation);
                            sFrames.Add(frame);
                            BoundingBox bb = new BoundingBox();
                            LoadSetupOrGfxObj(obj.ObjId, sFrames, ref bb, true);
                        }

                        /*
                    if (noise < obj.Freq && obj.WeenieObj == 0) {
                        // pseudo-randomized placement
                        var position = ObjectDesc.Displace(obj, globalCellX, globalCellY, j);

                        var lx = cellX * 24 + position.X;
                        var ly = cellY * 24 + position.Y;
                        var loc = new Vector3(lx, ly, position.Z);

                        // ensure within landblock range, and not near road
                        if (lx < 0 || ly < 0 || lx >= 192 || ly >= 192 || OnRoad(loc)) continue;

                        // load scenery
                        var pos = new Position(ID);
                        pos.Frame.Origin = loc;
                        var outside = LandDefs.AdjustToOutside(pos);
                        var cell = get_landcell(pos.ObjCellID);
                        if (cell == null) continue;

                        // check for buildings
                        var sortCell = (SortCell)cell;
                        if (sortCell != null && sortCell.has_building()) continue;

                        Polygon walkable = null;
                        var terrainPoly = cell.find_terrain_poly(pos.Frame.Origin, ref walkable);
                        if (walkable == null) continue;

                        // ensure walkable slope
                        if (!ObjectDesc.CheckSlope(obj, walkable.Plane.Normal.Z)) continue;

                        walkable.Plane.set_height(ref pos.Frame.Origin);

                        // rotation
                        if (obj.Align != 0)
                            pos.Frame = ObjectDesc.ObjAlign(obj, walkable.Plane, pos.Frame.Origin);
                        else
                            pos.Frame = ObjectDesc.RotateObj(obj, globalCellX, globalCellY, j, pos.Frame.Origin);

                        // build object
                        var physicsObj = PhysicsObj.makeObject(obj.ObjId, 0, false);
                        physicsObj.DatObject = true;
                        physicsObj.set_initial_frame(pos.Frame);
                        if (!physicsObj.obj_within_block()) continue;

                        physicsObj.add_obj_to_cell(cell, pos.Frame);
                        var scale = ObjectDesc.ScaleObj(obj, globalCellX, globalCellY, j);
                        physicsObj.SetScaleStatic(scale);
                        Scenery.Add(physicsObj);
                    }
                    */
                    }
                }
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        private void AddTerrain(CellLandblock landblockData, List<Frame> frames) {
            if (landblockData.Height != null && landblockData.Height.Count > 0) {
                for (uint tileX = 0; tileX < 8; tileX++) {
                    for (uint tileY = 0; tileY < 8; tileY++) {
                        uint v1 = tileX * 9 + tileY;
                        uint v2 = tileX * 9 + tileY + 1;
                        uint v3 = (tileX + 1) * 9 + tileY;
                        uint v4 = (tileX + 1) * 9 + tileY + 1;

                        var p1 = new Vector3();
                        p1.X = tileX * 24;
                        p1.Y = tileY * 24;
                        p1.Z = landblockData.Height[(int)v1] * 2;

                        var p2 = new Vector3();
                        p2.X = tileX * 24;
                        p2.Y = (tileY + 1) * 24;
                        p2.Z = landblockData.Height[(int)v2] * 2;

                        var p3 = new Vector3();
                        p3.X = (tileX + 1) * 24;
                        p3.Y = tileY * 24;
                        p3.Z = landblockData.Height[(int)v3] * 2;

                        //Program.Log($"{landblockData.Height.Count} -- {tileX}, {tileY} -- {p1.X:N3}, {p1.Y:N3}, {p1.Z:N3} - {p2.X:N3}, {p2.Y:N3}, {p2.Z:N3} - {p3.X:N3}, {p3.Y:N3}, {p3.Z:N3}");

                        AddPolygon(new List<Vector3>() { p3, p2, p1 }, frames);
                        var p4 = new Vector3();
                        p4.X = (tileX + 1) * 24;
                        p4.Y = (tileY + 1) * 24;
                        p4.Z = landblockData.Height[(int)v4] * 2;
                        AddPolygon(new List<Vector3>() { p4, p2, p3 }, frames);
                    }
                }
            }
        }

        public bool OnRoad(Vector3 obj) {
            int x = (int)(obj.X / 24.0f);
            int y = (int)(obj.Y / 24.0f);

            float rMin = 5.0f;
            float rMax = 24.0f - 5.0f;

            int x0 = x;
            int x1 = x0 + 1;
            int y0 = y;
            int y1 = y0 + 1;

            uint r0 = GetRoad(x0, y0);
            uint r1 = GetRoad(x0, y1);
            uint r2 = GetRoad(x1, y0);
            uint r3 = GetRoad(x1, y1);

            if (r0 == 0 && r1 == 0 && r2 == 0 && r3 == 0)
                return false;

            float dx = obj.X - x * 24.0f;
            float dy = obj.Y - y * 24.0f;

            if (r0 > 0) {
                if (r1 > 0) {
                    if (r2 > 0) {
                        if (r3 > 0)
                            return true;
                        else
                            return (dx < rMin || dy < rMin);
                    }
                    else {
                        if (r3 > 0)
                            return (dx < rMin || dy > rMax);
                        else
                            return (dx < rMin);
                    }
                }
                else {
                    if (r2 > 0) {
                        if (r3 > 0)
                            return (dx > rMax || dy < rMin);
                        else
                            return (dy < rMin);
                    }
                    else {
                        if (r3 > 0)
                            return (Math.Abs(dx - dy) < rMin);
                        else
                            return (dx + dy < rMin);
                    }
                }
            }
            else {
                if (r1 > 0) {
                    if (r2 > 0) {
                        if (r3 > 0)
                            return (dx > rMax || dy > rMax);
                        else
                            return (Math.Abs(dx + dy - 24.0f) < rMin);
                    }
                    else {
                        if (r3 > 0)
                            return (dy > rMax);
                        else
                            return (24.0f + dx - dy < rMin);
                    }
                }
                else {
                    if (r2 > 0) {
                        if (r3 > 0)
                            return (dx > rMax);
                        else
                            return (24.0f - dx + dy < rMin);
                    }
                    else {
                        if (r3 > 0)
                            return (24.0f * 2f - dx - dy < rMin);
                        else
                            return false;
                    }
                }
            }
        }

        public uint GetRoad(int x, int y) {
            try {
                ushort t0 = Terrain[x * 9 + y];
                t0 = (ushort)(t0 & ((1 << 2) - 1));
                return t0;
            }
            catch {
                return 0;
            }
        }

        /// <summary>
        /// Calculates the z value on the CellLandblock plane at coordinate x,y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>The Z value for a given X/Y in the CellLandblock</returns>
        public float GetZ(float x, float y) {
            x = Math.Abs(x);
            y = Math.Abs(y);
            // Find the exact tile in the 8x8 square grid. The cell is 192x192, so each tile is 24x24
            uint tileX = (uint)Math.Ceiling(x / 24) - 1; // Subract 1 to 0-index these
            uint tileY = (uint)Math.Ceiling(y / 24) - 1; // Subract 1 to 0-index these

            uint v1 = tileX * 9 + tileY;
            uint v2 = tileX * 9 + tileY + 1;
            uint v3 = (tileX + 1) * 9 + tileY;

            if (v1 >= 81 || v2 >= 81 || v3 >= 81)
                return 0;

            //Program.Log($"{x}, {y} -- {v1}, {v2}, {v3}");

            var p1 = new Vector3();
            p1.X = tileX * 24;
            p1.Y = tileY * 24;
            p1.Z = Height[(int)v1] * 2;

            var p2 = new Vector3();
            p2.X = tileX * 24;
            p2.Y = (tileY + 1) * 24;
            p2.Z = Height[(int)v2] * 2;

            var p3 = new Vector3();
            p3.X = (tileX + 1) * 24;
            p3.Y = tileY * 24;
            p3.Z = Height[(int)v3] * 2;

            return GetPointOnPlane(p1, p2, p3, x, y);
        }

        /// <summary>
        /// Note that we only need 3 unique points to calculate our plane.
        /// https://social.msdn.microsoft.com/Forums/en-US/1b32dc40-f84d-4365-a677-b59e49d41eb0/how-to-calculate-a-point-on-a-plane-based-on-a-plane-from-3-points?forum=vbgeneral 
        /// </summary>
        private float GetPointOnPlane(Vector3 p1, Vector3 p2, Vector3 p3, float x, float y) {
            var v1 = new Vector3();
            var v2 = new Vector3();
            var abc = new Vector3();

            v1.X = p1.X - p3.X;
            v1.Y = p1.Y - p3.Y;
            v1.Z = p1.Z - p3.Z;

            v2.X = p2.X - p3.X;
            v2.Y = p2.Y - p3.Y;
            v2.Z = p2.Z - p3.Z;

            abc.X = (v1.Y * v2.Z) - (v1.Z * v2.Y);
            abc.Y = (v1.Z * v2.X) - (v1.X * v2.Z);
            abc.Z = (v1.X * v2.Y) - (v1.Y * v2.X);

            float d = (abc.X * p3.X) + (abc.Y * p3.Y) + (abc.Z * p3.Z);

            float z = (d - (abc.X * x) - (abc.Y * y)) / abc.Z;

            return z;
        }

        /// <summary>
        /// Displaces a scenery object into a pseudo-randomized location
        /// </summary>
        /// <param name="obj">The object description</param>
        /// <param name="ix">The global cell X-offset</param>
        /// <param name="iy">The global cell Y-offset</param>
        /// <param name="iq">The scene index of the object</param>
        /// <returns>The new location of the object</returns>
        public static Vector3 Displace(ObjectDesc obj, uint ix, uint iy, uint iq) {
            float x;
            float y;
            float z;

            var loc = obj.BaseLoc.Origin;

            if (obj.DisplaceX <= 0)
                x = loc.X;
            else
                x = (float)((1813693831 * iy - (iq + 45773) * (1360117743 * iy * ix + 1888038839) - 1109124029 * ix)
                    * 2.3283064e-10 * obj.DisplaceX + loc.X);

            if (obj.DisplaceY <= 0)
                y = loc.Y;
            else
                y = (float)((1813693831 * iy - (iq + 72719) * (1360117743 * iy * ix + 1888038839) - 1109124029 * ix)
                    * 2.3283064e-10 * obj.DisplaceY + loc.Y);

            z = loc.Z;

            var quadrant = (1813693831 * iy - ix * (1870387557 * iy + 1109124029) - 402451965) * 2.3283064e-10;

            if (quadrant >= 0.75) return new Vector3(y, -x, z);
            if (quadrant >= 0.5) return new Vector3(-x, -y, z);
            if (quadrant >= 0.25) return new Vector3(-y, x, z);

            return new Vector3(x, y, z);
        }

        private void LoadSetupOrGfxObj(uint id, List<Frame> sFrames, ref BoundingBox boundingBox, bool coneTop = false) {
            if ((id & 0x02000000) != 0) {
                LoadSetup(id, sFrames, ref boundingBox, coneTop);
            }
            else {
                LoadGfxObj(id, sFrames, ref boundingBox);
            }
        }

        private void LoadGfxObj(uint cellFileIndex, List<Frame> frames, ref BoundingBox boundingBox) {
            var gfxObj = UBService.PortalDat.ReadFromDat<GfxObj>(cellFileIndex);
            if (gfxObj == null)
                return;
            foreach (var pkv in gfxObj.PhysicsPolygons) {
                var vertices = pkv.Value.VertexIds.Select(v => gfxObj.VertexArray.Vertices[(ushort)v].Origin.ToNumerics()).ToList();
                var pvertices = AddPolygon(vertices, frames);
                foreach (var pv in pvertices) {
                    boundingBox.Add(pv);
                }
            }
        }

        private void LoadSetup(uint cellFileIndex, List<Frame> frames, ref BoundingBox boundingBox, bool coneTop = false) {
            //Program.Log($"Loading setup: {cellFileIndex:X8}");  
            var setupModel = UBService.PortalDat.ReadFromDat<SetupModel>(cellFileIndex);

            // physics draw priority is child part physics polys, then cylspheres, then spheres?
            var hasPhysicsPolys = false;
            foreach (var partId in setupModel.Parts) {
                var gfxObj = UBService.PortalDat.ReadFromDat<GfxObj>(partId);
                if (gfxObj.PhysicsPolygons != null && gfxObj.PhysicsPolygons.Count > 0) {
                    hasPhysicsPolys = true;
                    break;
                }
            }

            if (setupModel.CylSpheres != null && setupModel.CylSpheres.Count > 0) {
                foreach (var cSphere in setupModel.CylSpheres) {
                    List<List<Vector3>> polys = GetCylSpherePolygons(cSphere.Origin.ToNumerics(), cSphere.Height, cSphere.Radius, coneTop);
                    foreach (var poly in polys) {
                        var pvs = AddPolygon(poly, frames);
                        foreach (var pv in pvs)
                            boundingBox.Add(pv);
                    }
                }
            }

            if (!hasPhysicsPolys && setupModel.Spheres != null && setupModel.Spheres.Count > 0) {
                foreach (var sphere in setupModel.Spheres) {
                    List<List<Vector3>> polys = GetSpherePolygons(sphere.Origin.ToNumerics(), sphere.Radius);
                    foreach (var poly in polys) {
                        var pvs = AddPolygon(poly, frames);
                        foreach (var pv in pvs)
                            boundingBox.Add(pv);
                    }
                }
            }

            // draw all the child parts
            for (var i = 0; i < setupModel.Parts.Count; i++) {
                // always use PlacementFrames[0] ?
                var tempFrames = frames.ToList(); // clone
                tempFrames.Insert(0, setupModel.PlacementFrames[0].AnimFrame.Frames[i]);
                LoadGfxObj(setupModel.Parts[i], tempFrames, ref boundingBox);
            }
        }

        private void LoadEnvironment(uint cellFileIndex, List<Frame> frames, int envCellIndex = -1) {
            var environment = UBService.PortalDat.ReadFromDat<Environment>(cellFileIndex);

            // weird vaulted ceiling environments we dont need... they have outwards facing polys on the roof
            // so we end up generating unneeded navmesh polys "outside" the dungeon if these are left in
            if (environment.Id == (uint)0x0D0000CA || environment.Id == (uint)0x0D00016D)
                return;

            if (envCellIndex >= 0) {
                //var polys = DrawPhysics ? environment.Cells[(uint)envCellIndex].PhysicsPolygons : environment.Cells[(uint)envCellIndex].Polygons;
                var polys = environment.Cells[(uint)envCellIndex].PhysicsPolygons;
                foreach (var poly in polys.Values) {
                    AddPolygon(poly.VertexIds.Select(v => environment.Cells[(uint)envCellIndex].VertexArray.Vertices[(ushort)v].Origin.ToNumerics()).ToList(), frames);
                }
            }
            else {
                foreach (var envCell in environment.Cells.Values) {
                    foreach (var poly in envCell.PhysicsPolygons.Values) {
                        AddPolygon(poly.VertexIds.Select(v => envCell.VertexArray.Vertices[(ushort)v].Origin.ToNumerics()).ToList(), frames);
                    }
                } 
            }
        }

        public List<Vector3> AddPolygon(List<Vector3> vertices, List<Frame> frames) {
            var poly = new List<int>();
            var retvertices = new List<Vector3>();
            for (var i = 0; i < vertices.Count; i++) {
                var vertice = vertices[vertices.Count - 1 - i];
                foreach (var frame in frames) {
                    vertice = Transform(vertice, frame.Orientation.ToNumerics());
                    vertice += frame.Origin.ToNumerics();
                }
                retvertices.Add(vertice);

                vertice = new Vector3() {
                    X = vertice.X,
                    Y = vertice.Z,
                    Z = vertice.Y
                };

                var index = Vertices.IndexOf(vertice);
                if (index != -1) {
                    // obj face indexes start at 1
                    poly.Add(index + 1);
                }
                else {
                    Vertices.Add(vertice);
                    poly.Add(Vertices.Count);
                }
            }
            Polygons.Add(poly);
            return retvertices;
        }

        public string ToObjString() {
            var outStr = new StringBuilder();

            var vs = Vertices.ToArray();
            foreach (var vertex in vs) {
                var ox = coords.IsOutside() ? coords.LBX() * 192f : 0f;
                var oy = coords.IsOutside() ? coords.LBY() * 192f : 0f;
                outStr.Append($"v {vertex.X + ox} {vertex.Y} {vertex.Z + oy}\n");
            }

            var ps = Polygons.ToArray();
            foreach (var polygon in ps) {
                outStr.Append("f ");
                var rp = polygon.ToList();
                foreach (var indice in rp) {
                    outStr.Append($"{indice} ");
                }
                outStr.Append("\n");
            }

            return outStr.ToString();
        }

        public static List<List<Vector3>> GetCylSpherePolygons(Vector3 origin, float height, float radius, bool coneTop = false) {
            var num_sides = 12;
            var axis = new Vector3(0, 0, height);
            var results = new List<List<Vector3>>();
            // Get two vectors perpendicular to the axis.
            Vector3 v1;
            if ((axis.Z < -0.01) || (axis.Z > 0.01))
                v1 = new Vector3(axis.Z, axis.Z, -axis.X - axis.Y);
            else
                v1 = new Vector3(-axis.Y - axis.Z, axis.X, axis.X);
            Vector3 v2 = Vector3.Cross(v1, axis);

            // Make the vectors have length radius.
            v1 *= (radius / v1.Length());
            v2 *= (radius / v2.Length());

            var Positions = new List<Vector3>();

            // Make the top end cap.
            // Make the end point.
            int pt0 = Positions.Count; // Index of end_point.
            Positions.Add(origin);

            // Make the top points.
            double theta = 0;
            double dtheta = 2 * Math.PI / num_sides;
            for (int i = 0; i < num_sides; i++) {
                Positions.Add(origin +
                    (float)Math.Cos(theta) * v1 +
                    (float)Math.Sin(theta) * v2);
                theta += dtheta;
            }

            // Make the top triangles.
            int pt1 = Positions.Count - 1; // Index of last point.
            int pt2 = pt0 + 1;                  // Index of first point.
            for (int i = 0; i < num_sides; i++) {
                results.Add(new List<Vector3>() { Positions[pt0], Positions[pt1], Positions[pt2] });
                pt1 = pt2++;
            }

            // Make the bottom end cap.
            // Make the end point.
            pt0 = Positions.Count; // Index of end_point2.
            Vector3 end_point2 = origin + axis;
            Positions.Add(end_point2);

            // Make the bottom points.
            theta = 0;
            for (int i = 0; i < num_sides; i++) {
                Positions.Add(end_point2 +
                    (float)Math.Cos(theta) * v1 +
                    (float)Math.Sin(theta) * v2);
                theta += dtheta;
            }

            // Make the bottom triangles.
            theta = 0;
            pt1 = Positions.Count - 1; // Index of last point.
            pt2 = pt0 + 1;                  // Index of first point.
            var p0 = Positions[num_sides + 1];
            if (coneTop && axis.Z > 20)
                p0 += new Vector3(0, 0, 20);

            for (int i = 0; i < num_sides; i++) {
                results.Add(new List<Vector3>() { p0, Positions[pt2], Positions[pt1] });
                pt1 = pt2++;
            }

            // Make the sides.
            // Add the points to the mesh.
            int first_side_point = Positions.Count;
            theta = 0;
            for (int i = 0; i < num_sides; i++) {
                Vector3 p1 = origin +
                    (float)Math.Cos(theta) * v1 +
                    (float)Math.Sin(theta) * v2;
                Positions.Add(p1);
                Vector3 p2 = p1 + axis;
                Positions.Add(p2);
                theta += dtheta;
            }

            // Make the side triangles.
            pt1 = Positions.Count - 2;
            pt2 = pt1 + 1;
            int pt3 = first_side_point;
            int pt4 = pt3 + 1;
            for (int i = 0; i < num_sides; i++) {
                results.Add(new List<Vector3>() { Positions[pt1], Positions[pt2], Positions[pt4] });
                results.Add(new List<Vector3>() { Positions[pt1], Positions[pt4], Positions[pt3] });

                pt1 = pt3;
                pt3 += 2;
                pt2 = pt4;
                pt4 += 2;
            }

            return results;
        }

        public static List<List<Vector3>> GetSpherePolygons(Vector3 origin, float radius) {
            var results = new List<List<Vector3>>();
            var vectors = new List<Vector3>();
            var indices = new List<int>();

            Icosahedron(vectors, indices);

            for (var i = 0; i < 1; i++)
                Subdivide(vectors, indices, true);

            /// normalize vectors to "inflate" the icosahedron into a sphere.
            for (var i = 0; i < indices.Count / 3; i++) {
                var testIndicies = new List<int>();
                for (var x = 2; x >= 0; x--) {
                    var ii = (i * 3) + x;
                    vectors[indices[ii]] = Vector3.Normalize(vectors[indices[ii]]) * radius;
                    testIndicies.Add(indices[ii]);
                }

                // adjust height.. 
                var h = new Vector3(0, 0, radius);
                results.Add(new List<Vector3>() { vectors[testIndicies[0]] + h, vectors[testIndicies[1]] + h, vectors[testIndicies[2]] + h });
            }

            return results;
        }
        public static Vector3 CalculateTriSurfaceNormal(Vector3 a, Vector3 b, Vector3 c) {
            Vector3 normal = new Vector3();
            Vector3 u = new Vector3();
            Vector3 v = new Vector3();

            u.X = b.X - a.X;
            u.Y = b.Y - a.Y;
            u.Z = b.Z - a.Z;

            v.X = c.X - a.X;
            v.Y = c.Y - a.Y;
            v.Z = c.Z - a.Z;

            normal.X = u.Y * v.Z - u.Z * v.Y;
            normal.Y = u.Z * v.X - u.X * v.Z;
            normal.Z = u.X * v.Y - u.Y * v.X;

            return normal;
        }

        public static Vector3 Transform(Vector3 value, Quaternion rotation) {
            float x2 = rotation.X + rotation.X;
            float y2 = rotation.Y + rotation.Y;
            float z2 = rotation.Z + rotation.Z;

            float wx2 = rotation.W * x2;
            float wy2 = rotation.W * y2;
            float wz2 = rotation.W * z2;
            float xx2 = rotation.X * x2;
            float xy2 = rotation.X * y2;
            float xz2 = rotation.X * z2;
            float yy2 = rotation.Y * y2;
            float yz2 = rotation.Y * z2;
            float zz2 = rotation.Z * z2;

            return new Vector3(
                value.X * (1.0f - yy2 - zz2) + value.Y * (xy2 - wz2) + value.Z * (xz2 + wy2),
                value.X * (xy2 + wz2) + value.Y * (1.0f - xx2 - zz2) + value.Z * (yz2 - wx2),
                value.X * (xz2 - wy2) + value.Y * (yz2 + wx2) + value.Z * (1.0f - xx2 - yy2));
        }

        private static int GetMidpointIndex(Dictionary<string, int> midpointIndices, List<Vector3> vertices, int i0, int i1) {

            var edgeKey = string.Format("{0}_{1}", Math.Min(i0, i1), Math.Max(i0, i1));

            var midpointIndex = -1;

            if (!midpointIndices.TryGetValue(edgeKey, out midpointIndex)) {
                var v0 = vertices[i0];
                var v1 = vertices[i1];

                var midpoint = (v0 + v1) / 2f;

                if (vertices.Contains(midpoint))
                    midpointIndex = vertices.IndexOf(midpoint);
                else {
                    midpointIndex = vertices.Count;
                    vertices.Add(midpoint);
                    midpointIndices.Add(edgeKey, midpointIndex);
                }
            }


            return midpointIndex;

        }

        /// <remarks>
        ///      i0
        ///     /  \
        ///    m02-m01
        ///   /  \ /  \
        /// i2---m12---i1
        /// </remarks>
        /// <param name="vectors"></param>
        /// <param name="indices"></param>
        public static void Subdivide(List<Vector3> vectors, List<int> indices, bool removeSourceTriangles) {
            var midpointIndices = new Dictionary<string, int>();

            var newIndices = new List<int>(indices.Count * 4);

            if (!removeSourceTriangles)
                newIndices.AddRange(indices);

            for (var i = 0; i < indices.Count - 2; i += 3) {
                var i0 = indices[i];
                var i1 = indices[i + 1];
                var i2 = indices[i + 2];

                var m01 = GetMidpointIndex(midpointIndices, vectors, i0, i1);
                var m12 = GetMidpointIndex(midpointIndices, vectors, i1, i2);
                var m02 = GetMidpointIndex(midpointIndices, vectors, i2, i0);

                newIndices.AddRange(
                    new[] {
                    i0,m01,m02
                    ,
                    i1,m12,m01
                    ,
                    i2,m02,m12
                    ,
                    m02,m01,m12
                    }
                    );

            }

            indices.Clear();
            indices.AddRange(newIndices);
        }

        /// <summary>
        /// create a regular icosahedron (20-sided polyhedron)
        /// </summary>
        /// <param name="primitiveType"></param>
        /// <param name="size"></param>
        /// <param name="vertices"></param>
        /// <param name="indices"></param>
        /// <remarks>
        /// You can create this programmatically instead of using the given vertex 
        /// and index list, but it's kind of a pain and rather pointless beyond a 
        /// learning exercise.
        /// </remarks>

        /// note: icosahedron definition may have come from the OpenGL red book. I don't recall where I found it. 
        public static void Icosahedron(List<Vector3> vertices, List<int> indices, float scale = 1f) {

            indices.AddRange(
                new int[]
                {
                0,4,1,
                0,9,4,
                9,5,4,
                4,5,8,
                4,8,1,
                8,10,1,
                8,3,10,
                5,3,8,
                5,2,3,
                2,7,3,
                7,10,3,
                7,6,10,
                7,11,6,
                11,0,6,
                0,1,6,
                6,1,10,
                9,0,11,
                9,11,2,
                9,2,5,
                7,2,11
                }
                .Select(i => i + vertices.Count)
            );

            var X = 0.525731112119133606f;
            var Z = 0.850650808352039932f;

            vertices.AddRange(
                new[]
                {
                new Vector3(-X, 0f, Z),
                new Vector3(X, 0f, Z),
                new Vector3(-X, 0f, -Z),
                new Vector3(X, 0f, -Z),
                new Vector3(0f, Z, X),
                new Vector3(0f, Z, -X),
                new Vector3(0f, -Z, X),
                new Vector3(0f, -Z, -X),
                new Vector3(Z, X, 0f),
                new Vector3(-Z, X, 0f),
                new Vector3(Z, -X, 0f),
                new Vector3(-Z, -X, 0f)
                }
            );
        }
    }
}
