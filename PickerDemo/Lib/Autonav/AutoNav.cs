﻿using DotRecast.Detour;
using DotRecast.Recast.Toolset;
using DotRecast.Recast;
using PickerDemo.Lib.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using Decal.Adapter;
using DotRecast.Recast.Toolset.Builder;
using DotRecast.Recast.Toolset.Geom;
using UBNetServer.Lib;
using System.IO;
using DotRecast.Detour.Io;
using System.Runtime.CompilerServices;
using DotRecast.Detour.TileCache;
using DotRecast.Core;
using DotRecast.Detour.TileCache.Io.Compress;
using DotRecast.Recast.Geom;
using DotRecast.Detour.Extras.Unity.Astar;
using UtilityBelt.Service;
using Microsoft.DirectX.Direct3D;
using System.Threading;
using UtilityBelt.Navigation.Shared.Data;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using UtilityBelt.Navigation.Shared.Lib;
using UtilityBelt.Navigation.Shared.Enums;
using UtilityBelt.Navigation.Shared.Models;

namespace PickerDemo.Lib.Autonav {
    public class TestTileCacheMeshProcess : IDtTileCacheMeshProcess {
        public void Process(DtNavMeshCreateParams option) {
            for (int i = 0; i < option.polyCount; ++i) {
                option.polyFlags[i] = 1;
            }
        }
    }

    public class AutoNav {
        private const int VERTS_PER_POLY = 6;
        private ConcurrentDictionary<uint, long> _outsideMeshes = new ConcurrentDictionary<uint, long>();
        private long _lastRef;
        private bool _hasWorldMeshLoaded = false;
        private LandblockGraphStore _graphStore;

        public DtNavMesh OutsideMesh { get; private set; }
        public string MeshDirectory => Path.Combine(PluginCore.AssemblyDirectory, "Meshes");
        public string OutsideMeshDirectory => Path.Combine(PluginCore.AssemblyDirectory, "Meshes", "Outside");

        public AutoNav() {
            if (!Directory.Exists(MeshDirectory)) {
                Directory.CreateDirectory(MeshDirectory);
            }
            if (!Directory.Exists(OutsideMeshDirectory)) {
                Directory.CreateDirectory(OutsideMeshDirectory);
            }
            if (!Directory.Exists(Path.Combine(MeshDirectory, "Dungeons"))) {
                Directory.CreateDirectory(Path.Combine(MeshDirectory, "Dungeons"));
            }
            if (!Directory.Exists(Path.Combine(MeshDirectory, "Buildings"))) {
                Directory.CreateDirectory(Path.Combine(MeshDirectory, "Buildings"));
            }

            OutsideMesh = new DtNavMesh(new DtNavMeshParams() {
                maxTiles = 30,
                tileHeight = 192f,
                tileWidth = 192f,
                orig = new DotRecast.Core.Numerics.RcVec3f(0, 0, 0)
            }, VERTS_PER_POLY);

            Task.Run(() => {
                try {
                    var sw = new Stopwatch();
                    sw.Start();

                    var file = Path.Combine(PluginCore.AssemblyDirectory, "data", "landblockgraph_ace.ubd");
                    _graphStore = new LandblockGraphStore();

                    if (!_graphStore.TryDeserialize(file)) {
                        CoreManager.Current.Actions.AddChatText($"Failed to load graph: {file}", 1);
                        return;
                    }

                    var graph = _graphStore.Graph;
                    graph.BuildEdgeNodeCaches();

                    _hasWorldMeshLoaded = true;

                    sw.Stop();
                    CoreManager.Current.Actions.AddChatText($"Finished loading world navmesh in {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms", 1);
                }
                catch (Exception ex) { UtilityBelt.Service.UBService.LogException(ex); }
            });
        }

        public async Task<List<Coordinates>> FindLocalPath(Coordinates start, Coordinates end) {
            var path = new List<Coordinates>();

            if (Math.Abs(start.LBX() - end.LBX()) > 1 || Math.Abs(start.LBY() - end.LBY()) > 1) {
                throw new Exception($"Start landblock and end landblock need to be at most 1 tile apart... 0x{start.LandCell:X8} vs 0x{end.LandCell:X8}");
            }

            await LoadLocalLandblocks(start);

            return path;
        }

        public async Task<List<LBNode>?> FindWorldPath(Coordinates startCoords, Coordinates endCoords) {
            return await Task.Run(() => {
                var questFlags = new List<string>();
                var keys = new List<string>();
                var minLevel = CoreManager.Current.CharacterFilter.Level;

                var start = _graphStore.Graph.Nodes.Where(n => (n.Frame.Landblock >> 16) == (startCoords.LandCell >> 16)).FirstOrDefault();
                var goal = _graphStore.Graph.Nodes.Where(n => (n.Frame.Landblock >> 16) == (endCoords.LandCell >> 16)).FirstOrDefault();

                if (start == null) {
                    CoreManager.Current.Actions.AddChatText($"Start is null", 1);
                    return null;
                }
                if (goal == null) {
                    CoreManager.Current.Actions.AddChatText($"Goal is null", 1);
                    return null;
                }

                var startName = $"Start: {start.FriendlyName}";
                var goalName = $"End: {goal.FriendlyName}";

                if (start.Type == LocationType.Dungeon)
                    startName = DungeonsNames.GetName(start.Frame.Landblock);
                if (goal.Type == LocationType.Dungeon)
                    goalName = DungeonsNames.GetName(goal.Frame.Landblock);


                CoreManager.Current.Actions.AddChatText($"Generating path from {startName} -> {goalName} (With MinLevel: {minLevel}, Quest Flags: {(questFlags.Count > 0 ? string.Join(", ", questFlags) : "none")}, Keys: {(keys.Count > 0 ? string.Join(", ", keys) : "none")})", 1);

                var sw2 = new Stopwatch();
                sw2.Start();
                var path = LBAStar.FindPath(_graphStore.Graph, start, goal, questFlags, minLevel, out float distance, out uint checkedNodeCount);
                sw2.Stop();

                if (path == null) {
                    CoreManager.Current.Actions.AddChatText($"Path is null! Took {((double)sw2.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms total to check {checkedNodeCount:N0} nodes\n", 1);
                    return null;
                }

                var pathStrs = new List<string>();
                var prevNode = path.First().Node;
                for (var i = 0; i < path.Count; i++) {
                    var node = path[i].Node;
                    var reqs = node.Requirements == null ? "" : node.Requirements.ToString();

                    if (node.Type == LocationType.Dungeon) {
                        //pathStrs.Add($"- Enter Dungeon: {node.FriendlyName} [0x{node.Frame.Landblock >> 16:X4}] {reqs}");
                    }
                    else if (node.Type == LocationType.Portal) {
                        if (prevNode.Type == LocationType.Dungeon) {
                            //pathStrs.Add($"- Run to: {node.Frame.ToLocalString()}");
                            pathStrs.Add($"- Use Portal: {node.FriendlyName} @ {node.Frame.ToLocalString()} {reqs}");
                        }
                        else {
                            pathStrs.Add($"- Use Portal: {node.FriendlyName}[{node.WeenieId}] @ {node.Frame.ToCoordinatesString(3, false)} {reqs}");
                        }
                    }
                    else if (node.Type == LocationType.Landscape) {
                        while (i < path.Count) {
                            node = path[i].Node;
                            if (i + 1 < path.Count && path[i + 1].Node.Type == LocationType.Landscape) {
                                i++;
                            }
                            else { break; }
                        }
                        var dest = node.Frame.ToCoordinatesString(3, false);
                        if (path.Count > i + 1 && path[i + 1].Node.Type == LocationType.Portal)
                            dest = path[i + 1].Node.Frame.ToCoordinatesString(3, false);
                        pathStrs.Add($"- Run To: {dest} {reqs}");
                    }
                    prevNode = node;
                }

                CoreManager.Current.Actions.AddChatText($"{string.Join("\n", pathStrs)}\n-- Cost: {distance:N2}", 1);
                CoreManager.Current.Actions.AddChatText($"Took {((double)sw2.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to find path, with {checkedNodeCount:N0} nodes checked", 1);

                return path.Select(n => n.Node).ToList();
            });
        }

        private async Task LoadLocalLandblocks(Coordinates start) {

            var landblock = start.LandCell;

            var landblocks = new List<uint>();
            for (var i = 0; i <= 0xFFFF; i++) {
                landblocks.Add((uint)i << 16);
            }

            var p = 0xFFFF;
            foreach (var lbId in landblocks) {
                var c = new Coordinates(lbId, 0, 0, 0);
                var meshPath = Path.Combine(OutsideMeshDirectory, $"{c.LandCell >> 16:X4}.mesh");
                if (!File.Exists(meshPath)) {
                    try {
                        var q = await LoadLocalLandblock(c);
                        if (q is null) {
                            //CoreManager.Current.Actions.AddChatText($"Skipped: {c.LandCell:X8}", 1);
                        }
                        else {
                            CoreManager.Current.Actions.AddChatText($"Generated: {c.LandCell:X8} {(((float)0xFFFF - p) / (float)0xFFFF * 100f)}", 1);
                        }
                    }
                    catch (Exception ex) { UBService.LogException(ex); }
                }
                else {
                    //CoreManager.Current.Actions.AddChatText($"Skipped: {c.LandCell:X8}", 1);
                }
                Interlocked.Decrement(ref p);
            };
            return;
            var s = 2;

            for (var x = start.LBX() - s; x <= start.LBX() + s; x++) {
                for (var y = start.LBY() - s; y <= start.LBY() + s; y++) {
                    var newLbId = ((uint)x << 24) + ((uint)y << 16);
                    if (newLbId != (start.LandCell & 0xFFFF0000)) {
                        await LoadLocalLandblock(new Coordinates(newLbId, 0, 0, 0));
                    }
                }
            }
        }

        private async Task<DtMeshData?> LoadLocalLandblock(Coordinates coords) {
            if (coords.IsOutside()) {
                return await LoadLocalOutsideLandblock(coords);
            }

            return null;
        }

        private async Task<DtMeshData?> LoadLocalOutsideLandblock(Coordinates coords) {
            var meshPath = Path.Combine(OutsideMeshDirectory, $"{coords.LandCell >> 16:X4}.mesh");
            if (File.Exists(meshPath)) {
                return null;
                try {
                    var meshReader = new DtMeshDataReader();

                    using (var stream = File.OpenRead(meshPath))
                    using (var reader = new BinaryReader(stream)) {
                        var rcBytes = new RcByteBuffer(reader.ReadBytes((int)stream.Length));
                        var meshData = meshReader.Read(rcBytes, VERTS_PER_POLY, true);
                        LoadTile(coords, meshData);

                        return meshData;
                    }
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }

            var geom = CellGeometryProvider.LoadFile(coords.LandCell);
            if (geom is null) return null;

            var builder = new NavMeshBuilder();
            var settings = GetMeshSettings(coords);
            var res = builder.Build(geom, settings);

            if (res is not null) {
                res.header.x = (int)(coords.LandCell >> 24) & 0xFF;
                res.header.y = (int)(coords.LandCell >> 16) & 0xFF;
                //LoadTile(coords, res);

                var meshWriter = new DtMeshDataWriter();
                using (var stream = File.OpenWrite(meshPath))
                using (var writer = new BinaryWriter(stream)) {
                    meshWriter.Write(writer, res, RcByteOrder.LITTLE_ENDIAN, false);
                }
            }

            return res;
        }

        private void LoadTile(Coordinates coords, DtMeshData meshData) {
            CoreManager.Current.Actions.AddChatText($"mesh: {meshData.header.x:X2}{meshData.header.y:X2} // {meshData.verts[0]}", 1);
            if (_outsideMeshes.TryRemove(coords.LandCell & 0xFFFF0000, out var tileRef)) {
                OutsideMesh.RemoveTile(tileRef);
            }
            tileRef = OutsideMesh.UpdateTile(meshData, 0);

            _lastRef = tileRef;
            _outsideMeshes.TryAdd(coords.LandCell & 0xFFFF0000, tileRef);
        }

        private RcNavMeshBuildSettings GetMeshSettings(Coordinates? coords = null) {
            return new RcNavMeshBuildSettings() {
                agentHeight = 1.6f, // todo, lugians have different height at least...
                agentMaxClimb = 0.9f,
                agentMaxSlope = 50f,
                cellHeight = 0.25f,
                cellSize = 0.25f,
                agentRadius = 0.4f,
                detailSampleDist = 6.0f,
                detailSampleMaxError = 1.0f,
                edgeMaxError = 1f,
                edgeMaxLen = 12.0f,
                mergedRegionSize = 20,
                minRegionSize = 8,
                vertsPerPoly = VERTS_PER_POLY,
                partitioning = (int)RcPartition.WATERSHED
            };
        }
    }
}
